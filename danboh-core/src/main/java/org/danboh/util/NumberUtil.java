/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.util;

import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;
import org.danboh.Constants;

/**
 * 数値関連のユーティリティクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class NumberUtil implements Constants {
	/**
	 * コンストラクタ。
	 */
	private NumberUtil() {
	}

	/**
	 * オブジェクトを数値に変換します。
	 *
	 * @param obj オブジェクト
	 * @return 数値
	 */
	public static int makeInt(Object obj) {
		if ((null == obj) || (!(obj instanceof String))) {
			return -1;
		}
		String val = (String) obj;
		if (EMPTY.equals(val)) {
			return -1;
		}
		return Integer.parseInt(StringUtils.trim((String) obj));
	}

	/**
	 * 数値を文字列に変換します。
	 *
	 * @param number 数値
	 * @return 文字列
	 */
	public static String toString(Integer number) {
		if (number == null) {
			return EMPTY;
		}

		return (number > 0) ? String.valueOf(number) : EMPTY;

	}

	/**
	 * 数値をゼロサプライします。
	 *
	 * @param value
	 *            数値
	 * @param digit
	 *            桁数
	 * @return ゼロサプライした文字列
	 */
	public static String zeroSupply(int value, int digit) {
		StringBuilder format = new StringBuilder();
		for (int i = 0; i < digit; i++) {
			format.append("0");
		}
		DecimalFormat df = new DecimalFormat(format.toString());
		return df.format(value);
	}
}
