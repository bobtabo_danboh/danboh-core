/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.util;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.danboh.Constants;
import org.danboh.mapper.table.Column;
import org.danboh.mapper.table.Relation;
import org.danboh.mapper.table.Table;
import org.danboh.xml.jaxb.setting.ForeignKey;

import com.google.common.base.CaseFormat;

/**
 * Velocity関連のユーティリティクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class VelocityUtil implements Constants {
	/** 唯一のインスタンス */
	private static VelocityUtil instance = new VelocityUtil();

	/**
	 * コンストラクタ。
	 */
	private VelocityUtil() {
	}

	/**
	 * インスタンスを取得する
	 *
	 * @return インスタンス
	 */
	public static VelocityUtil getInstance() {
		return VelocityUtil.instance;
	}

	/**
	 * 先頭を大文字にする
	 *
	 * @param value
	 *            文字列
	 * @return 先頭が大文字の文字列
	 */
	public static String largeOne(String value) {
		return value.substring(0, 1).toUpperCase() + value.substring(1);
	}

	/**
	 * 先頭を子文字にする
	 *
	 * @param value
	 *            文字列
	 * @return 先頭が子文字の文字列
	 */
	public static String smallOne(String value) {
		return value.substring(0, 1).toLowerCase() + value.substring(1);
	}

	public static String toSeparated(String name, char separator) {
		StringBuilder stringBuilder = new StringBuilder();
		for (char charactor : name.toCharArray()) {
			if (charactor >= 'A' && charactor <= 'Z') {
				stringBuilder.append(separator);
				stringBuilder.append(Character.toLowerCase(charactor));
			} else {
				stringBuilder.append(charactor);
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * キャメルケースに変換します。
	 *
	 * @param name 文字列
	 * @param separator 区切り文字
	 * @return
	 */
	public static String toCamelCase(String name, char separator) {
		StringBuilder stringBuilder = new StringBuilder();

		boolean toUppserCase = false;
		for (char charactor : name.toCharArray()) {
			if (toUppserCase) {
				stringBuilder.append(Character.toUpperCase(charactor));
				toUppserCase = false;
			} else if (charactor == separator) {
				toUppserCase = true;
			} else {
				stringBuilder.append(charactor);
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * リレーションクラス名を取得します。
	 *
	 * @param tableMap テーブル情報マップ
	 * @param relation リレーション情報
	 * @return リレーションクラス名
	 */
	public static String getRelationClassName(Map<String, Table> tableMap, Relation relation) {
		String result = EMPTY;
		if (relation.isToMany()) {
			result = "java.util.List<"
					+ ((Table) tableMap.get(relation.getEntityName())).getClassName()
					+ ">";
		} else {
			if (tableMap.containsKey(relation.getEntityName())) {
				result = ((Table) tableMap.get(relation.getEntityName())).getClassName();
			}
		}

		if (StringUtils.isEmpty(result) && StringUtils.isNotEmpty(relation.getClasspath())) {
			result = relation.getClasspath();
		}

		return result;
	}

	/**
	 * リレーションフィールド名を取得します。
	 * @param relation リレーション情報
	 * @return リレーションフィールド名
	 */
	public static String getRelationFieldName(Relation relation) {
		if (relation.isToMany()) {
			return relation.getFieldName() + "List";
		}
		return relation.getFieldName();
	}

	/**
	 * リレーションフィールド名が存在するか確認します。
	 *
	 * @param tableMap テーブルマップ
	 * @param table テーブル情報
	 * @param relation リレーション情報
	 * @return 存在する場合 true を返します
	 */
	public static boolean hasRelationField(Map<String, Table> tableMap, Table table, Relation relation) {
		String relationFieldName = smallOne(((Table) tableMap.get(relation.getEntityName())).getClassName()) + "Id";

		for (Column column : table.getColumnMap().values()) {
			if (column.getFieldName().equals(relationFieldName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * リレーション情報を取得します。
	 *
	 * @param tableMap テーブルマップ
	 * @param table テーブル情報
	 * @param relation リレーション情報
	 * @return リレーション情報
	 */
	public static String getRelationMappedBy(Map<String, Table> tableMap, Table table, Relation relation) {
		if (relation.isToMany()
				|| (relation.getMultiplicity().equals("OneToOne") && !hasRelationField(tableMap, table, relation))) {
			if (relation.getMappedBy().equals(EMPTY)) {
				return smallOne(table.getClassName());
			} else {
				return relation.getMappedBy();
			}
		} else {
			return EMPTY;
		}
	}

	/**
	 * 外部キー結合出力を行うか確認します。
	 *
	 * @param tableMap テーブルマップ
	 * @param table テーブル情報
	 * @param relation リレーション情報
	 * @return 外部キー結合出力を行う場合 true を返します
	 */
	public static boolean isRelationJoinColumn(Map<String, Table> tableMap, Table table, Relation relation) {
		return (relation.getMultiplicity().equals("ManyToOne") && StringUtils.isNotEmpty(relation.getJoinColumn()));
	}

	/**
	 * 外部キーが出力可能であるか確認します。
	 *
	 * @param foreignKey 外部キー設定オブジェクト
	 * @param relation リレーション情報
	 * @return 出力可能な場合 true を返します
	 */
	public static boolean isOutPutForeignKey(ForeignKey foreignKey, Relation relation) {
		return foreignKey.isOutput() && foreignKey.getMultiplicity().value().equals(relation.getMultiplicity())
				&& StringUtils.isNotEmpty(relation.getJoinColumn());
	}

	/**
	 * スネークケースに変換します。
	 *
	 * @param camel キャメルケース文字列
	 * @return 文字列
	 */
	public static String toSnake(String camel) {
		return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, camel);
	}

	/**
	 * キャメルケースに変換します。
	 *
	 * @param snake スネークケース文字列
	 * @return 文字列
	 */
	public static String toCamel(String snake) {
		return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, snake);
	}

	/**
	 * トークン設定であるか確認します。
	 *
	 * @param token トークン設定値
	 * @return 設定の場合 true を返します
	 */
	public static boolean isTokenSet(String token) {
		return StringUtils.equals(token, "設定");
	}

	/**
	 * トークン確認であるか確認します。
	 *
	 * @param token トークン設定値
	 * @return 確認の場合 true を返します
	 */
	public static boolean isTokenCheck(String token) {
		return StringUtils.equals(token, "確認");
	}
}