/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * 他の文字コードをアスキーコードに変換するInputStreamクラス。<BR>
 * native2asciiの代わりにプロパティファイル等を変換することが出来る。<BR>
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class NativeToAsciiInputStream extends FilterInputStream {
	private InputStreamReader reader;
	private int c;
	private int state;
	private int markedc;
	private int markedstate;

	/**
	 *  変換元のエンコード文字列を指定してNativeToAsciiInputStreamを作成する。
	 *
	 * @param in 変換を行うInputStream
	 * @param encode エンコード文字列
	 * @throws UnsupportedEncodingException サポートしていないエンコード文字列の場合
	 */
	public NativeToAsciiInputStream(InputStream in, String encode)
			throws UnsupportedEncodingException {
		super(in);
		reader = new InputStreamReader(in, encode);
	}

	/**
	 * デフォルトのエンコード文字列を使用してNativeToAsciiInputStream object作成する。
	 *
	 * @param in Description of the Parameter
	 */
	public NativeToAsciiInputStream(InputStream in) {
		super(in);
		reader = new InputStreamReader(in);
	}

	/**
	 * 入力ストリームから 1 バイトを読み込みます。
	 * このメソッドは、有効な入力がまだない場合はブロックします。
	 *
	 * @return データの次のバイト。ファイルの終わりに達した場合は -1
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public int read() throws IOException {
		switch (state) {
		case 0:
			c = reader.read();
			if (c <= 0x00ff) {
				return c;
			}
			state = 1;
			return (int) '\\';
		case 1:
			state = 2;
			return (int) 'u';
		default:
			// シフトして1バイト分ずつ取り出す
			int uc = (c >> (4 * (5 - state))) & 0x000f;
			state++;
			if (state == 6) {
				state = 0;
			}
			// 文字コードを返す
			return (uc < 10 ? uc + (int) '0' : uc - 10 + (int) 'a');
		}
	}

	/**
	 * 入力ストリームからバイト配列へ最大 len バイトのデータを読み込みます。
	 * このメソッドは、入力が可能になるまでブロックします。
	 *
	 * @param b データの読み込み先のバッファ
	 * @param off データの開始オフセット
	 * @param len 読み込む最大バイト数
	 * @return バッファに読み込んだバイトの合計数。ファイルの終わりに達してデータがない場合は -1
	 * @throws IOException  入出力エラーが発生した場合
	 */
	public int read(byte b[], int off, int len) throws IOException {
		int count = 0;
		int i = 0;
		for (; off < len; off++) {
			i = read();
			if (i == -1) {
				break;
			}
			b[off] = (byte) i;
			count++;
		}
		if (i == -1 && count == 0) {
			return -1;
		}
		return count;
	}

	/**
	 * 入力ストリームの現在位置にマークを設定します。
	 *
	 * @param n スキップするバイト数
	 * @return 実際にスキップされたバイト数
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public long skip(long n) throws IOException {
		long l = 0;
		int i = 0;
		for (; l < n; l++) {
			i = read();
			if (i == -1) {
				break;
			}
		}
		if (i == -1 && l == 0) {
			return -1;
		}
		return l;
	}

	/**
	 * 入力ストリームの現在位置にマークを設定します。
	 *
	 * @param readlimit マーク位置が無効になる前に読み込み可能なバイトの最大リミット
	 */
	public synchronized void mark(int readlimit) {
		in.mark(readlimit);
		markedc = c;
		markedstate = state;
	}

	/**
	 * このストリームの位置を、入力ストリームで最後に mark メソッドが呼び出されたときのマーク位置に再設定します。
	 *
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public synchronized void reset() throws IOException {
		in.reset();
		c = markedc;
		state = markedstate;
	}

	/**
	 * 設定されているエンコード文字列を返します。
	 *
	 * @return エンコード文字列
	 */
	public String getEncoding() {
		return reader.getEncoding();
	}
}
