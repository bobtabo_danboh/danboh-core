/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.Arrays;

import org.apache.oro.io.GlobFilenameFilter;

/**
 * ファイル関連のユーティリティクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public final class FileUtil {
	/**
	 * コンストラクタ。
	 */
	private FileUtil() {
	}

	/**
	 * 対象ファイルを取得します。
	 *
	 * @return 対象ファイル配列
	 * @throws FileNotFoundException 対象ファイルが見つからない場合にスローされる例外です
	 */
	public static File[] getFiles(String filePath) throws FileNotFoundException {
		FilenameFilter filenameFilter = new GlobFilenameFilter(filePath.substring(filePath.lastIndexOf("/") + 1));
		File path = new File(filePath.substring(0, filePath.lastIndexOf("/")));
		File[] files = path.listFiles(filenameFilter);

		if (files == null || files.length == 0) {
			throw new FileNotFoundException("処理対象のファイルが見つかりませんでした");
		}

		Arrays.sort(files, new java.util.Comparator<File>() {
			public int compare(File file1, File file2){
			    return file1.getName().compareTo(file2.getName());
			}
		});

		return files;
	}
}
