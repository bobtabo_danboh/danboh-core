/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.Mapping;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.table.Column;
import org.danboh.mapper.table.Table;
import org.danboh.mapper.web.Field;
import org.danboh.mapper.web.Web;

/**
 * マッピング関連のユーティリティクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class MappingUtil {
	/**
	 * コンストラクタ。
	 */
	private MappingUtil() {
	}

	/**
	 * Web定義にテーブル定義をマッピングします。
	 *
	 * @param webMap Web定義書マップ
	 * @param tableMap テーブル定義書マップ
	 */
	public static void makeMapping(Map<String, Web> webMap, Map<String, Table> tableMap) {
		for (Web web : webMap.values()) {
			makeMapping(web, tableMap);
			for (Web subClass : web.getSubClasses().values()) {
				makeMapping(subClass, tableMap);
			}
		}
	}

	/**
	 * バッチ定義にテーブル定義をマッピングします。
	 *
	 * @param batchMap バッチ定義書マップ
	 * @param tableMap テーブル定義書マップ
	 */
	public static void makeBatchMapping(Map<String, Batch> batchMap, Map<String, Table> tableMap) {
		for (Batch batch : batchMap.values()) {
			makeMapping(batch, tableMap);
			for (Batch subClass : batch.getSubClasses().values()) {
				makeMapping(subClass, tableMap);
			}
		}
	}

	/**
	 * メール定義にテーブル定義をマッピングします。
	 *
	 * @param mailMap メール定義書マップ
	 * @param tableMap テーブル定義書マップ
	 */
	public static void makeMailMapping(Map<String, Mail> mailMap, Map<String, Table> tableMap) {
		for (Mail mail : mailMap.values()) {
			makeMapping(mail, tableMap);
			for (Mail subClass : mail.getSubClasses().values()) {
				makeMapping(subClass, tableMap);
			}
		}
	}

	/**
	 * Web定義にテーブル定義をマッピングします。
	 *
	 * @param web Web定義オブジェクト
	 * @param tableMap テーブル定義書マップ
	 */
	public static void makeMapping(Web web, Map<String, Table> tableMap) {
		if (MapUtils.isEmpty(tableMap)) {
			return;
		}

		for (Field field : web.getFieldMap().values()) {
			if (StringUtils.isNotEmpty(field.getRefEntity())) {
				Table table = (Table) tableMap.get(field.getRefEntity());
				if (table == null) {
					throw new RuntimeException("参照エンティティが見つかりません。"
							+ web.getEntityName() + ":"
							+ field.getAttributeName());
				}
				Mapping mapping = new Mapping();
				mapping.setDtoField(field.getFieldName());
				mapping.setDtoFieldType(field.getType());
				Column refField = table.getColumnMap().get(field.getRefField());
				if (refField == null) {
					throw new RuntimeException("参照フィールドが見つかりません。"
							+ web.getEntityName() + ":"
							+ field.getAttributeName());
				}
				mapping.setEntityField(refField.getFieldName());
				mapping.setDateString(field.getDateFormat());
				if (web.getMapping().get(table.getClassName()) == null) {
					web.getMapping().put(table.getClassName(), new HashMap<String, Mapping>());
				}
				web.getMapping().get(table.getClassName()).put(field.getFieldName(), mapping);
			}
		}
	}

	/**
	 * バッチ定義にテーブル定義をマッピングします。
	 *
	 * @param batch バッチ定義オブジェクト
	 * @param tableMap テーブル定義書マップ
	 */
	public static void makeMapping(Batch batch, Map<String, Table> tableMap) {
		if (MapUtils.isEmpty(tableMap)) {
			return;
		}

		for (org.danboh.mapper.batch.Field field : batch.getFieldMap().values()) {
			if (StringUtils.isNotEmpty(field.getRefEntity())) {
				Table table = (Table) tableMap.get(field.getRefEntity());
				if (table == null) {
					throw new RuntimeException("参照エンティティが見つかりません。"
							+ batch.getEntityName() + ":"
							+ field.getAttributeName());
				}
				Mapping mapping = new Mapping();
				mapping.setDtoField(field.getFieldName());
				mapping.setDtoFieldType(field.getType());
				Column refField = table.getColumnMap().get(field.getRefField());
				if (refField == null) {
					throw new RuntimeException("参照フィールドが見つかりません。"
							+ batch.getEntityName() + ":"
							+ field.getAttributeName());
				}
				mapping.setEntityField(refField.getFieldName());
				if (batch.getMapping().get(table.getClassName()) == null) {
					batch.getMapping().put(table.getClassName(), new HashMap<String, Mapping>());
				}
				batch.getMapping().get(table.getClassName()).put(field.getFieldName(), mapping);
			}
		}
	}


	/**
	 * メール定義にテーブル定義をマッピングします。
	 *
	 * @param mail メール定義オブジェクト
	 * @param tableMap テーブル定義書マップ
	 */
	public static void makeMapping(Mail mail, Map<String, Table> tableMap) {
		if (MapUtils.isEmpty(tableMap)) {
			return;
		}

		for (org.danboh.mapper.mail.Field field : mail.getFieldMap().values()) {
			if (StringUtils.isNotEmpty(field.getRefEntity())) {
				Table table = (Table) tableMap.get(field.getRefEntity());
				if (table == null) {
					throw new RuntimeException("参照エンティティが見つかりません。"
							+ mail.getEntityName() + ":"
							+ field.getAttributeName());
				}
				Mapping mapping = new Mapping();
				mapping.setDtoField(field.getFieldName());
				mapping.setDtoFieldType(field.getType());
				Column refField = table.getColumnMap().get(field.getRefField());
				if (refField == null) {
					throw new RuntimeException("参照フィールドが見つかりません。"
							+ mail.getEntityName() + ":"
							+ field.getAttributeName());
				}
				mapping.setEntityField(refField.getFieldName());
				if (mail.getMapping().get(table.getClassName()) == null) {
					mail.getMapping().put(table.getClassName(), new HashMap<String, Mapping>());
				}
				mail.getMapping().get(table.getClassName()).put(field.getFieldName(), mapping);
			}
		}
	}

	/**
	 * Enumsの相対パスを取得します。
	 *
	 * @param enumsMap Enumマップ
	 * @param code Enum名
	 * @return enums配下のパス
	 */
	public static String getEnumsRelativePath(Map<String, Enums> enumsMap, String code) {
		Enums enums = enumsMap.get(code);
		if (enums == null) {
			throw new RuntimeException("Enumが見つかりません。：" + code);
		}
		StringBuilder builder = new StringBuilder();
		builder.append("enums").append(".");
		if (StringUtils.isNotEmpty(enums.getCategoryName())) {
			builder.append(enums.getCategoryName()).append(".");
		}
		builder.append(enums.getClassName());
		return builder.toString();
	}

	/**
	 * Enumsの絶対パスを取得します。
	 *
	 * @param settingPackage 設定オブジェクトのパッケージ
	 * @param outputPackageRoot 出力オブジェクトのパッケージルート
	 * @param enumsMap Enumマップ
	 * @param code Enum名
	 * @return 絶対パス
	 */
	public static String getEnumsAbsolutePath(String settingPackage, String outputPackageRoot, Map<String, Enums> enumsMap, String code) {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isNotEmpty(outputPackageRoot)) {
			builder.append(outputPackageRoot);
		} else {
			builder.append(settingPackage);
		}
		builder.append(".");
		builder.append(getEnumsRelativePath(enumsMap, code));
		return builder.toString();
	}
}
