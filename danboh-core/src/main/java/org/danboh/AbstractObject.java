/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh;

import org.danboh.xml.jaxb.setting.Setting;

/**
 * 基底Objectクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class AbstractObject implements Constants {
	private Setting setting;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 */
	public AbstractObject(Setting setting) {
		this.setting = setting;
	}


	/**
	 * 設定オブジェクトを取得します。
	 *
	 * @return 設定オブジェクト
	 */
	protected Setting getSetting() {
		return setting;
	}
}
