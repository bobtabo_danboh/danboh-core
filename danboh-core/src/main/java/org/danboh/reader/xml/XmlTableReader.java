/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.xml;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.table.Column;
import org.danboh.mapper.table.Relation;
import org.danboh.mapper.table.Table;
import org.danboh.util.VelocityUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.ColumnField;
import org.danboh.xml.jaxb.definition.RelationField;
import org.danboh.xml.jaxb.definition.TableDefinition;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceType;
import org.danboh.xml.service.DefinitionTableService;

/**
 * テーブル定義XMLの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XmlTableReader extends AbstractXmlReader<Table> {
	private Map<String, Enums> enumsMap;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param enumsMap Enum定義マップ
	 */
	public XmlTableReader(Setting setting, Map<String, Enums> enumsMap) {
		super(setting);
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFilePath() {
		return getSetting().getRoot() + "/" + getSetting().getXml().getResource().getTable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Table> parse(File[] files) throws Exception {
		Map<String, Table> result = new LinkedHashMap<String, Table>();

		DefinitionTableService service = ServiceLocator.getDefinitionTableService();
		for (File file : files) {
			TableDefinition definition = service.config(file);

			Table table = new Table(getSetting());
			table.setEntityName(definition.getName());
			table.setTableName(definition.getTableName());
			table.setClassName(VelocityUtil.largeOne(VelocityUtil.toCamelCase(table.getTableName().toLowerCase(), '_')));
			// TODO スキーマ名
			table.setSchemaName(EMPTY);

			// TODO 読み取り専用
			table.setReadOnly(false);

			if (definition.getCols() != null) {
				Output enumOutput = null;
				for (Output output : getSetting().getOutputs().getOutput()) {
					if (output.getType().ordinal() == SourceType.ENUM.ordinal()) {
						enumOutput = output;
						break;
					}
				}

				for (ColumnField colField : definition.getCols().getCol()) {
					Column column = new Column(getSetting(), this.enumsMap, enumOutput);
					column.setAttributeName(colField.getName());
					column.setColumnName(colField.getColumn());
					column.setFieldName(colField.getField());
					if (StringUtils.isEmpty(column.getFieldName())) {
						column.setFieldName(VelocityUtil.toCamelCase(column.getColumnName().toLowerCase(), '_'));
					}
					column.setType(colField.getType());
					column.setIntNum(colField.getInt());
					column.setDecNum(colField.getDec());
					column.setPrimaryKey(colField.isPk());
					column.setUnsigned(colField.isUnsigned());
					column.setNotNull(colField.isNotNull());
					column.setIdentity(colField.isAutoIncrement());
					column.setDefaultValue(colField.getDefaultValue());
					column.setCode(colField.getEnums());
					column.setSequenceName(colField.getSequence());
					column.setUnique(colField.getUnique());
					column.setIndex1(colField.getIndex1());
					column.setIndex2(colField.getIndex2());
					column.setIndex3(colField.getIndex3());
					column.setIndex4(colField.getIndex4());
					column.setIndex5(colField.getIndex5());

					table.add(column);
				}

				if (definition.getRelations() != null) {
					for (RelationField relationField : definition.getRelations().getRelation()) {
						Relation relation = new Relation(table, result);
						relation.setEntityName(relationField.getEntityName());
						relation.setMultiplicity(relationField.getMultiplicity());
						relation.setFieldName(relationField.getFieldName());
						relation.setOriginalFieldName(relationField.getFieldName());
						relation.setMappedBy(relationField.getMappedBy());
						relation.setJoinColumn(relationField.getJoinColumn());
						relation.setClasspath(relationField.getClasspath());
						relation.setRelation(true);
						relation.setTableName(VelocityUtil.toSnake(relation.getFieldName()));
						table.getRelationList().add(relation);
					}
				}
			}

			result.put(table.getKey(), table);
		}

		return result;
	}
}
