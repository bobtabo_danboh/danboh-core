/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.xml;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.danboh.xml.jaxb.definition.ActionEventField;
import org.danboh.xml.jaxb.definition.ActionField;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.MessageField;
import org.danboh.xml.jaxb.definition.ValidateField;
import org.danboh.xml.jaxb.definition.WebDefinition;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionWebService;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.table.Table;
import org.danboh.mapper.web.Action;
import org.danboh.mapper.web.Event;
import org.danboh.mapper.web.Field;
import org.danboh.mapper.web.Message;
import org.danboh.mapper.web.Web;
import org.danboh.util.MappingUtil;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;

/**
 * Web定義XMLの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XmlWebReader extends AbstractXmlReader<Web> {
	private Map<String, Table> tableMap;

	private Map<String, Enums> enumsMap;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param tableMap テーブル定義マップ
	 * @param enumsMap Enum定義マップ
	 */
	public XmlWebReader(Setting setting, Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		super(setting);
		this.tableMap = tableMap;
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFilePath() {
		return getSetting().getRoot() + "/" + getSetting().getXml().getResource().getWeb();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Web> parse(File[] files) throws Exception {
		Map<String, Web> result = new LinkedHashMap<String, Web>();

		DefinitionWebService service = ServiceLocator.getDefinitionWebService();
		for (File file : files) {
			WebDefinition definition = service.config(file);

			Web web = new Web();
			web.setEntityName(definition.getDto().getName());
			web.setClassName(definition.getDto().getClassName());
			web.setCategoryName(definition.getDto().getCategory());

			if (definition.getDto().getProperties() != null) {
				for (DtoPropertyField dtoPropertyField : definition.getDto().getProperties().getProperty()) {
					Field field = new Field(this.enumsMap);
					field.setAttributeName(dtoPropertyField.getName());
					field.setFieldName(dtoPropertyField.getField());
					field.setType(dtoPropertyField.getType());
					field.setDefaultValue(dtoPropertyField.getDefaultValue());
					field.setCode(dtoPropertyField.getEnum());
					field.setRefEntity(dtoPropertyField.getTable());
					field.setRefField(dtoPropertyField.getTableColumn());
					field.setInput(dtoPropertyField.isInput());
					web.add(field);
				}
			}

			if (definition.getSubDtos() != null) {
				for (DtoField dto : definition.getSubDtos().getDto()) {
					Web subWeb = new Web();
					subWeb.setEntityName(dto.getName());
					subWeb.setClassName(dto.getClassName());
					subWeb.setCategoryName(dto.getCategory());

					for (DtoPropertyField subDtoPropertyField : dto.getProperties().getProperty()) {
						Field subField = new Field(this.enumsMap);
						subField.setAttributeName(subDtoPropertyField.getName());
						subField.setFieldName(subDtoPropertyField.getField());
						subField.setType(subDtoPropertyField.getType());
						subField.setDefaultValue(subDtoPropertyField.getDefaultValue());
						subField.setCode(subDtoPropertyField.getEnum());
						subField.setRefEntity(subDtoPropertyField.getTable());
						subField.setRefField(subDtoPropertyField.getTableColumn());
						subField.setInput(subDtoPropertyField.isInput());
						subWeb.add(subField);
					}

					web.getSubClasses().put(subWeb.getClassName(), subWeb);
				}
			}

			if (definition.getActions() != null) {
				for (ActionField actionField : definition.getActions().getAction()) {
					Action action = new Action();
					action.setActionName(actionField.getName());
					action.setClassName(actionField.getClassName());
					action.setCategoryName(actionField.getCategory());

					if (actionField.getEvents() != null) {
						for (ActionEventField actionEventField : actionField.getEvents().getEvent()) {
							Event event = new Event();
							event.setEventName(actionEventField.getName());
							event.setBackward(actionEventField.getBackward());
							event.setMethodName(actionEventField.getMethod());
							event.setValidate(actionEventField.isValidate());
							if (actionEventField.getToken() != null) {
								event.setToken(actionEventField.getToken().name());
							}
							event.setForward(actionEventField.getForward());
							action.getEventList().add(event);
						}
					}

					web.getActionList().add(action);
				}
			}

			if (definition.getValidates() != null) {
				web.setHasValidation(true);
				web.setHasMessage(true);
				for (ValidateField validateField : definition.getValidates().getValidate()) {
					Field field = getValidateField(web, validateField.getName());
					field.setRequired(validateField.isRequired());
					field.setMinLength(NumberUtil.makeInt(validateField.getMin()));
					field.setMaxLength(NumberUtil.makeInt(validateField.getMax()));
					field.setDateFormat(validateField.getDateFormat());
					field.setPrecision(NumberUtil.makeInt(validateField.getPrecision()));
					field.setScale(NumberUtil.makeInt(validateField.getScale()));
					field.setEmail(validateField.isEmail());
					field.setUrl(validateField.isUrl());
					field.setRegexExpression(validateField.getRegex());
					field.setOgnlExpression(validateField.getOgnl());
					field.setAlphaNumeric(validateField.isAlphaNumeric());
					field.setNumeric(validateField.isNumeric());
					field.setOneByteKana(validateField.isHalfKana());
				}
			}

			if (definition.getMessages() != null) {
				web.setHasMessage(true);
				for (MessageField messageField : definition.getMessages().getMessage()) {
					Message message = new Message();
					message.setKey(messageField.getKey());
					message.setType(messageField.getType());
					message.setMessage(messageField.getMessage());
					web.getMessageList().add(message);
				}
			}

			result.put(web.getKey(), web);
		}

		MappingUtil.makeMapping(result, this.tableMap);
		return result;
	}

	/**
	 * 検証フィールドを取得します。
	 *
	 * @param web Web定義書オブジェクト
	 * @param fieldName 対象フィールド名
	 * @return 対象フィールド
	 */
	private Field getValidateField(Web web, String fieldName) {
		Field result = web.getFieldMap().get(fieldName);

		if (result == null) {
			for(Entry<String, Web> entry : web.getSubClasses().entrySet()) {
				result = entry.getValue().getFieldMap().get(fieldName);
				if (result != null) {
					break;
				}
			}
		}

		return result;
	}
}
