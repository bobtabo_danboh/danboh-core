/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.xml;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.danboh.mapper.batch.Arg;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.batch.Field;
import org.danboh.mapper.batch.Process;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.table.Table;
import org.danboh.util.MappingUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.BatchDefinition;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.ProcessArgField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionBatchService;

/**
 * バッチ定義XMLの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XmlBatchReader extends AbstractXmlReader<Batch> {
	private Map<String, Table> tableMap;

	private Map<String, Enums> enumsMap;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param tableMap テーブル定義マップ
	 * @param enumsMap Enum定義マップ
	 */
	public XmlBatchReader(Setting setting, Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		super(setting);
		this.tableMap = tableMap;
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFilePath() {
		return getSetting().getRoot() + "/" + getSetting().getXml().getResource().getBatch();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Batch> parse(File[] files) throws Exception {
		Map<String, Batch> result = new LinkedHashMap<String, Batch>();

		DefinitionBatchService service = ServiceLocator.getDefinitionBatchService();
		for (File file : files) {
			BatchDefinition definition = service.config(file);

			Batch batch = new Batch();
			batch.setEntityName(definition.getDto().getName());
			batch.setClassName(definition.getDto().getClassName());
			batch.setCategoryName(definition.getDto().getCategory());

			if (definition.getDto().getProperties() != null) {
				for (DtoPropertyField dtoPropertyField : definition.getDto().getProperties().getProperty()) {
					Field field = new Field(this.enumsMap);
					field.setAttributeName(dtoPropertyField.getName());
					field.setFieldName(dtoPropertyField.getField());
					field.setType(dtoPropertyField.getType());
					field.setDefaultValue(dtoPropertyField.getDefaultValue());
					field.setCode(dtoPropertyField.getEnum());
					field.setRefEntity(dtoPropertyField.getTable());
					field.setRefField(dtoPropertyField.getTableColumn());
					batch.add(field);
				}
			}

			if (definition.getSubDtos() != null) {
				for (DtoField dto : definition.getSubDtos().getDto()) {
					Batch subBatch = new Batch();
					subBatch.setEntityName(dto.getName());
					subBatch.setClassName(dto.getClassName());
					subBatch.setCategoryName(dto.getCategory());

					for (DtoPropertyField subDtoPropertyField : dto.getProperties().getProperty()) {
						Field subField = new Field(this.enumsMap);
						subField.setAttributeName(subDtoPropertyField.getName());
						subField.setFieldName(subDtoPropertyField.getField());
						subField.setType(subDtoPropertyField.getType());
						subField.setDefaultValue(subDtoPropertyField.getDefaultValue());
						subField.setCode(subDtoPropertyField.getEnum());
						subField.setRefEntity(subDtoPropertyField.getTable());
						subField.setRefField(subDtoPropertyField.getTableColumn());
						subBatch.add(subField);
					}

					batch.getSubClasses().put(subBatch.getClassName(), subBatch);
				}
			}

			if (definition.getProcess() != null) {
				Process process = new Process();
				process.setProcessName(definition.getProcess().getName());
				process.setMethodName(definition.getProcess().getMethod());
				process.setResultType(definition.getProcess().getResultType());
				process.setResultName(definition.getProcess().getResult());

				for (ProcessArgField argField : definition.getProcess().getArg()) {
					Arg arg = new Arg();
					arg.setArgName(argField.getName());
					arg.setArgType(argField.getType());
					arg.setArgField(argField.getProperty());
					process.getArgList().add(arg);
				}
			}

			result.put(batch.getKey(), batch);
		}

		MappingUtil.makeBatchMapping(result, this.tableMap);
		return result;
	}
}
