/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.danboh.AbstractObject;
import org.danboh.reader.Readable;
import org.danboh.util.FileUtil;
import org.danboh.xml.jaxb.setting.Setting;

/**
 * XML定義読込の基底クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 * @param <V> 読込クラス
 */
public abstract class AbstractXmlReader<V> extends AbstractObject implements Readable<V> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 */
	public AbstractXmlReader(Setting setting) {
		super(setting);
	}

	/**
	 * @return filePath
	 */
	public abstract String getFilePath();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, V> parse() throws Exception {
		Map<String, V> result = null;

		try {
			File[] files = getFiles();
			result = parse(files);
		} catch (FileNotFoundException e) {
			result = new LinkedHashMap<String, V>();
		}

		return result;
	}

	/**
	 * 対象ファイルを取得します。
	 *
	 * @return 対象ファイル配列
	 * @throws FileNotFoundException 対象ファイルが見つからない場合にスローされる例外です
	 */
	protected File[] getFiles() throws FileNotFoundException {
		return FileUtil.getFiles(getFilePath());
	}

	/**
	 * XML定義書を解析します。
	 *
	 * @param files XML定義書ファイル外列
	 * @return 定義書マップ
	 * @throws Exception 解析失敗時にスローされる例外です
	 */
	protected abstract Map<String, V> parse(File[] files) throws Exception;
 }
