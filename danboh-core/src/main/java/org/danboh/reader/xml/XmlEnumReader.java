/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.xml;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.danboh.xml.jaxb.definition.EnumDefinition;
import org.danboh.xml.jaxb.definition.EnumValueField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionEnumService;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.enums.Field;
import org.danboh.xml.ServiceLocator;

/**
 * Enum定義XMLの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XmlEnumReader extends AbstractXmlReader<Enums> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 */
	public XmlEnumReader(Setting setting) {
		super(setting);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFilePath() {
		return getSetting().getRoot() + "/" + getSetting().getXml().getResource().getEnum();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Enums> parse(File[] files) throws Exception {
		Map<String, Enums> result = new LinkedHashMap<String, Enums>();

		DefinitionEnumService service = ServiceLocator.getDefinitionEnumService();
		for (File file : files) {
			EnumDefinition definition = service.config(file);

			Enums enums = new Enums();
			enums.setEntityName(definition.getEnum().getName());
			enums.setClassName(definition.getEnum().getClassName());
			enums.setCategoryName(definition.getEnum().getCategory());

			if (definition.getEnum().getValues() != null) {
				for (EnumValueField enumValueField : definition.getEnum().getValues().getValue()) {
					Field field = new Field(null);
					field.setAttributeName(enumValueField.getLabel());
					field.setFieldName(enumValueField.getKey());
					field.setDefaultValue(String.valueOf(enumValueField.getValue()));
					enums.add(field);
				}
			}

			if (StringUtils.isNotEmpty(enums.getClassName())) {
				for (Iterator<Enums> iterator = result.values().iterator(); iterator.hasNext();) {
					Enums element = iterator.next();
					validate(enums, element);
				}
			}

			result.put(enums.getKey(), enums);
		}

		return result;
	}

	/**
	 *
	 * @param value
	 * @param element
	 */
	protected void validate(Enums value, Enums element) {
		if (value.getEntityName().equals(
				element.getEntityName())) {
			throw new RuntimeException("日本語名称が重複しています。"
					+ value.getEntityName() + ":"
					+ element.getEntityName() + " / "
					+ value.getClassName() + ":"
					+ element.getClassName());
		}

		if (value.getClassName().equals(element.getClassName())) {
			throw new RuntimeException("クラス名が重複しています。"
					+ value.getEntityName() + ":"
					+ element.getEntityName() + " / "
					+ value.getClassName() + ":"
					+ element.getClassName());
		}
	}
}
