/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.xml;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.MailDefinition;
import org.danboh.xml.jaxb.definition.SendField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionMailService;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.mail.Field;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.mail.Send;
import org.danboh.mapper.table.Table;
import org.danboh.util.MappingUtil;
import org.danboh.xml.ServiceLocator;

/**
 * メール定義XMLの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XmlMailReader extends AbstractXmlReader<Mail> {
	private Map<String, Table> tableMap;

	private Map<String, Enums> enumsMap;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param tableMap テーブル定義マップ
	 * @param enumsMap Enum定義マップ
	 */
	public XmlMailReader(Setting setting, Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		super(setting);
		this.tableMap = tableMap;
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFilePath() {
		return getSetting().getRoot() + "/" + getSetting().getXml().getResource().getMail();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Mail> parse(File[] files) throws Exception {
		Map<String, Mail> result = new LinkedHashMap<String, Mail>();

		DefinitionMailService service = ServiceLocator.getDefinitionMailService();
		for (File file : files) {
			MailDefinition definition = service.config(file);

			Mail mail = new Mail();
			mail.setEntityName(definition.getDto().getName());
			mail.setClassName(definition.getDto().getClassName());
			mail.setCategoryName(definition.getDto().getCategory());

			for (DtoPropertyField dtoPropertyField : definition.getDto().getProperties().getProperty()) {
				Field field = new Field(this.enumsMap);
				field.setAttributeName(dtoPropertyField.getName());
				field.setFieldName(dtoPropertyField.getField());
				field.setType(dtoPropertyField.getType());
				field.setDefaultValue(dtoPropertyField.getDefaultValue());
				field.setCode(dtoPropertyField.getEnum());
				field.setRefEntity(dtoPropertyField.getTable());
				field.setRefField(dtoPropertyField.getTableColumn());
				mail.add(field);
			}

			if (definition.getSubDtos() != null) {
				for (DtoField dto : definition.getSubDtos().getDto()) {
					Mail subMail = new Mail();
					subMail.setEntityName(dto.getName());
					subMail.setClassName(dto.getClassName());
					subMail.setCategoryName(dto.getCategory());

					for (DtoPropertyField subDtoPropertyField : dto.getProperties().getProperty()) {
						Field subField = new Field(this.enumsMap);
						subField.setAttributeName(subDtoPropertyField.getName());
						subField.setFieldName(subDtoPropertyField.getField());
						subField.setType(subDtoPropertyField.getType());
						subField.setDefaultValue(subDtoPropertyField.getDefaultValue());
						subField.setCode(subDtoPropertyField.getEnum());
						subField.setRefEntity(subDtoPropertyField.getTable());
						subField.setRefField(subDtoPropertyField.getTableColumn());
						subMail.add(subField);
					}

					mail.getSubClasses().put(subMail.getClassName(), subMail);
				}
			}

			for (SendField sendField : definition.getSend()) {
				Send send = new Send();
				send.setProcessName(sendField.getName());
				send.setMethodName(sendField.getMethod());
				mail.getSendList().add(send);
			}

			result.put(mail.getKey(), mail);
		}

		MappingUtil.makeMailMapping(result, this.tableMap);
		return result;
	}
}
