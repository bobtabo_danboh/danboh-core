/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.google;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.batch.Arg;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.batch.Field;
import org.danboh.mapper.batch.Process;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.table.Table;
import org.danboh.util.MappingUtil;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;

import com.google.api.services.sheets.v4.Sheets;

/**
 * バッチ定義Googleスプレッドの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class GoogleBatchReader extends AbstractGoogleReader<Batch> {

	private Map<String, Table> tableMap;

	private Map<String, Enums> enumsMap;

	private enum SectionType {
		INOUT, PROCESS
	}

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 * @param tableMap テーブル定義マップ
	 * @param enumsMap Enum定義マップ
	 */
	public GoogleBatchReader(Setting setting, Sheets sheets, Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		super(setting, sheets);
		this.tableMap = tableMap;
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<Spread> getSpreads() {
		return getSetting().getGoogle().getBatch().getSpread();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getSkipSheets() {
		return StringUtils.split(getSetting().getGoogle().getBatch().getSkip(), ",");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Batch> parse() throws Exception {
		Map<String, Batch> result = super.parse();
		MappingUtil.makeBatchMapping(result, tableMap);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Batch getSheetValue(List<List<Object>> details) throws Exception {
		Batch batch = new Batch();

		SectionType section = null;
		Batch subClass = null;
		boolean sub = false;
		for (List<Object> row : details) {
			if (CollectionUtils.isEmpty(row)) {
				section = null;
				if (sub) {
					batch.getSubClasses().put(subClass.getClassName(), subClass);
					sub = false;
					subClass = null;
				}
				continue;
			}

			if (section == null) {
				section = getSection(row);
				continue;
			}

			switch (section) {
				case INOUT:
					if ("日本語名".equals(getRow(row, 0, EMPTY))) {
						if (sub) {
							batch.getSubClasses().put(subClass.getClassName(), subClass);
							sub = false;
							subClass = null;
						}

						sub = MapUtils.isNotEmpty(batch.getFieldMap());
						if (sub) {
							if (subClass == null) {
								subClass = new Batch();
								subClass.setEntityName(getRow(row, 1, EMPTY));
								subClass.setClassName(getRow(row, 3, EMPTY));
								subClass.setCategoryName(getRow(row, 5, EMPTY));
							}
						} else {
							batch.setEntityName(getRow(row, 1, EMPTY));
							batch.setClassName(getRow(row, 3, EMPTY));
							batch.setCategoryName(getRow(row, 5, EMPTY));
						}
					} else if ("フィールド名".equals(getRow(row, 0, EMPTY))) {
						continue;
					} else {
						Field field = new Field(enumsMap);
						field.setAttributeName(getRow(row, 0, EMPTY));
						field.setFieldName(getRow(row, 1, EMPTY));
						field.setType(getRow(row, 2, EMPTY));
						field.setDefaultValue(getRow(row, 3, EMPTY));
						field.setRefEntity(getRow(row, 4, EMPTY));
						field.setRefField(getRow(row, 5, EMPTY));
						field.setCode(getRow(row, 6, EMPTY));
						if (sub) {
							subClass.add(field);
						} else {
							batch.add(field);
						}
					}
					break;
				case PROCESS:
					if ("処理名".equals(row.get(0))) {
						Process process = new Process();
						process.setProcessName(getRow(row, 1, EMPTY));
						process.setMethodName(getRow(row, 3, EMPTY));
						process.setResultName(getRow(row, 5, EMPTY));
						process.setResultType(getRow(row, 7, EMPTY));
						batch.setProcess(process);
					} else if ("引数名".equals(getRow(row, 0, EMPTY))) {
						continue;
					} else {
						Arg arg = new Arg();
						arg.setArgName(getRow(row, 0, EMPTY));
						arg.setArgField(getRow(row, 1, EMPTY));
						arg.setArgType(getRow(row, 2, EMPTY));
						batch.getProcess().getArgList().add(arg);
					}
					break;
				}
		}

		return batch;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validate(Batch value, Batch element) {
		if (value == null || element == null) {
			return;
		}

		if (value.getCategoryName().equals(
				element.getCategoryName())) {
			if (value.getEntityName().equals(
					element.getEntityName())) {
				throw new RuntimeException("日本語名称が重複しています。"
						+ value.getEntityName() + ":"
						+ element.getEntityName() + " / "
						+ value.getClassName() + ":"
						+ element.getClassName());
			}

			if (value.getClassName().equals(
					element.getClassName())) {
				throw new RuntimeException("クラス名が重複しています。"
						+ value.getEntityName() + ":"
						+ element.getEntityName() + " / "
						+ value.getClassName() + ":"
						+ element.getClassName());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getRange() {
		return ResourceBundle.getBundle("google").getString("range.batch");
	}

	/**
	 * 定義項目を取得します。
	 *
	 * @param row 行
	 * @return 定義項目Enum
	 */
	private SectionType getSection(List<Object> row) {
		if ("入出力項目".equals(row.get(0))) {
			return SectionType.INOUT;
		} else if ("処理".equals(row.get(0))) {
			return SectionType.PROCESS;
		}
		return null;
	}
}
