/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.google;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.table.Column;
import org.danboh.mapper.table.Relation;
import org.danboh.mapper.table.Table;
import org.danboh.util.NumberUtil;
import org.danboh.util.VelocityUtil;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceType;
import org.danboh.xml.jaxb.setting.Spread;

import com.google.api.services.sheets.v4.Sheets;

/**
 * テーブル定義Googleスプレッドの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class GoogleTableReader extends AbstractGoogleReader<Table> {
	private Map<String, Enums> enumsMap;

	private enum SectionType {
		ENTITY, ATTRIBUTE, RELATION
	}

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 * @param enumsMap Enum定義マップ
	 */
	public GoogleTableReader(Setting setting, Sheets sheets, Map<String, Enums> enumsMap) {
		super(setting, sheets);
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<Spread> getSpreads() {
		return getSetting().getGoogle().getTable().getSpread();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getSkipSheets() {
		return StringUtils.split(getSetting().getGoogle().getTable().getSkip(), ",");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Table getSheetValue(List<List<Object>> details) throws Exception {
		Map<String, Table> tableMap = new LinkedHashMap<String, Table>();
		Table table = new Table(getSetting());

		SectionType section = null;
		for (List<Object> row : details) {
			if (CollectionUtils.isEmpty(row)) {
				section = null;
				continue;
			}

			if (section == null) {
				section = getSection(row);
				if (!SectionType.ENTITY.equals(section)) {
					continue;
				}
			}

			switch (section) {
				case ENTITY:
					table.setEntityName(getRow(row, 1, EMPTY));
					table.setTableName(getRow(row, 3, EMPTY));
					table.setClassName(getRow(row, 5, EMPTY));
					if (table.getClassName().equals(EMPTY)) {
						table.setClassName(VelocityUtil.largeOne(VelocityUtil.toCamelCase(
								table.getTableName().toLowerCase(), '_')));
					}
					// スキーマ名
					table.setSchemaName(getRow(row, 9, EMPTY));

					// 読み取り専用
					table.setReadOnly(ON.equals(getRow(row, 11, OFF)));

					if (StringUtils.isEmpty(table.getEntityName())) {
						return table;
					}
					section = null;
					break;
				case ATTRIBUTE:
					Output enumOutput = null;
					for (Output output : getSetting().getOutputs().getOutput()) {
						if (output.getType().ordinal() == SourceType.ENUM.ordinal()) {
							enumOutput = output;
							break;
						}
					}

					Column column = new Column(getSetting(), enumsMap, enumOutput);

					// 属性名
					column.setAttributeName(getRow(row, 0, EMPTY));
					// カラム名
					column.setColumnName(getRow(row, 1, EMPTY));
					// フィールド名
					column.setFieldName(getRow(row, 2, EMPTY));
					if (column.getFieldName().equals(EMPTY)) {
						column.setFieldName(VelocityUtil.toCamelCase(column.getColumnName()
								.toLowerCase(), '_'));
					}
					// 型
					column.setType(getRow(row, 3, EMPTY));
					// 整数桁
					column.setIntNum(NumberUtil.makeInt(getRow(row, 4, EMPTY)));
					// 小数桁
					column.setDecNum(NumberUtil.makeInt(getRow(row, 5, EMPTY)));
					// 主キー
					column.setPrimaryKey(ON.equals(getRow(row, 6, OFF)));
					// UNSIGNED
					column.setUnsigned(ON.equals(getRow(row, 7, OFF)));
					// 非NULL
					column.setNotNull(ON.equals(getRow(row, 8, OFF)));
					// 自動採番
					column.setIdentity(ON.equals(getRow(row, 9, OFF)));
					// デフォルト値
					column.setDefaultValue(getRow(row, 10, EMPTY));
					// コード
					column.setCode(getRow(row, 11, EMPTY));
					// シーケンス名
					column.setSequenceName(getRow(row, 12, EMPTY));
					//
					column.setUnique(getRow(row, 13, EMPTY));
					//
					column.setIndex1(getRow(row, 14, EMPTY));
					//
					column.setIndex2(getRow(row, 15, EMPTY));
					//
					column.setIndex3(getRow(row, 16, EMPTY));
					//
					column.setIndex4(getRow(row, 17, EMPTY));
					//
					column.setIndex5(getRow(row, 18, EMPTY));

					table.add(column);
					break;
				case RELATION:
					Relation relation = new Relation(table, tableMap);
					table.getRelationList().add(relation);

					relation.setEntityName(getRow(row, 0, EMPTY));
					relation.setMultiplicity(getRow(row, 1, EMPTY));
					relation.setFieldName(getRow(row, 2, EMPTY));
					relation.setOriginalFieldName(getRow(row, 2, EMPTY));
					relation.setMappedBy(getRow(row, 3, EMPTY));
					relation.setJoinColumn(getRow(row, 4, EMPTY));
					relation.setClasspath(getRow(row, 5, EMPTY));
					relation.setRelation(EMPTY.equals(getRow(row, 6, EMPTY)));
					relation.setTableName(VelocityUtil.toSnake(relation.getFieldName()));

					break;
			}
		}

		return table;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validate(Table value, Table element) {
		//TODO 何かある？
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getRange() {
		return ResourceBundle.getBundle("google").getString("range.table");
	}

	/**
	 * 定義項目を取得します。
	 *
	 * @param row 行
	 * @return 定義項目Enum
	 */
	private SectionType getSection(List<Object> row) {
		if ("エンティティ名".equals(row.get(0))) {
			return SectionType.ENTITY;
		} else if ("属性名".equals(row.get(0))) {
			return SectionType.ATTRIBUTE;
		} else if ("関連エンティティ名".equals(row.get(0))) {
			return SectionType.RELATION;
		}
		return null;
	}
}
