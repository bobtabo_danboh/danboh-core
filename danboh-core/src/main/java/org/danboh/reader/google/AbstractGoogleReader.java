/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.google;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.google.AbstractGoogleObject;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;
import org.danboh.mapper.AbstractSelect;
import org.danboh.mapper.Selectable;
import org.danboh.reader.Readable;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;

/**
 * Googleスプレッド定義読込の基底クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 * @param <V> 読込クラス
 */
public abstract class AbstractGoogleReader<V> extends AbstractGoogleObject implements Readable<V> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public AbstractGoogleReader(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, V> parse() throws Exception {
		Map<String, V> result = new LinkedHashMap<String, V>();

		List<Spread> spreads = getSpreads();
		String[] skipSheets = getSkipSheets();
		for (Spread spread : spreads) {
			if (StringUtils.isEmpty(spread.getId())) {
				continue;
			}

			Spreadsheet spreadsheet = getAccessor().getSpreadSheets(spread.getId());

			ObjectMapper mapper = new ObjectMapper();
			Map map = mapper.readValue(spreadsheet.toString(), new TypeReference<Map>() {
			});
			List<Map> sheets = (List<Map>) map.get("sheets");

			for (Map sheet : sheets) {
				Map properties = (Map) sheet.get("properties");
				String title = (String) properties.get("title");

				if (ArrayUtils.contains(skipSheets, title)) {
					continue;
				}

				List<List<Object>> details = getAccessor().getValues(spread.getId(), title, getRange());

				V value = getSheetValue(details);
				((AbstractSelect) value).setSheetName(title);

				if (StringUtils.isNotEmpty(((Selectable) value).getClassName())) {
					for (Iterator<V> iterator = result.values().iterator(); iterator.hasNext();) {
						V element = iterator.next();
						validate(value, element);
					}
				}

				result.put(((Selectable) value).getKey(), value);
			}
		}

		return result;
	}

	/**
	 *
	 * @return
	 */
	protected abstract List<Spread> getSpreads();

	/**
	 *
	 * @return
	 */
	protected abstract String[] getSkipSheets();

	/**
	 *
	 *
	 * @param details
	 * @return
	 * @throws Exception
	 */
	protected abstract V getSheetValue(List<List<Object>> details) throws Exception;

	/**
	 *
	 * @param row
	 * @param index
	 * @param defaultValue
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected <E> E getRow(List<Object> row, int index, E defaultValue) {
		return row.size() > index ? (E) row.get(index) : defaultValue;
	}

	/**
	 *
	 * @param value
	 * @param element
	 */
	protected abstract void validate(V value, V element);

	/**
	 *
	 * @return
	 */
	protected abstract String getRange();
}
