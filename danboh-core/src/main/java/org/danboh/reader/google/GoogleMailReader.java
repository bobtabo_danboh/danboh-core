/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.google;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.mail.Field;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.mail.Send;
import org.danboh.mapper.table.Table;
import org.danboh.util.MappingUtil;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;

import com.google.api.services.sheets.v4.Sheets;

/**
 * メール定義Googleスプレッドの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class GoogleMailReader extends AbstractGoogleReader<Mail> {

	private Map<String, Table> tableMap;

	private Map<String, Enums> enumsMap;

	private enum SectionType {
		MAIL, SEND
	}

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 * @param tableMap テーブル定義マップ
	 * @param enumsMap Enum定義マップ
	 */
	public GoogleMailReader(Setting setting, Sheets sheets, Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		super(setting, sheets);
		this.tableMap = tableMap;
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<Spread> getSpreads() {
		return getSetting().getGoogle().getMail().getSpread();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getSkipSheets() {
		return StringUtils.split(getSetting().getGoogle().getMail().getSkip(), ",");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Mail> parse() throws Exception {
		Map<String, Mail> result = super.parse();
		MappingUtil.makeMailMapping(result, tableMap);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Mail getSheetValue(List<List<Object>> details) throws Exception {
		Mail mail = new Mail();

		SectionType section = null;
		Mail subClass = null;
		boolean sub = false;
		for (List<Object> row : details) {
			if (CollectionUtils.isEmpty(row)) {
				section = null;
				if (sub) {
					mail.getSubClasses().put(subClass.getClassName(), subClass);
					sub = false;
					subClass = null;
				}
				continue;
			}

			if (section == null) {
				section = getSection(row);
				continue;
			}

			switch (section) {
				case MAIL:
					if ("日本語名".equals(getRow(row, 0, EMPTY))) {
						if (sub) {
							mail.getSubClasses().put(subClass.getClassName(), subClass);
							sub = false;
							subClass = null;
						}

						sub = MapUtils.isNotEmpty(mail.getFieldMap());
						if (sub) {
							if (subClass == null) {
								subClass = new Mail();
								subClass.setEntityName(getRow(row, 1, EMPTY));
								subClass.setClassName(getRow(row, 3, EMPTY));
								subClass.setCategoryName(getRow(row, 5, EMPTY));
							}
						} else {
							mail.setEntityName(getRow(row, 1, EMPTY));
							mail.setClassName(getRow(row, 3, EMPTY));
							mail.setCategoryName(getRow(row, 5, EMPTY));
						}
					} else if ("フィールド名".equals(getRow(row, 0, EMPTY))) {
						continue;
					} else {
						Field field = new Field(enumsMap);
						field.setAttributeName(getRow(row, 0, EMPTY));
						field.setFieldName(getRow(row, 1, EMPTY));
						field.setType(getRow(row, 2, EMPTY));
						field.setDefaultValue(getRow(row, 3, EMPTY));
						field.setRefEntity(getRow(row, 4, EMPTY));
						field.setRefField(getRow(row, 5, EMPTY));
						field.setCode(getRow(row, 6, EMPTY));
						if (sub) {
							subClass.add(field);
						} else {
							mail.add(field);
						}
					}
					break;
				case SEND:
					if ("処理名".equals(row.get(0))) {
						continue;
					} else {
						Send send = new Send();
						send.setProcessName(getRow(row, 0, EMPTY));
						send.setMethodName(getRow(row, 1, EMPTY));
						mail.getSendList().add(send);
					}
					break;
				}
		}

		return mail;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validate(Mail value, Mail element) {
		if (value == null || element == null) {
			return;
		}

		if (value.getCategoryName().equals(
				element.getCategoryName())) {
			if (value.getEntityName().equals(
					element.getEntityName())) {
				throw new RuntimeException("日本語名称が重複しています。"
						+ value.getEntityName() + ":"
						+ element.getEntityName() + " / "
						+ value.getClassName() + ":"
						+ element.getClassName());
			}

			if (value.getClassName().equals(
					element.getClassName())) {
				throw new RuntimeException("クラス名が重複しています。"
						+ value.getEntityName() + ":"
						+ element.getEntityName() + " / "
						+ value.getClassName() + ":"
						+ element.getClassName());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getRange() {
		return ResourceBundle.getBundle("google").getString("range.mail");
	}

	/**
	 * 定義項目を取得します。
	 *
	 * @param row 行
	 * @return 定義項目Enum
	 */
	private SectionType getSection(List<Object> row) {
		if ("メール項目".equals(row.get(0))) {
			return SectionType.MAIL;
		} else if ("送信".equals(row.get(0))) {
			return SectionType.SEND;
		}
		return null;
	}
}
