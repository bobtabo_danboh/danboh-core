/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.google;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.enums.Field;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;

import com.google.api.services.sheets.v4.Sheets;

/**
 * Enum定義Googleスプレッドの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class GoogleEnumReader extends AbstractGoogleReader<Enums> {

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public GoogleEnumReader(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<Spread> getSpreads() {
		return getSetting().getGoogle().getEnum().getSpread();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getSkipSheets() {
		return StringUtils.split(getSetting().getGoogle().getEnum().getSkip(), ",");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Enums getSheetValue(List<List<Object>> details) throws Exception {
		Enums enums = new Enums();

		for (List<Object> row : details) {
			if ("Enum".equals(getRow(row, 0, EMPTY))) {
				enums.setEntityName(getRow(row, 1, EMPTY));
				enums.setClassName(getRow(row, 2, EMPTY));
			} else if ("カテゴリ".equals(getRow(row, 0, EMPTY))) {
				enums.setCategoryName(getRow(row, 1, EMPTY));
			} else if ("日本語名".equals(getRow(row, 0, EMPTY))) {
				continue;
			} else {
				Field field = new Field(null);
				// フィールド名(日本語名)
				field.setAttributeName(getRow(row, 0, EMPTY));
				// フィールド名
				field.setFieldName(getRow(row, 1, EMPTY));
				// デフォルト値
				field.setDefaultValue(getRow(row, 2, EMPTY));
				enums.add(field);
			}
		}

		return enums;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validate(Enums value, Enums element) {
		if (value.getEntityName().equals(
				element.getEntityName())) {
			throw new RuntimeException("日本語名称が重複しています。"
					+ value.getEntityName() + ":"
					+ element.getEntityName() + " / "
					+ value.getClassName() + ":"
					+ element.getClassName());
		}

		if (value.getClassName().equals(element.getClassName())) {
			throw new RuntimeException("クラス名が重複しています。"
					+ value.getEntityName() + ":"
					+ element.getEntityName() + " / "
					+ value.getClassName() + ":"
					+ element.getClassName());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getRange() {
		return ResourceBundle.getBundle("google").getString("range.enum");
	}
}
