/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader.google;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.table.Table;
import org.danboh.mapper.web.Action;
import org.danboh.mapper.web.Event;
import org.danboh.mapper.web.Field;
import org.danboh.mapper.web.Message;
import org.danboh.mapper.web.Web;
import org.danboh.util.MappingUtil;
import org.danboh.util.NumberUtil;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;

import com.google.api.services.sheets.v4.Sheets;

/**
 * Web定義Googleスプレッドの読込クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class GoogleWebReader extends AbstractGoogleReader<Web> {

	private Map<String, Table> tableMap;

	private Map<String, Enums> enumsMap;

	private enum SectionType {
		DISPLAY, ACTION, VALIDATION, MESSAGE
	}

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 * @param tableMap テーブル定義マップ
	 * @param enumsMap Enum定義マップ
	 */
	public GoogleWebReader(Setting setting, Sheets sheets, Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		super(setting, sheets);
		this.tableMap = tableMap;
		this.enumsMap = enumsMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<Spread> getSpreads() {
		return getSetting().getGoogle().getWeb().getSpread();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getSkipSheets() {
		return StringUtils.split(getSetting().getGoogle().getWeb().getSkip(), ",");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Web> parse() throws Exception {
		Map<String, Web> result = super.parse();
		MappingUtil.makeMapping(result, tableMap);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Web getSheetValue(List<List<Object>> details) throws Exception {
		Web web = new Web();

		SectionType section = null;
		Web subClass = null;
		boolean sub = false;
		int actionIndex = -1;
		for (List<Object> row : details) {
			if (CollectionUtils.isEmpty(row)) {
				section = null;
				if (sub) {
					web.getSubClasses().put(subClass.getClassName(), subClass);
					sub = false;
					subClass = null;
				}
				continue;
			}

			if (section == null) {
				section = getSection(row);
				continue;
			}

			switch (section) {
				case DISPLAY:
					if ("日本語名".equals(getRow(row, 0, EMPTY))) {
						if (sub) {
							web.getSubClasses().put(subClass.getClassName(), subClass);
							sub = false;
							subClass = null;
						}

						sub = MapUtils.isNotEmpty(web.getFieldMap());
						if (sub) {
							if (subClass == null) {
								subClass = new Web();
								subClass.setEntityName(getRow(row, 1, EMPTY));
								subClass.setClassName(getRow(row, 3, EMPTY));
								subClass.setCategoryName(getRow(row, 5, EMPTY));
							}
						} else {
							web.setEntityName(getRow(row, 1, EMPTY));
							web.setClassName(getRow(row, 3, EMPTY));
							web.setCategoryName(getRow(row, 5, EMPTY));
						}
					} else if ("フィールド名".equals(getRow(row, 0, EMPTY))) {
						continue;
					} else {
						Field field = new Field(enumsMap);
						field.setAttributeName(getRow(row, 0, EMPTY));
						field.setFieldName(getRow(row, 1, EMPTY));
						field.setType(getRow(row, 2, EMPTY));
						field.setDefaultValue(getRow(row, 3, EMPTY));
						field.setRefEntity(getRow(row, 4, EMPTY));
						field.setRefField(getRow(row, 5, EMPTY));
						field.setCode(getRow(row, 6, EMPTY));
						field.setInput(ON.equals(getRow(row, 7, OFF)));
						if (sub) {
							subClass.add(field);
						} else {
							web.add(field);
						}
					}
					break;
				case ACTION:
					if ("アクション名".equals(row.get(0))) {
						Action action = new Action();
						action.setActionName(getRow(row, 1, EMPTY));
						action.setClassName(getRow(row, 3, EMPTY));
						action.setCategoryName(getRow(row, 5, EMPTY));
						web.getActionList().add(action);
						actionIndex++;
						continue;
					}

					if ("イベント名".equals(row.get(0))) {
						continue;
					}

					Event event = new Event();
					event.setEventName(getRow(row, 0, EMPTY));
					event.setBackward(getRow(row, 1, EMPTY));
					event.setMethodName(getRow(row, 2, EMPTY));
					event.setValidate(!EMPTY.equals(getRow(row, 3, EMPTY)));
					event.setToken(getRow(row, 4, EMPTY));
					event.setForward(getRow(row, 5, EMPTY));
					web.getActionList().get(actionIndex).getEventList().add(event);
					break;
				case VALIDATION:
					if ("フィールド名".equals(row.get(0))) {
						continue;
					}

					web.setHasValidation(true);
					web.setHasMessage(true);
					Field field = web.getFieldMap().get(row.get(0).toString());
					if (field == null) {
						throw new RuntimeException("参照先のフィールドが見つかりません"
								+ web.getEntityName() + ":"
								+ row.get(0).toString());
					}

					// 必須チェック
					field.setRequired(ON.equals(getRow(row, 1, OFF)));
					// 桁数チェック
					field.setMinLength(NumberUtil.makeInt(getRow(row, 2, EMPTY)));
					field.setMaxLength(NumberUtil.makeInt(getRow(row, 3, EMPTY)));
					// 形式チェック
					field.setDateFormat(getRow(row, 4, EMPTY));
					String decimalFormat = getRow(row, 5, EMPTY);
					if (!decimalFormat.equals(EMPTY)) {
						String[] decimalFormatItem = decimalFormat.split(",");
						field.setPrecision(Integer.parseInt(decimalFormatItem[0]));
						if (decimalFormatItem.length == 2) {
							field.setScale(Integer.parseInt(decimalFormatItem[1]));
						}
					}
					field.setEmail(ON.equals(getRow(row, 6, OFF)));
					field.setUrl(ON.equals(getRow(row, 7, OFF)));
					field.setRegexExpression(getRow(row, 8, EMPTY));
					field.setOgnlExpression(getRow(row, 9, EMPTY));
					field.setAlphaNumeric(ON.equals(getRow(row, 10, OFF)));
					field.setNumeric(ON.equals(getRow(row, 11, OFF)));
					field.setOneByteKana(ON.equals(getRow(row, 12, OFF)));
					break;
				case MESSAGE:
					if ("キー".equals(row.get(0))) {
						continue;
					}

					web.setHasMessage(true);
					Message message = new Message();
					message.setKey(getRow(row, 0, EMPTY));
					message.setType(getRow(row, 1, EMPTY));
					message.setMessage(getRow(row, 2, EMPTY));
					web.getMessageList().add(message);
					break;
				}
		}

		return web;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validate(Web value, Web element) {
		if (value == null || element == null) {
			return;
		}

		if (value.getCategoryName().equals(
				element.getCategoryName())) {
			if (value.getEntityName().equals(
					element.getEntityName())) {
				throw new RuntimeException("日本語名称が重複しています。"
						+ value.getEntityName() + ":"
						+ element.getEntityName() + " / "
						+ value.getClassName() + ":"
						+ element.getClassName());
			}

			if (value.getClassName().equals(
					element.getClassName())) {
				throw new RuntimeException("クラス名が重複しています。"
						+ value.getEntityName() + ":"
						+ element.getEntityName() + " / "
						+ value.getClassName() + ":"
						+ element.getClassName());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getRange() {
		return ResourceBundle.getBundle("google").getString("range.web");
	}

	/**
	 * 定義項目を取得します。
	 *
	 * @param row 行
	 * @return 定義項目Enum
	 */
	private SectionType getSection(List<Object> row) {
		if ("画面項目".equals(row.get(0))) {
			return SectionType.DISPLAY;
		} else if ("アクション".equals(row.get(0))) {
			return SectionType.ACTION;
		} else if ("入力チェック".equals(row.get(0))) {
			return SectionType.VALIDATION;
		} else if ("メッセージ".equals(row.get(0))) {
			return SectionType.MESSAGE;
		}
		return null;
	}
}
