/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader;

import java.util.Map;

/**
 * 定義読込を提供するインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 * @param <V> 読込クラス
 */
public interface Readable<V> {

	/**
	 * 解析します。
	 *
	 * @return 定義書マップ
	 */
	public Map<String, V> parse() throws Exception;
}
