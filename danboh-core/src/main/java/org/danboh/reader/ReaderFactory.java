/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.reader;

import java.util.Map;

import org.danboh.google.AbstractFactory;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceType;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.table.Table;
import org.danboh.mapper.web.Web;
import org.danboh.reader.google.GoogleBatchReader;
import org.danboh.reader.google.GoogleEnumReader;
import org.danboh.reader.google.GoogleMailReader;
import org.danboh.reader.google.GoogleTableReader;
import org.danboh.reader.google.GoogleWebReader;
import org.danboh.reader.xml.XmlBatchReader;
import org.danboh.reader.xml.XmlEnumReader;
import org.danboh.reader.xml.XmlMailReader;
import org.danboh.reader.xml.XmlTableReader;
import org.danboh.reader.xml.XmlWebReader;

/**
 * 定義オブジェクトを生成するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class ReaderFactory extends AbstractFactory {
	private static ReaderFactory instance = null;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param useGoogle GoogleAPI利用時は true を設定します
	 */
	private ReaderFactory(Setting setting, boolean useGoogle) {
		super(setting, useGoogle);
	}

	/**
	 * Web定義オブジェクトを取得します。
	 *
	 * @param tableMap テーブル定義オブジェクト
	 * @param enumsMap Enum定義マップ
	 * @return 定義オブジェクト
	 */
	public Readable<Web> getWebReader(Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		validate(SourceType.WEB);
		return useGoogle() ? new GoogleWebReader(getSetting(), getService(), tableMap, enumsMap) : new XmlWebReader(getSetting(), tableMap, enumsMap);
	}

	/**
	 * Enum定義オブジェクトを取得します。
	 *
	 * @return 定義オブジェクト
	 */
	public Readable<Enums> getEnumReader() {
		validate(SourceType.ENUM);
		return useGoogle() ? new GoogleEnumReader(getSetting(), getService()) : new XmlEnumReader(getSetting());
	}

	/**
	 * テーブル定義オブジェクトを取得します。
	 *
	 * @param enumsMap Enum定義マップ
	 * @return 定義オブジェクト
	 */
	public Readable<Table> getTableReader(Map<String, Enums> enumsMap) {
		validate(SourceType.TABLE);
		return useGoogle() ? new GoogleTableReader(getSetting(), getService(), enumsMap) : new XmlTableReader(getSetting(), enumsMap);
	}

	/**
	 * バッチ定義オブジェクトを取得します。
	 *
	 * @param tableMap テーブル定義オブジェクト
	 * @param enumsMap Enum定義マップ
	 * @return 定義オブジェクト
	 */
	public Readable<Batch> getBatchReader(Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		validate(SourceType.BATCH);
		return useGoogle() ? new GoogleBatchReader(getSetting(), getService(), tableMap, enumsMap) : new XmlBatchReader(getSetting(), tableMap, enumsMap);
	}

	/**
	 * バッチ定義オブジェクトを取得します。
	 *
	 * @param tableMap テーブル定義オブジェクト
	 * @param enumsMap Enum定義マップ
	 * @return 定義オブジェクト
	 */
	public Readable<Mail> getMailReader(Map<String, Table> tableMap, Map<String, Enums> enumsMap) {
		validate(SourceType.MAIL);
		return useGoogle() ? new GoogleMailReader(getSetting(), getService(), tableMap, enumsMap) : new XmlMailReader(getSetting(), tableMap, enumsMap);
	}

	/**
	 * 設定オブエクトを検証します。
	 *
	 * @param documentType 定義ファイル種類
	 */
	private void validate(SourceType sourceType) {
		if (useGoogle() && getSetting().getGoogle() == null) {
			throw new RuntimeException("Googleスプレッドが未設定です。");
		}

		//TODO ファイル種類のチェック
	}

	/**
	 * インスタンスを生成します。
	 *
	 * @param setting 設定オブジェクト
	 * @param useGoogle GoogleAPI利用時は true を設定します
	 * @return このクラスのインスタンス
	 */
	public static ReaderFactory getInstance(Setting setting, boolean useGoogle) {
		if (instance == null) {
			instance = new ReaderFactory(setting, useGoogle);
		}

		return instance;
	}
}