/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.google;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.util.TimeUtil;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.BatchUpdate;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.Get;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.Values.Update;
import com.google.api.services.sheets.v4.SheetsRequest;
import com.google.api.services.sheets.v4.model.AddSheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.DeleteSheetRequest;
import com.google.api.services.sheets.v4.model.MergeCellsRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.UpdateCellsRequest;
import com.google.api.services.sheets.v4.model.UpdateDimensionPropertiesRequest;
import com.google.api.services.sheets.v4.model.ValueRange;

/**
 * Googleスプレッドのアクセサクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 *
 */
public class GoogleSpreadAccessor {
	private Sheets sheets;

	/**
	 * コンストラクタ
	 * @param sheets Googleスプレッドサービス
	 */
	protected GoogleSpreadAccessor(Sheets sheets) {
		this.sheets = sheets;
	}

	/**
	 * スプレッドシートを取得します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @return スプレッドシート
	 * @throws IOException 入出力例外
	 */
	public Spreadsheet getSpreadSheets(String spreadsheetId) throws IOException {
		return getSpreadSheets(spreadsheetId, false, null);
	}

	/**
	 * スプレッドシートを取得します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param includeGridData セル情報を取得する場合 true を設定します
	 * @param ranges 範囲
	 * @return スプレッドシート
	 * @throws IOException 入出力例外
	 */
	public Spreadsheet getSpreadSheets(String spreadsheetId, boolean includeGridData, List<String> ranges) throws IOException {
		return getSpreadSheets(spreadsheetId, includeGridData, ranges, null);
	}

	/**
	 * スプレッドシートを取得します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param includeGridData セル情報を取得する場合 true を設定します
	 * @param ranges 範囲
	 * @return スプレッドシート
	 * @throws IOException 入出力例外
	 */
	public Spreadsheet getSpreadSheets(String spreadsheetId, boolean includeGridData, List<String> ranges, String fields) throws IOException {
		if (includeGridData && CollectionUtils.isEmpty(ranges)) {
			throw new RuntimeException("セル情報を取得時の範囲指定がされていません。");
		}

		Get request = this.sheets.spreadsheets().get(spreadsheetId);
		if (includeGridData) {
			request.setIncludeGridData(includeGridData);
			request.setFields("sheets");
		}
		if (StringUtils.isEmpty(fields)) {
			request.setFields("sheets");
		} else {
			request.setFields(fields);
		}
		if (CollectionUtils.isNotEmpty(ranges)) {
			request.setRanges(ranges);
		}

		Spreadsheet response = executeRequest(request);
		return response;
	}

	/**
	 * 指定範囲のスプレッドシート値を取得します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param sheetName シート名
	 * @param range 範囲
	 * @return スプレッドシート値リストのリスト
	 * @throws IOException 入出力例外
	 */
	public List<List<Object>> getValues(String spreadsheetId, String sheetName, String range)
			throws IOException {
		Sheets.Spreadsheets.Values.Get request = this.sheets.spreadsheets().values().get(spreadsheetId, sheetName + "!" + range);
		ValueRange response = request.execute();
		List<List<Object>> result = response.getValues();
		return result;
	}

	/**
	 * テンプレートシートを取得します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param ranges 範囲
	 * @return テンプレートシート
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map getTemplateSheet(String spreadsheetId, List<String> ranges) {
		Map result = new LinkedHashMap();

		try {
			Spreadsheet spreadsheet = getSpreadSheets(spreadsheetId, true, ranges, "sheets.data");
			result.put("Spreadsheet", spreadsheet);
			result.put("Sheets", spreadsheet.getSheets().get(0));
			result.put("data", (List)spreadsheet.getSheets().get(0).get("data"));
			result.put("GridData", ((List)result.get("data")).get(0));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * シートを削除します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param skipSheets 削除スキップシート名
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deleteSheets(String spreadsheetId, String... skipSheets) {
		List<Request> requests = new ArrayList<Request>();

		try {
			Spreadsheet spreadsheet = getSpreadSheets(spreadsheetId);
			ObjectMapper mapper = new ObjectMapper();
			Map map = mapper.readValue(spreadsheet.toString(), new TypeReference<Map>() {});
			List<Map> sheets = (List<Map>) map.get("sheets");

			for (Map sheet : sheets) {
				Map properties = (Map) sheet.get("properties");
				String title = (String) properties.get("title");

				if (ArrayUtils.contains(skipSheets, title)) {
					continue;
				}

				Integer targetSheetId = (Integer) properties.get("sheetId");
				DeleteSheetRequest deleteSheetRequest = new DeleteSheetRequest().setSheetId(targetSheetId);
				requests.add(new Request().setDeleteSheet(deleteSheetRequest));
			}

			if (CollectionUtils.isEmpty(requests)) {
				return;
			}

			batchUpdateSpreadsheet(spreadsheetId, requests);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * シートを追加します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param sheetName シート名
	 * @param range シート範囲
	 * @param contents シート内容
	 * @param adapter 書式設定アダプタ
	 * @throws IOException
	 */
	public void addSheet(String spreadsheetId, String sheetName, String range, List<List<Object>> contents, SheetAdapter adapter) throws IOException {
		//シートを追加します
		SheetProperties sheetProperties = new SheetProperties().setTitle(sheetName);
		AddSheetRequest addSheetRequest = new AddSheetRequest().setProperties(sheetProperties);
		List<Request> requests = new ArrayList<Request>();
		requests.add(new Request().setAddSheet(addSheetRequest));
		BatchUpdateSpreadsheetResponse batchResponse = batchUpdateSpreadsheet(spreadsheetId, requests);

		//追加シートIDを取得します
		Integer targetSheetId = batchResponse.getReplies().get(0).getAddSheet().getProperties().getSheetId();

		//シート内容を更新します
		ValueRange valueRange = new ValueRange();
		valueRange.setValues(contents);
		valueRange.setMajorDimension("ROWS");
		Update updateRequest = this.sheets.spreadsheets().values().update(spreadsheetId, sheetName + "!" + range, valueRange);
		updateRequest.setValueInputOption("USER_ENTERED");
		executeRequest(updateRequest);

		//列幅を調整します
		requests.clear();
		List<UpdateDimensionPropertiesRequest> dimensdions = adapter.getUpdateDimensionPropertiesRequest(targetSheetId);
		for (UpdateDimensionPropertiesRequest dimensdion : dimensdions) {
			requests.add(new Request().setUpdateDimensionProperties(dimensdion));
		}

		//セルの書式設定します
		List<UpdateCellsRequest> updateCells = adapter.getUpdateCellsRequest(targetSheetId, contents);
		for (UpdateCellsRequest updateCell : updateCells) {
			requests.add(new Request().setUpdateCells(updateCell));
		}

		//セルを結合します
		List<MergeCellsRequest> mergeCells = adapter.getMergeCellsRequest(targetSheetId, contents);
		if (CollectionUtils.isNotEmpty(mergeCells)) {
			for (MergeCellsRequest mergeCell : mergeCells) {
				requests.add(new Request().setMergeCells(mergeCell));
			}
		}

		batchUpdateSpreadsheet(spreadsheetId, requests);
	}

	/**
	 * 一括リクエストします。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param requests リクエストのリスト
	 * @return レスポンス
	 */
	private <T> T batchUpdateSpreadsheet(String spreadsheetId, List<Request> requests) {
		try {
			BatchUpdateSpreadsheetRequest spreadsheetRequest = new BatchUpdateSpreadsheetRequest();
			spreadsheetRequest.setRequests(requests);
			BatchUpdate batchUpdate = this.sheets.spreadsheets().batchUpdate(spreadsheetId, spreadsheetRequest);
			return executeRequest(batchUpdate);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 連続リクエスト防止で、スリープ後にリクエストします。
	 *
	 * @param request リクエスト
	 * @return レスポンス
	 */
	@SuppressWarnings("unchecked")
	private <T> T executeRequest(SheetsRequest<?> request) {
		try {
			TimeUtil.sleepSeconds(1);
			return (T) request.execute();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
