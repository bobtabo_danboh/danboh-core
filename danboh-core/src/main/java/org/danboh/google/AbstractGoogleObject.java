/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.google;

import org.danboh.AbstractObject;
import org.danboh.xml.jaxb.setting.Setting;

import com.google.api.services.sheets.v4.Sheets;

/**
 * 基底Googleクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractGoogleObject extends AbstractObject {
	private Sheets sheets;
	private GoogleSpreadAccessor accessor;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 */
	public AbstractGoogleObject(Setting setting) {
		this(setting, null);
	}

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param Googleスプレッドサービス
	 */
	public AbstractGoogleObject(Setting setting, Sheets sheets) {
		super(setting);
		setService(sheets);
	}

	/**
	 * Googleスプレッドサービスを取得します。
	 *
	 * @return Googleスプレッドサービス
	 */
	protected Sheets getService() {
		return this.sheets;
	}

	/**
	 * Googleスプレッドサービスを設定します。
	 *
	 * @param sheets Googleスプレッドサービス
	 */
	protected void setService(Sheets sheets) {
		this.sheets = sheets;
		if (this.sheets != null && this.accessor == null) {
			accessor = new GoogleSpreadAccessor(this.sheets);
		}
	}

	/**
	 * Googleスプレッドのアクセサを取得します。
	 *
	 * @return Googleスプレッドのアクセサ
	 */
	protected GoogleSpreadAccessor getAccessor() {
		if (getService() == null) {
			throw new RuntimeException("Googleスプレッドにアクセスできません！");
		}

		return this.accessor;
	}
}
