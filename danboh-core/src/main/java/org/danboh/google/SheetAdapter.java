/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.google;

import java.util.List;

import com.google.api.services.sheets.v4.model.MergeCellsRequest;
import com.google.api.services.sheets.v4.model.UpdateCellsRequest;
import com.google.api.services.sheets.v4.model.UpdateDimensionPropertiesRequest;

/**
 * スプレッドシート書式設定アダプタのインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface SheetAdapter {
	/**
	 * 列幅設定リクエストを取得します。
	 *
	 * @param sheetId シートID
	 * @return リクエストのリスト
	 */
	public List<UpdateDimensionPropertiesRequest> getUpdateDimensionPropertiesRequest(Integer sheetId);

	/**
	 * セル書式設定リクエストを取得します。
	 *
	 * @param sheetId シートID
	 * @param contents シート内容
	 * @return リクエストのリスト
	 */
	public List<UpdateCellsRequest> getUpdateCellsRequest(Integer sheetId, List<List<Object>> contents);

	/**
	 * セル結合リクエストを取得します。
	 *
	 * @param sheetId シートID
	 * @param contents シート内容
	 * @return リクエストのリスト
	 */
	public List<MergeCellsRequest> getMergeCellsRequest(Integer sheetId, List<List<Object>> contents);
}
