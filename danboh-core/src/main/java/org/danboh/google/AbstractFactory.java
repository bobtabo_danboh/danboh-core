/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.google;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.danboh.xml.jaxb.setting.Setting;

import com.google.api.services.sheets.v4.Sheets;

/**
 * 基底ファクトリクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractFactory extends AbstractGoogleObject {
	private boolean useGoogle;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param useGoogle GoogleAPI利用時は true を設定します
	 */
	public AbstractFactory(Setting setting, boolean useGoogle) {
		super(setting);
		this.useGoogle = useGoogle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final Sheets getService() {
		if (!useGoogle()) {
			throw new RuntimeException("GoogleAPIは使用しません");
		}

		if (super.getService() == null) {
			try {
				createService();
			} catch (GeneralSecurityException | IOException e) {
				throw new RuntimeException(e);
			}
		}

		return super.getService();
	}

	/**
	 * GoogleAPIを利用するか確認します。
	 *
	 * @return GoogleAPI利用時は true を返します
	 */
	protected final boolean useGoogle() {
		return this.useGoogle;
	}

	/**
	 * GoogleAPI認証を行い、Googleスプレッドサービスを取得します。
	 *
	 * @throws GeneralSecurityException セキュリティ例外
	 * @throws IOException 入出力例外
	 */
	private void createService() throws GeneralSecurityException, IOException {
		GoogleAccessor accessor = new GoogleAccessor(getSetting());
		setService(accessor.getService());
	}
}
