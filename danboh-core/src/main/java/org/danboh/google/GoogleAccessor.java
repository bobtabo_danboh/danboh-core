/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.google;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.danboh.xml.jaxb.setting.Setting;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;

/**
 * Googleスプレッドのアクセスクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
final class GoogleAccessor {
	private Setting setting;

	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	protected GoogleAccessor(Setting setting) {
		this.setting = setting;
	}

	/**
	 * Googleスプレッドサービスを取得します。
	 *
	 * @return Googleスプレッドサービス
	 * @throws GeneralSecurityException セキュリティ例外
	 * @throws IOException 入出力例外
	 */
	public Sheets getService() throws GeneralSecurityException, IOException {
		Credential credential = authorize();
		Sheets service = getSheetsService(credential);
		return service;
	}

	/**
	 * GoogleAPI認証を行います。
	 *
	 * @return GoogleAPI認証オブジェクト
	 * @throws GeneralSecurityException セキュリティ例外
	 * @throws IOException入出力例外
	 */
	protected Credential authorize() throws GeneralSecurityException, IOException {
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		File file = FileUtils.getFile(this.setting.getRoot() + "/" + this.setting.getGoogle().getClientKey());
		InputStream in = FileUtils.openInputStream(file);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(in));

		List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);
		File dataStoreDir = new File(System.getProperty("user.home"), ".credentials/danboh");
		FileDataStoreFactory dataStoreFactory = new FileDataStoreFactory(dataStoreDir);

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, jsonFactory, clientSecrets, scopes)
						.setDataStoreFactory(dataStoreFactory)
						.setAccessType("offline")
						.build();

		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

		return credential;
	}

	/**
	 * Googleスプレッドサービスを取得します。
	 *
	 * @param credential GoogleAPI認証オブジェクト
	 * @return Googleスプレッドサービス
	 * @throws GeneralSecurityException セキュリティ例外
	 * @throws IOException入出力例外
	 */
	protected Sheets getSheetsService(Credential credential) throws GeneralSecurityException, IOException {
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		ResourceBundle resource = ResourceBundle.getBundle("google");
		int timeout = NumberUtils.toInt(resource.getString("timeout"));

		return new Sheets.Builder(httpTransport, jsonFactory, setTimeout(credential, timeout))
				.setApplicationName(this.setting.getGoogle().getAppName())
				.build();
	}

	/**
	 * タイムアウトを設定します。
	 *
	 * @param initializer HTTPリクエスト初期化インターフェース
	 * @param timeout タイムアウト時間
	 * @return 初期化されたリクエスト
	 */
	private HttpRequestInitializer setTimeout(final HttpRequestInitializer initializer, final int timeout) {
	    return request -> {
	        initializer.initialize(request);
	        request.setReadTimeout(timeout);
	    };
	}
}
