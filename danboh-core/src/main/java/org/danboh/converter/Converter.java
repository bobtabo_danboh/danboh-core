/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter;

import java.io.File;
import java.util.Map;

import org.danboh.Console;
import org.danboh.converter.google.BatchGoogleConverter;
import org.danboh.converter.google.EnumGoogleConverter;
import org.danboh.converter.google.GoogleConvertable;
import org.danboh.converter.google.MailGoogleConverter;
import org.danboh.converter.google.TableGoogleConverter;
import org.danboh.converter.google.WebGoogleConverter;
import org.danboh.converter.xml.BatchXmlConverter;
import org.danboh.converter.xml.EnumXmlConverter;
import org.danboh.converter.xml.MailXmlConverter;
import org.danboh.converter.xml.TableXmlConverter;
import org.danboh.converter.xml.WebXmlConverter;
import org.danboh.converter.xml.XmlConvertable;
import org.danboh.google.AbstractFactory;
import org.danboh.util.FileUtil;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceType;

/**
 * 変換処理を実行するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Converter extends AbstractFactory {
	private static Converter instance = null;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 */
	private Converter(Setting setting) {
		super(setting, true);
	}

	/**
	 * Web定義出力オブジェクトを取得します。
	 * @param <V>
	 *
	 * @return 定義出力オブジェクト
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <V> void toXml(SourceType type, Map<String, V> map) throws Exception {
		Console.out(type.name(), " 出力開始！");

		XmlConvertable<V> convert = null;
		switch (type) {
			case WEB:
				convert = (XmlConvertable<V>) new WebXmlConverter(getSetting());
				break;
			case ENUM:
				convert = (XmlConvertable<V>) new EnumXmlConverter(getSetting());
				break;
			case TABLE:
				convert = (XmlConvertable<V>) new TableXmlConverter(getSetting());
				break;
			case BATCH:
				convert = (XmlConvertable<V>) new BatchXmlConverter(getSetting());
				break;
			case MAIL:
				convert = (XmlConvertable<V>) new MailXmlConverter(getSetting());
				break;
			default:
				throw new RuntimeException("対象外です");
		}

		Console.out(type.name(), " 出力終了！");

		convert.convert(map);
	}

	/**
	 * Web定義出力オブジェクトを取得します。
	 * @param <V>
	 *
	 * @return 定義出力オブジェクト
	 * @throws Exception
	 */
	public <V> void toGoogle(SourceType type) throws Exception {
		GoogleConvertable<?> convert = null;
		String path = null;
		switch (type) {
			case WEB:
				path = getSetting().getRoot() + "/" + getSetting().getXml().getResource().getWeb();
				convert = new WebGoogleConverter(getSetting(), getService());
				break;
			case ENUM:
				path = getSetting().getRoot() + "/" + getSetting().getXml().getResource().getEnum();
				convert = new EnumGoogleConverter(getSetting(), getService());
				break;
			case TABLE:
				path = getSetting().getRoot() + "/" + getSetting().getXml().getResource().getTable();
				convert = new TableGoogleConverter(getSetting(), getService());
				break;
			case BATCH:
				path = getSetting().getRoot() + "/" + getSetting().getXml().getResource().getBatch();
				convert = new BatchGoogleConverter(getSetting(), getService());
				break;
			case MAIL:
				path = getSetting().getRoot() + "/" + getSetting().getXml().getResource().getMail();
				convert = new MailGoogleConverter(getSetting(), getService());
				break;
			default:
				throw new RuntimeException("対象外です");
		}

		File[] files = FileUtil.getFiles(path);
		convert.convert(files);
	}
	/**
	 * インスタンスを生成します。
	 *
	 * @param setting 設定オブジェクト
	 * @return このクラスのインスタンス
	 */
	public static Converter getInstance(Setting setting) {
		if (instance == null) {
			instance = new Converter(setting);
		}

		return instance;
	}
}