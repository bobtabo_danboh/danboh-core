/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.util.Map;
import java.util.Map.Entry;

import org.danboh.mapper.table.Column;
import org.danboh.mapper.table.Relation;
import org.danboh.mapper.table.Table;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.ColumnField;
import org.danboh.xml.jaxb.definition.RelationField;
import org.danboh.xml.jaxb.definition.TableDefinition;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionTableService;

/**
 * テーブル定義書のXML変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class TableXmlConverter extends AbstractXmlConverter<Table> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	public TableXmlConverter(Setting setting) {
		super(setting);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void execute(Map<String, Table> map) throws Exception {
		DefinitionTableService service = ServiceLocator.getDefinitionTableService();

		int fileNo = 1;
		for (Entry<String, Table> entry : map.entrySet()) {
			Table table = entry.getValue();

			TableDefinition definition = new TableDefinition();
			definition.setSheetName(table.getSheetName());
			definition.setName(table.getEntityName());
			definition.setTableName(table.getTableName());

			for (Entry<String, Column> columnEntry : table.getColumnMap().entrySet()) {
				Column column = columnEntry.getValue();

				ColumnField columnField = new ColumnField();
				columnField.setName(column.getAttributeName());
				columnField.setColumn(column.getColumnName());
				columnField.setField(column.getFieldName());
				columnField.setType(column.getType());
				columnField.setInt(column.getIntNum());
				columnField.setDec(column.getDecNum());
				columnField.setPk(column.getPrimaryKey());
				columnField.setUnsigned(column.getUnsigned());
				columnField.setNotNull(column.getNotNull());
				columnField.setAutoIncrement(column.getIdentity());
				columnField.setDefaultValue(column.getDefaultValue());
				columnField.setEnums(column.getCode());
				columnField.setSequence(column.getSequenceName());
				columnField.setUnique(column.getUnique());
				columnField.setIndex1(column.getIndex1());
				columnField.setIndex2(column.getIndex2());
				columnField.setIndex3(column.getIndex3());
				columnField.setIndex4(column.getIndex4());
				columnField.setIndex5(column.getIndex5());

				if (definition.getCols() == null) {
					definition.setCols(new TableDefinition.Cols());
				}
				definition.getCols().getCol().add(columnField);
			}

			for (Relation relation : table.getRelationList()) {
				RelationField relationField = new RelationField();
				relationField.setEntityName(relation.getEntityName());
				relationField.setTableName(relation.getTableName());
				relationField.setFieldName(relation.getFieldName());
				relationField.setMultiplicity(relation.getMultiplicity());
				relationField.setMappedBy(relation.getMappedBy());
				relationField.setJoinColumn(relation.getJoinColumn());
				relationField.setClasspath(relation.getClasspath());

				if (definition.getRelations() == null) {
					definition.setRelations(new TableDefinition.Relations());
				}
				definition.getRelations().getRelation().add(relationField);
			}

			String fileName = NumberUtil.zeroSupply(fileNo, 3) + "_" + table.getClassName();
			service.output(definition, getSavePath(fileName));
			fileNo++;
		}
	}
}
