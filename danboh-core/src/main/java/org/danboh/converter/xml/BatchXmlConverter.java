/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.util.Map;
import java.util.Map.Entry;

import org.danboh.mapper.batch.Arg;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.batch.Field;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.BatchDefinition;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.ProcessArgField;
import org.danboh.xml.jaxb.definition.ProcessField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionBatchService;

/**
 * バッチ定義書のXML変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class BatchXmlConverter extends AbstractXmlConverter<Batch> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	public BatchXmlConverter(Setting setting) {
		super(setting);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void execute(Map<String, Batch> map) throws Exception {
		DefinitionBatchService service = ServiceLocator.getDefinitionBatchService();

		int fileNo = 1;
		for (Entry<String, Batch> entry : map.entrySet()) {
			Batch batch = entry.getValue();

			DtoField dto = new DtoField();
			dto.setName(batch.getEntityName());
			dto.setClassName(batch.getClassName());
			dto.setCategory(batch.getCategoryName());

			for (Entry<String, Field> batchField : batch.getFieldMap().entrySet()) {
				Field field = batchField.getValue();
				DtoPropertyField property = new DtoPropertyField();
				property.setName(field.getAttributeName());
				property.setField(field.getFieldName());
				property.setType(field.getType());
				property.setDefaultValue(field.getDefaultValue());
				property.setTable(field.getRefEntity());
				property.setTableColumn(field.getRefField());
				property.setEnum(field.getCode());

				if (dto.getProperties() == null) {
					dto.setProperties(new DtoField.Properties());
				}
				dto.getProperties().getProperty().add(property);
			}

			BatchDefinition definition = new BatchDefinition();
			definition.setSheetName(batch.getSheetName());
			definition.setDto(dto);

			for (Entry<String, Batch> subField : batch.getSubClasses().entrySet()) {
				Batch subBatch = subField.getValue();

				DtoField subDto = new DtoField();
				subDto.setName(subBatch.getEntityName());
				subDto.setClassName(subBatch.getClassName());
				subDto.setCategory(subBatch.getCategoryName());

				for (Entry<String, Field> webField : subBatch.getFieldMap().entrySet()) {
					Field field = webField.getValue();
					DtoPropertyField property = new DtoPropertyField();
					property.setName(field.getAttributeName());
					property.setField(field.getFieldName());
					property.setType(field.getType());
					property.setDefaultValue(field.getDefaultValue());
					property.setTable(field.getRefEntity());
					property.setTableColumn(field.getRefField());
					property.setEnum(field.getCode());


					if (subDto.getProperties() == null) {
						subDto.setProperties(new DtoField.Properties());
					}
					subDto.getProperties().getProperty().add(property);
				}


				if (definition.getSubDtos() == null) {
					definition.setSubDtos(new BatchDefinition.SubDtos());
				}
				definition.getSubDtos().getDto().add(subDto);
			}

			org.danboh.mapper.batch.Process batchProcess = batch.getProcess();
			ProcessField process = new ProcessField();
			process.setName(batchProcess.getProcessName());
			process.setMethod(batchProcess.getMethodName());
			process.setResult(batchProcess.getResultName());
			process.setResultType(batchProcess.getResultType());

			for (Arg batchProcessArg : batchProcess.getArgList()) {
				ProcessArgField arg = new ProcessArgField();
				arg.setName(batchProcessArg.getArgName());
				arg.setProperty(batchProcessArg.getArgField());
				arg.setType(batchProcessArg.getArgType());
				process.getArg().add(arg);
			}

			definition.setProcess(process);

			String fileName = NumberUtil.zeroSupply(fileNo, 3) + "_" + batch.getClassName();
			service.output(definition, getSavePath(fileName));
			fileNo++;
		}
	}
}
