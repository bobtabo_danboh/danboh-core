/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.web.Action;
import org.danboh.mapper.web.Event;
import org.danboh.mapper.web.Field;
import org.danboh.mapper.web.Message;
import org.danboh.mapper.web.Web;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.ActionEventField;
import org.danboh.xml.jaxb.definition.ActionField;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.MessageField;
import org.danboh.xml.jaxb.definition.Token;
import org.danboh.xml.jaxb.definition.ValidateField;
import org.danboh.xml.jaxb.definition.WebDefinition;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionWebService;

/**
 * Web定義書のXML変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class WebXmlConverter extends AbstractXmlConverter<Web> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	public WebXmlConverter(Setting setting) {
		super(setting);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void execute(Map<String, Web> map) throws Exception {
		DefinitionWebService service = ServiceLocator.getDefinitionWebService();

		int fileNo = 1;
		for (Entry<String, Web> entry : map.entrySet()) {
			Web web = entry.getValue();

			DtoField dto = new DtoField();
			dto.setName(web.getEntityName());
			dto.setClassName(web.getClassName());
			dto.setCategory(web.getCategoryName());

			for (Entry<String, Field> webField : web.getFieldMap().entrySet()) {
				Field field = webField.getValue();
				DtoPropertyField property = new DtoPropertyField();
				property.setName(field.getAttributeName());
				property.setField(field.getFieldName());
				property.setType(field.getType());
				property.setDefaultValue(field.getDefaultValue());
				property.setTable(field.getRefEntity());
				property.setTableColumn(field.getRefField());
				property.setEnum(field.getCode());
				property.setInput(field.isInput());

				if (dto.getProperties() == null) {
					dto.setProperties(new DtoField.Properties());
				}
				dto.getProperties().getProperty().add(property);
			}

			WebDefinition definition = new WebDefinition();
			definition.setSheetName(web.getSheetName());
			definition.setDto(dto);

			for (Entry<String, Web> subField : web.getSubClasses().entrySet()) {
				Web subWeb = subField.getValue();

				DtoField subDto = new DtoField();
				subDto.setName(subWeb.getEntityName());
				subDto.setClassName(subWeb.getClassName());
				subDto.setCategory(subWeb.getCategoryName());

				for (Entry<String, Field> webField : subWeb.getFieldMap().entrySet()) {
					Field field = webField.getValue();
					DtoPropertyField property = new DtoPropertyField();
					property.setName(field.getAttributeName());
					property.setField(field.getFieldName());
					property.setType(field.getType());
					property.setDefaultValue(field.getDefaultValue());
					property.setTable(field.getRefEntity());
					property.setTableColumn(field.getRefField());
					property.setEnum(field.getCode());
					property.setInput(field.isInput());

					if (subDto.getProperties() == null) {
						subDto.setProperties(new DtoField.Properties());;
					}
					subDto.getProperties().getProperty().add(property);
				}

				if (definition.getSubDtos() == null) {
					definition.setSubDtos(new WebDefinition.SubDtos());
				}
				definition.getSubDtos().getDto().add(subDto);
			}

			for (Action action : web.getActionList()) {
				ActionField actionField = new ActionField();
				actionField.setName(action.getActionName());
				actionField.setClassName(action.getClassName());
				actionField.setCategory(action.getCategoryName());

				for (Event event : action.getEventList()) {
					ActionEventField actionEventField = new ActionEventField();
					actionEventField.setName(event.getEventName());
					actionEventField.setMethod(event.getMethodName());
					actionEventField.setBackward(event.getBackward());
					actionEventField.setForward(event.getForward());
					actionEventField.setValidate(event.isValidate());
					if (StringUtils.isNotEmpty(event.getToken())) {
						actionEventField.setToken(Token.fromValue(event.getToken()));
					}
					//TODO
					actionEventField.setDescprition(EMPTY);

					if (actionField.getEvents() == null) {
						actionField.setEvents(new ActionField.Events());
					}
					actionField.getEvents().getEvent().add(actionEventField);
				}


				if (definition.getActions() == null) {
					definition.setActions(new WebDefinition.Actions());
				}
				definition.getActions().getAction().add(actionField);
			}

			for (Entry<String, Field> webField : web.getFieldMap().entrySet()) {
				Field field = webField.getValue();
				if (!field.isValidate()) {
					continue;
				}
				ValidateField validateField = new ValidateField();
				validateField.setName(field.getAttributeName());
				validateField.setRequired(field.isRequired());
				validateField.setMin(NumberUtil.toString(field.getMinLength()));
				validateField.setMax(NumberUtil.toString(field.getMaxLength()));
				validateField.setDateFormat(field.getDateFormat());
				validateField.setPrecision(NumberUtil.toString(field.getPrecision()));
				validateField.setScale(NumberUtil.toString(field.getScale()));
				validateField.setEmail(field.isEmail());
				validateField.setUrl(field.isUrl());
				validateField.setRegex(field.getRegexExpression());
				validateField.setOgnl(field.getOgnlExpression());
				validateField.setAlphaNumeric(field.isAlphaNumeric());
				validateField.setNumeric(field.isNumeric());
				validateField.setHalfKana(field.isOneByteKana());

				if (definition.getValidates() == null) {
					definition.setValidates(new WebDefinition.Validates());
				}
				definition.getValidates().getValidate().add(validateField);
			}

			for (Message message : web.getMessageList()) {
				MessageField messageField = new MessageField();
				messageField.setKey(message.getKey());
				messageField.setType(message.getType());
				messageField.setMessage(message.getMessage());

				if (definition.getMessages() == null) {
					definition.setMessages(new WebDefinition.Messages());
				}
				definition.getMessages().getMessage().add(messageField);
			}

			String fileName = NumberUtil.zeroSupply(fileNo, 3) + "_" + web.getClassName();
			service.output(definition, getSavePath(fileName));
			fileNo++;
		}
	}
}
