/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.math.NumberUtils;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.enums.Field;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.EnumDefinition;
import org.danboh.xml.jaxb.definition.EnumField;
import org.danboh.xml.jaxb.definition.EnumValueField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionEnumService;

/**
 * Enum定義書のXML変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class EnumXmlConverter extends AbstractXmlConverter<Enums> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	public EnumXmlConverter(Setting setting) {
		super(setting);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void execute(Map<String, Enums> map) throws Exception {
		DefinitionEnumService service = ServiceLocator.getDefinitionEnumService();

		int fileNo = 1;
		for (Entry<String, Enums> entry : map.entrySet()) {
			Enums enums = entry.getValue();

			EnumField enumField = new EnumField();
			enumField.setName(enums.getEntityName());
			enumField.setClassName(enums.getClassName());
			enumField.setCategory(enums.getCategoryName());

			for (Entry<String, Field> enumsEntry : enums.getFieldMap().entrySet()) {
				Field field = enumsEntry.getValue();

				EnumValueField value = new EnumValueField();
				value.setLabel(field.getAttributeName());
				value.setKey(field.getFieldName());
				value.setValue(NumberUtils.toInt(field.getDefaultValue()));

				if (enumField.getValues() == null) {
					enumField.setValues(new EnumField.Values());
				}
				enumField.getValues().getValue().add(value);
			}

			EnumDefinition definition = new EnumDefinition();
			definition.setSheetName(enums.getSheetName());
			definition.setEnum(enumField);

			String fileName = NumberUtil.zeroSupply(fileNo, 3) + "_" + enums.getClassName();
			service.output(definition, getSavePath(fileName));
			fileNo++;
		}
	}
}
