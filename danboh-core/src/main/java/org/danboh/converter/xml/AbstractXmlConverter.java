/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.io.File;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ClassUtils;
import org.danboh.Constants;
import org.danboh.xml.jaxb.setting.Setting;

/**
 * 定義書XML変換の基底クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractXmlConverter<V> implements XmlConvertable<V>, Constants {
	private Setting setting;

	private static ResourceBundle resource = ResourceBundle.getBundle("danboh");

	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	public AbstractXmlConverter(Setting setting) {
		this.setting = setting;
	}

	/**
	 * 設定オブジェクトを取得します。
	 *
	 * @return setting 設定オブジェクト
	 */
	protected Setting getSetting() {
		return setting;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void convert(Map<String, V> map) throws Exception {
		if (getSetting().getXml().getExport().isRemoveOldOutput()) {
			File dir = new File(getSaveDir());
			FileUtils.deleteQuietly(dir);
		}

		execute(map);
	}

	/**
	 * 変換を処理します。
	 *
	 * @param map 定義書マップ
	 * @throws Exception 変換失敗時にスローされる例外です
	 */
	protected abstract void execute(Map<String, V> map) throws Exception;

	/**
	 * ファイル保存パスを取得します。
	 *
	 * @param fileName ファイル名
	 * @return ファイルパス
	 * @throws ClassNotFoundException このクラスの型取得失敗時にスローされる例外です
	 */
	protected String getSavePath(String fileName) throws ClassNotFoundException {
		StringBuilder builder = new StringBuilder();
		builder.append(getSaveDir());

		File dir = new File(getSaveDir());
		dir.mkdirs();

		builder.append("/").append(fileName).append(".").append(resource.getString("definition.extension"));
		return builder.toString();
	}

	/**
	 * 保存ディレクトリパスを取得します。
	 *
	 * @return ディレクトリパス
	 * @throws ClassNotFoundException このクラスの型取得失敗時にスローされる例外です
	 */
	@SuppressWarnings("rawtypes")
	protected String getSaveDir() throws ClassNotFoundException {
		ResourceBundle resource = ResourceBundle.getBundle("danboh");

		Type type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		String typeName = type.toString().split(" ")[1];
		Class typeClass = ClassUtils.getClass(typeName);

		StringBuilder builder = new StringBuilder();
		builder.append(getSetting().getRoot());
		builder.append("/").append(resource.getString("danboh.root")).append("/xml").append("/");
		builder.append(typeClass.getSimpleName().toLowerCase());

		return builder.toString();
	}
}
