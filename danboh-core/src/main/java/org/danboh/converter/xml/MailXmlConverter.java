/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.util.Map;
import java.util.Map.Entry;

import org.danboh.mapper.mail.Field;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.mail.Send;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.MailDefinition;
import org.danboh.xml.jaxb.definition.SendField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.service.DefinitionMailService;

/**
 * メール定義書のXML変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class MailXmlConverter extends AbstractXmlConverter<Mail> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 */
	public MailXmlConverter(Setting setting) {
		super(setting);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void execute(Map<String, Mail> map) throws Exception {
		DefinitionMailService service = ServiceLocator.getDefinitionMailService();

		int fileNo = 1;
		for (Entry<String, Mail> entry : map.entrySet()) {
			Mail mail = entry.getValue();

			DtoField dto = new DtoField();
			dto.setName(mail.getEntityName());
			dto.setClassName(mail.getClassName());
			dto.setCategory(mail.getCategoryName());

			for (Entry<String, Field> mailField : mail.getFieldMap().entrySet()) {
				Field field = mailField.getValue();
				DtoPropertyField property = new DtoPropertyField();
				property.setName(field.getAttributeName());
				property.setField(field.getFieldName());
				property.setType(field.getType());
				property.setDefaultValue(field.getDefaultValue());
				property.setTable(field.getRefEntity());
				property.setTableColumn(field.getRefField());
				property.setEnum(field.getCode());

				if (dto.getProperties() == null) {
					dto.setProperties(new DtoField.Properties());
				}
				dto.getProperties().getProperty().add(property);
			}

			MailDefinition definition = new MailDefinition();
			definition.setSheetName(mail.getSheetName());
			definition.setDto(dto);

			for (Entry<String, Mail> subField : mail.getSubClasses().entrySet()) {
				Mail subMail = subField.getValue();

				DtoField subDto = new DtoField();
				subDto.setName(subMail.getEntityName());
				subDto.setClassName(subMail.getClassName());
				subDto.setCategory(subMail.getCategoryName());

				for (Entry<String, Field> webField : subMail.getFieldMap().entrySet()) {
					Field field = webField.getValue();
					DtoPropertyField property = new DtoPropertyField();
					property.setName(field.getAttributeName());
					property.setField(field.getFieldName());
					property.setType(field.getType());
					property.setDefaultValue(field.getDefaultValue());
					property.setTable(field.getRefEntity());
					property.setTableColumn(field.getRefField());
					property.setEnum(field.getCode());

					if (subDto.getProperties() == null) {
						subDto.setProperties(new DtoField.Properties());
					}
					subDto.getProperties().getProperty().add(property);
				}


				if (definition.getSubDtos() == null) {
					subDto.setProperties(new DtoField.Properties());
				}
				definition.getSubDtos().getDto().add(subDto);
			}

			for (Send send : mail.getSendList()) {
				SendField sendField = new SendField();
				sendField.setName(send.getProcessName());
				sendField.setMethod(send.getMethodName());
				definition.getSend().add(sendField);
			}

			String fileName = NumberUtil.zeroSupply(fileNo, 3) + "_" + mail.getClassName();
			service.output(definition, getSavePath(fileName));
			fileNo++;
		}
	}
}
