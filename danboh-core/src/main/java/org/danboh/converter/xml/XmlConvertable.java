/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.xml;

import java.util.Map;

import org.danboh.converter.Convertable;

/**
 * 定義書のXML変換機能を提供するインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface XmlConvertable<V> extends Convertable<V> {

	/**
	 * 変換します。
	 *
	 * @param map 定義書マップ
	 * @throws Exception 変換失敗時にスローされる例外です
	 */
	public void convert(Map<String, V> map) throws Exception;
}
