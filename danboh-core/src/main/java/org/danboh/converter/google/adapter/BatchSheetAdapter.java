/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google.adapter;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.google.api.services.sheets.v4.model.GridData;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.MergeCellsRequest;
import com.google.api.services.sheets.v4.model.RowData;
import com.google.api.services.sheets.v4.model.UpdateCellsRequest;

/**
 * バッチ定義書スプレッドシート書式設定アダプタクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class BatchSheetAdapter extends AbstractSheetAdapter {
	private enum SectionType {
		INOUT, PROCESS
	}

	/**
	 * コンストラクタ。
	 *
	 * @param template テンプレート書式マップ
	 */
	public BatchSheetAdapter(Map<?, ?> template) {
		super(template);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UpdateCellsRequest> getUpdateCellsRequest(Integer sheetId, List<List<Object>> contents) {
		List<UpdateCellsRequest> result = new LinkedList<UpdateCellsRequest>();

		GridData gridData = (GridData) getTemplate().get("GridData");
		SectionType section = null;

		for (int i = 0; i < contents.size(); i++) {
			List<Object> content = contents.get(i);
			if (CollectionUtils.isEmpty(content)) {
				continue;
			}

			if ("入出力項目".equals(content.get(0))) {
				section = SectionType.INOUT;
			} else if ("処理".equals(content.get(0))) {
				section = SectionType.PROCESS;
			}

			UpdateCellsRequest request = new UpdateCellsRequest();
			request.setFields("userEnteredFormat,dataValidation,note");
			request.setRows(new LinkedList<RowData>());

			RowData row = null;

			switch (section) {
				case INOUT:
					if ("入出力項目".equals(content.get(0))) {
						row = gridData.getRowData().get(0);
					} else if ("日本語名".equals(content.get(0))) {
						row = gridData.getRowData().get(1);
					} else if ("フィールド名".equals(content.get(0))) {
						row = gridData.getRowData().get(2);
					} else {
						row = gridData.getRowData().get(3);
					}
					break;
				case PROCESS:
					if ("処理".equals(content.get(0))) {
						row = gridData.getRowData().get(5);
					} else if ("処理名".equals(content.get(0))) {
						row = gridData.getRowData().get(6);
					} else if ("引数名".equals(content.get(0))) {
						row = gridData.getRowData().get(7);
					} else {
						row = gridData.getRowData().get(8);
					}
					break;
			}

			GridRange gridRange = new GridRange();
			gridRange.setStartRowIndex(i);
			gridRange.setEndRowIndex(i + 1);
			gridRange.setSheetId(sheetId);

			request.setRange(gridRange);
			request.getRows().add(row);

			result.add(request);
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<MergeCellsRequest> getMergeCellsRequest(Integer sheetId, List<List<Object>> contents) {
		List<MergeCellsRequest> result = new LinkedList<MergeCellsRequest>();

		SectionType section = null;

		for (int i = 0; i < contents.size(); i++) {
			List<Object> content = contents.get(i);
			if (CollectionUtils.isEmpty(content)) {
				continue;
			}

			if ("入出力項目".equals(content.get(0))) {
				section = SectionType.INOUT;
			} else if ("処理".equals(content.get(0))) {
				section = SectionType.PROCESS;
			}

			boolean merge = false;
			int startColumnIndex = 0;
			int endColumnIndex = 13;
			switch (section) {
				case INOUT:
					if ("入出力項目".equals(content.get(0))) {
						merge = true;
					} else if ("日本語名".equals(content.get(0))) {
						merge = true;
						startColumnIndex = 5;
					} else if ("フィールド名".equals(content.get(0))) {
						merge = true;
						startColumnIndex = 7;
					} else {
						merge = true;
						startColumnIndex = 7;
					}
					break;
				case PROCESS:
					if ("処理".equals(content.get(0))) {
						merge = true;
					} else if ("処理名".equals(content.get(0))) {
						merge = true;
						startColumnIndex = 9;
					} else if ("引数名".equals(content.get(0))) {
						merge = true;
						startColumnIndex = 3;
					} else {
						merge = true;
						startColumnIndex = 3;
					}
					break;
			}

			if (!merge) {
				continue;
			}

			MergeCellsRequest request = new MergeCellsRequest();
			request.setMergeType(null);

			GridRange gridRange = new GridRange();
			gridRange.setStartRowIndex(i);
			gridRange.setEndRowIndex(i + 1);
			gridRange.setStartColumnIndex(startColumnIndex);
			gridRange.setEndColumnIndex(endColumnIndex);
			gridRange.setSheetId(sheetId);

			request.setRange(gridRange);
			result.add(request);
		}

		return result;
	}
}
