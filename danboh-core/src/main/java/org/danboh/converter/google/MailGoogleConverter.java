/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.converter.google.adapter.MailSheetAdapter;
import org.danboh.google.SheetAdapter;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.MailDefinition;
import org.danboh.xml.jaxb.definition.SendField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;
import org.danboh.xml.service.DefinitionMailService;

import com.google.api.services.sheets.v4.Sheets;

/**
 * メール定義書のスプレッド変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class MailGoogleConverter extends AbstractGoogleConverter<MailDefinition> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public MailGoogleConverter(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void convert(File[] files) throws Exception {
		DefinitionMailService service = ServiceLocator.getDefinitionMailService();

		ResourceBundle resource = ResourceBundle.getBundle("google");
		String range = resource.getString("range.mail");
		String templateRange = resource.getString("template.range.mail");

		List<String> templateRanges = Arrays.asList("テンプレート!" + templateRange);

		List<Spread> spreads = getSetting().getGoogle().getMail().getSpread();
		for (Spread spread : spreads) {
			getAccessor().deleteSheets(spread.getId(), StringUtils.split(getSetting().getGoogle().getMail().getSkip(), ","));
			Map template = getAccessor().getTemplateSheet(spread.getId(), templateRanges);
			SheetAdapter adapter = new MailSheetAdapter(template);

			for (File file : files) {
				MailDefinition definition = service.config(file);
				addSheet(spread.getId(), definition.getSheetName(), range, definition, adapter);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<List<Object>> toSpreadValue(MailDefinition definition) {
		List<List<Object>> result = new LinkedList<List<Object>>();

		result.add(Utils.asList("メール項目"));
		result.add(Utils.asList("日本語名", definition.getDto().getName(), "クラス名", definition.getDto().getClassName(), "カテゴリ", definition.getDto().getCategory()));
		result.add(Utils.asList("フィールド名", "フィールド名", "型", "デフォルト値", "参照テーブル", "参照カラム", "コード", "備考"));

		if (definition.getDto().getProperties() != null) {
			for (DtoPropertyField prop : definition.getDto().getProperties().getProperty()) {
				result.add(Utils.asList(
						  prop.getName()
						, prop.getField()
						, prop.getType()
						, prop.getDefaultValue()
						, prop.getTable()
						, prop.getTableColumn()
						, prop.getEnum()
				));
			}
		}

		if (definition.getSubDtos() != null) {
			for (DtoField subDto : definition.getSubDtos().getDto()) {
				result.add(Utils.asList("日本語名", subDto.getName(), "クラス名", subDto.getClassName(), "カテゴリ", subDto.getCategory()));
				result.add(Utils.asList("フィールド名", "フィールド名", "型", "デフォルト値", "参照テーブル", "参照カラム", "コード", "備考"));
				for (DtoPropertyField subProp : subDto.getProperties().getProperty()) {
					result.add(Utils.asList(
							  subProp.getName()
							, subProp.getField()
							, subProp.getType()
							, subProp.getDefaultValue()
							, subProp.getTable()
							, subProp.getTableColumn()
							, subProp.getEnum()
					));
				}
			}
		}

		result.add(new ArrayList<Object>());
		result.add(Utils.asList("送信"));
		result.add(Utils.asList("処理名", "メソッド名"));
		if (CollectionUtils.isNotEmpty(definition.getSend())) {
			for (SendField send : definition.getSend()) {
				result.add(Utils.asList(send.getName(), send.getMethod()));
			}
		}

		return result;
	}
}
