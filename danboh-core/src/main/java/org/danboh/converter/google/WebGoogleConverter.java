/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.danboh.converter.google.adapter.WebSheetAdapter;
import org.danboh.google.SheetAdapter;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.ActionEventField;
import org.danboh.xml.jaxb.definition.ActionField;
import org.danboh.xml.jaxb.definition.DtoField;
import org.danboh.xml.jaxb.definition.DtoPropertyField;
import org.danboh.xml.jaxb.definition.MessageField;
import org.danboh.xml.jaxb.definition.ValidateField;
import org.danboh.xml.jaxb.definition.WebDefinition;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;
import org.danboh.xml.service.DefinitionWebService;

import com.google.api.services.sheets.v4.Sheets;

/**
 * Web定義書のスプレッド変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class WebGoogleConverter extends AbstractGoogleConverter<WebDefinition> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public WebGoogleConverter(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void convert(File[] files) throws Exception {
		DefinitionWebService service = ServiceLocator.getDefinitionWebService();

		ResourceBundle resource = ResourceBundle.getBundle("google");
		String range = resource.getString("range.web");
		String templateRange = resource.getString("template.range.web");

		List<String> templateRanges = Arrays.asList("テンプレート!" + templateRange);

		List<Spread> spreads = getSetting().getGoogle().getWeb().getSpread();
		for (Spread spread : spreads) {
			getAccessor().deleteSheets(spread.getId(), StringUtils.split(getSetting().getGoogle().getWeb().getSkip(), ","));
			Map template = getAccessor().getTemplateSheet(spread.getId(), templateRanges);
			SheetAdapter adapter = new WebSheetAdapter(template);

			for (File file : files) {
				WebDefinition definition = service.config(file);
				if (StringUtils.isNotEmpty(spread.getCategory())) {
					if (!StringUtils.equals(spread.getCategory(), definition.getDto().getCategory())) {
						continue;
					}
				}
				addSheet(spread.getId(), definition.getSheetName(), range, definition, adapter);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<List<Object>> toSpreadValue(WebDefinition definition) {
		List<List<Object>> result = new LinkedList<List<Object>>();
		result.add(Utils.asList("画面項目"));
		result.add(Utils.asList("日本語名", definition.getDto().getName(), "クラス名", definition.getDto().getClassName(), "カテゴリ", definition.getDto().getCategory()));
		result.add(Utils.asList("フィールド名", "フィールド名", "型", "デフォルト値", "参照テーブル", "参照カラム", "コード", "入力", "備考"));

		if (definition.getDto().getProperties() != null) {
			for (DtoPropertyField prop : definition.getDto().getProperties().getProperty()) {
				result.add(Utils.asList(
						  prop.getName()
						, prop.getField()
						, prop.getType()
						, prop.getDefaultValue()
						, prop.getTable()
						, prop.getTableColumn()
						, prop.getEnum()
						, prop.isInput() ? ON : OFF
				));
			}
		}

		if (definition.getSubDtos() != null) {
			for (DtoField subDto : definition.getSubDtos().getDto()) {
				result.add(Utils.asList("日本語名", subDto.getName(), "クラス名", subDto.getClassName(), "カテゴリ", subDto.getCategory()));
				result.add(Utils.asList("フィールド名", "フィールド名", "型", "デフォルト値", "参照テーブル", "参照カラム", "コード", "入力", "備考"));
				for (DtoPropertyField subProp : subDto.getProperties().getProperty()) {
					result.add(Utils.asList(
							  subProp.getName()
							, subProp.getField()
							, subProp.getType()
							, subProp.getDefaultValue()
							, subProp.getTable()
							, subProp.getTableColumn()
							, subProp.getEnum()
							, subProp.isInput() ? ON : OFF
					));
				}
			}
		}

		result.add(new ArrayList<Object>());
		result.add(Utils.asList("アクション"));
		if (definition.getActions() == null) {
			result.add(Utils.asList("アクション名", EMPTY, "クラス名", EMPTY, "カテゴリ", EMPTY));
			result.add(Utils.asList("イベント名", "遷移元", "メソッド名", "入力チェック", "トークン", "遷移先", "処理内容"));
		} else {
			for (ActionField action : definition.getActions().getAction()) {
				result.add(Utils.asList("アクション名", action.getName(), "クラス名", action.getClassName(), "カテゴリ", action.getCategory()));
				result.add(Utils.asList("イベント名", "遷移元", "メソッド名", "入力チェック", "トークン", "遷移先", "処理内容"));
				if (action.getEvents() != null) {
					for (ActionEventField event : action.getEvents().getEvent()) {
						result.add(Utils.asList(
								  event.getName()
								, event.getBackward()
								, event.getMethod()
								, event.isValidate() ? ON : OFF
								, event.getToken() == null ? null : event.getToken().name()
								, event.getForward()
								, event.getDescprition()
						));
					}
				}
			}
		}

		result.add(new ArrayList<Object>());
		result.add(Utils.asList("入力チェック"));
		result.add(Utils.asList("フィールド名", "必須", "最小桁数", "最大桁数", "日付形式", "数値形式", "メール", "URL", "正規表現", "OGNL", "半角入力", "数字入力", "カタカナ"));
		if (definition.getValidates() != null) {
			for (ValidateField validate : definition.getValidates().getValidate()) {
				result.add(Utils.asList(
						  validate.getName()
						, validate.isRequired() ? ON : OFF
						, validate.getMin()
						, validate.getMax()
						, validate.getDateFormat()
						, (StringUtils.isEmpty(validate.getPrecision()) || StringUtils.isEmpty(validate.getScale())) ? EMPTY :  validate.getPrecision() + "," + validate.getScale()
						, validate.isEmail() ? ON : OFF
						, validate.isUrl() ? ON : OFF
						, validate.getRegex()
						, validate.getOgnl()
						, validate.isAlphaNumeric() ? ON : OFF
						, validate.isNumeric() ? ON : OFF
						, validate.isHalfKana() ? ON : OFF
				));
			}
		}

		result.add(new ArrayList<Object>());
		result.add(Utils.asList("メッセージ"));
		result.add(Utils.asList("キー", "タイプ", "メッセージ"));
		if (definition.getMessages() != null) {
			for (MessageField message : definition.getMessages().getMessage()) {
				result.add(Utils.asList(message.getKey(), message.getType(), message.getMessage()));
			}
		}

		return result;
	}
}
