/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google.adapter;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.google.api.services.sheets.v4.model.GridData;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.MergeCellsRequest;
import com.google.api.services.sheets.v4.model.RowData;
import com.google.api.services.sheets.v4.model.UpdateCellsRequest;

/**
 * テーブル定義書スプレッドシート書式設定アダプタクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class TableSheetAdapter extends AbstractSheetAdapter {
	/**
	 * コンストラクタ。
	 *
	 * @param template テンプレート書式マップ
	 */
	public TableSheetAdapter(Map<?, ?> template) {
		super(template);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UpdateCellsRequest> getUpdateCellsRequest(Integer sheetId, List<List<Object>> contents) {
		List<UpdateCellsRequest> result = new LinkedList<UpdateCellsRequest>();

		GridData gridData = (GridData) getTemplate().get("GridData");
		boolean relation = false;

		for (int i = 0; i < contents.size(); i++) {
			List<Object> content = contents.get(i);
			if (CollectionUtils.isEmpty(content)) {
				continue;
			}

			UpdateCellsRequest request = new UpdateCellsRequest();
			request.setFields("formattedValue,userEnteredFormat,effectiveFormat,dataValidation,note");
			request.setRows(new LinkedList<RowData>());

			RowData row = null;
			if (i <= 1) {
				row = gridData.getRowData().get(i);
			} else {
				if ("関連エンティティ名".equals(content.get(0))) {
					row = gridData.getRowData().get(4);
					relation = true;
				} else {
					row = gridData.getRowData().get(relation ? 5 : 2);
				}
			}

			GridRange gridRange = new GridRange();
			gridRange.setStartRowIndex(i);
			gridRange.setEndRowIndex(i + 1);
			gridRange.setSheetId(sheetId);

			request.setRange(gridRange);
			request.getRows().add(row);

			result.add(request);
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<MergeCellsRequest> getMergeCellsRequest(Integer sheetId, List<List<Object>> contents) {
		List<MergeCellsRequest> result = new LinkedList<MergeCellsRequest>();

		boolean relation = false;

		for (int i = 0; i < contents.size(); i++) {
			List<Object> content = contents.get(i);
			if (CollectionUtils.isEmpty(content)) {
				continue;
			}

			boolean merge = false;
			int endColumnIndex = 10;
			if ("エンティティ名".equals(content.get(0))) {
				merge = true;
			} else if ("関連エンティティ名".equals(content.get(0))) {
				relation = true;
				merge = true;
				endColumnIndex = 11;
			} else {
				if (relation) {
					merge = true;
					endColumnIndex = 11;
				}
			}

			if (!merge) {
				continue;
			}

			MergeCellsRequest request = new MergeCellsRequest();
			request.setMergeType(null);

			GridRange gridRange = new GridRange();
			gridRange.setStartRowIndex(i);
			gridRange.setEndRowIndex(i + 1);
			gridRange.setStartColumnIndex(5);
			gridRange.setEndColumnIndex(endColumnIndex);
			gridRange.setSheetId(sheetId);

			request.setRange(gridRange);
			result.add(request);
		}

		return result;
	}
}
