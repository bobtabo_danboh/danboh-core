/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google;

import java.io.File;

import org.danboh.converter.Convertable;

/**
 * 定義書のスプレッド変換機能を提供するインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface GoogleConvertable<V> extends Convertable<V> {
	/**
	 * 変換します。
	 *
	 * @param files 定義書ファイル
	 * @throws Exception 変換失敗時にスローされる例外です
	 */
	public void convert(File[] files) throws Exception;
}
