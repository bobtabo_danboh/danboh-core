/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.danboh.google.AbstractGoogleObject;
import org.danboh.google.SheetAdapter;
import org.danboh.xml.jaxb.setting.Setting;

import com.google.api.services.sheets.v4.Sheets;

/**
 * 定義書のスプレッド変換を行う基底クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractGoogleConverter<V> extends AbstractGoogleObject implements GoogleConvertable<V> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public AbstractGoogleConverter(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * シートを追加します。
	 *
	 * @param spreadsheetId スプレッドシートID
	 * @param sheetName 追加するシート名
	 * @param range 追加データ範囲
	 * @param definition 定義書オブジェクト
	 * @param adapter 書式設定アダプタ
	 */
	protected void addSheet(String spreadsheetId, String sheetName, String range, V definition, SheetAdapter adapter) {
		try {
			getAccessor().addSheet(spreadsheetId, sheetName, range, toSpreadValue(definition), adapter);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 定義書オブジェクトをスプレッドシートデータに変換します。
	 *
	 * @param definition 定義書オブジェクト
	 * @return スプレッドシートデータ
	 */
	protected abstract List<List<Object>> toSpreadValue(V definition);

	/**
	 * サブクラス用ユーティリティのインナークラスです。
	 */
	protected static class Utils {
		/**
		 * <T>配列をオブジェクトリストへ変換します。
		 *
		 * @return 変換したリスト
		 */
		@SafeVarargs
		public static <T> List<Object> asList(T... objects) {
			List<Object> result = new LinkedList<Object>();
			for (T object : objects) {
				result.add((Object) object);
			}
			return result;
		}
	}
}
