/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.danboh.converter.google.adapter.TableSheetAdapter;
import org.danboh.google.SheetAdapter;
import org.danboh.util.NumberUtil;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.ColumnField;
import org.danboh.xml.jaxb.definition.RelationField;
import org.danboh.xml.jaxb.definition.TableDefinition;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;
import org.danboh.xml.service.DefinitionTableService;

import com.google.api.services.sheets.v4.Sheets;

/**
 * テーブル定義書のスプレッド変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class TableGoogleConverter extends AbstractGoogleConverter<TableDefinition> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public TableGoogleConverter(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void convert(File[] files) throws Exception {
		DefinitionTableService service = ServiceLocator.getDefinitionTableService();

		ResourceBundle resource = ResourceBundle.getBundle("google");
		String range = resource.getString("range.table");
		String templateRange = resource.getString("template.range.table");

		List<String> templateRanges = Arrays.asList("テンプレート!" + templateRange);

		List<Spread> spreads = getSetting().getGoogle().getTable().getSpread();
		for (Spread spread : spreads) {
			getAccessor().deleteSheets(spread.getId(), StringUtils.split(getSetting().getGoogle().getTable().getSkip(), ","));
			Map template = getAccessor().getTemplateSheet(spread.getId(), templateRanges);
			SheetAdapter adapter = new TableSheetAdapter(template);

			for (File file : files) {
				TableDefinition definition = service.config(file);
				addSheet(spread.getId(), definition.getSheetName(), range, definition, adapter);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<List<Object>> toSpreadValue(TableDefinition definition) {
		List<List<Object>> result = new LinkedList<List<Object>>();

		result.add(Utils.asList("エンティティ名", definition.getName(), "テーブル名", definition.getTableName(), "クラス名", EMPTY));
		result.add(Utils.asList("属性名", "カラム名", "フィールド名", "型", "整数桁", "少数桁", "主キー", "UN", "NN", "AI", "デフォルト値", "コード", "シーケンス名", "UNIQUE", "INDEX1", "INDEX2", "INDEX3", "INDEX4", "INDEX5", "備考"));

		if (definition.getCols() != null) {
			for (ColumnField col : definition.getCols().getCol()) {
				result.add(Utils.asList(
						  col.getName()
						, col.getColumn()
						, col.getField()
						, col.getType()
						, NumberUtil.toString(col.getInt())
						, NumberUtil.toString(col.getDec())
						, col.isPk() ? ON : OFF
						, col.isUnsigned() ? ON : OFF
						, col.isNotNull() ? ON : OFF
						, col.isAutoIncrement() ? ON : OFF
						, col.getDefaultValue()
						, col.getEnums()
						, col.getSequence()
						, col.getUnique()
						, col.getIndex1()
						, col.getIndex2()
						, col.getIndex3()
						, col.getIndex4()
						, col.getIndex5()
				));
			}
		}

		result.add(new ArrayList<Object>());
		result.add(Utils.asList("関連エンティティ名", "多重度", "フィールド名", "カラム名", "外部キー", "関連クラス名"));

		if (definition.getRelations() != null) {
			for (RelationField relation : definition.getRelations().getRelation()) {
				result.add(Utils.asList(
						  relation.getEntityName()
						, relation.getMultiplicity()
						, relation.getFieldName()
						, relation.getJoinColumn()
						, relation.getMappedBy()
						, relation.getClasspath()
				));
			}
		}

		return result;
	}
}
