/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google.adapter;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.danboh.google.SheetAdapter;

import com.google.api.services.sheets.v4.model.DimensionProperties;
import com.google.api.services.sheets.v4.model.DimensionRange;
import com.google.api.services.sheets.v4.model.GridData;
import com.google.api.services.sheets.v4.model.UpdateDimensionPropertiesRequest;

/**
 * 基底スプレッドシート書式設定アダプタクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractSheetAdapter implements SheetAdapter {
	private Map<?, ?> template;

	/**
	 * コンストラクタ。
	 *
	 * @param template テンプレート書式リスト
	 */
	public AbstractSheetAdapter(Map<?, ?> template) {
		this.template = template;
	}

	/**
	 * テンプレート書式リストを取得します。
	 *
	 * @return テンプレート書式リスト
	 */
	protected Map<?, ?> getTemplate() {
		return this.template;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UpdateDimensionPropertiesRequest> getUpdateDimensionPropertiesRequest(Integer sheetId) {
		List<UpdateDimensionPropertiesRequest> result = new LinkedList<UpdateDimensionPropertiesRequest>();

		GridData gridData = (GridData) getTemplate().get("GridData");
		int i = 0;
		for (DimensionProperties prop : gridData.getColumnMetadata()) {
			UpdateDimensionPropertiesRequest request = new UpdateDimensionPropertiesRequest();
			DimensionRange dimensionRange = new DimensionRange();
			dimensionRange.setSheetId(sheetId);
			dimensionRange.setDimension("COLUMNS");
			dimensionRange.setStartIndex(i);
			dimensionRange.setEndIndex(i + 1);

			DimensionProperties dimensionProperties = new DimensionProperties();
			dimensionProperties.setHiddenByFilter(null);
			dimensionProperties.setHiddenByUser(null);
			dimensionProperties.setPixelSize(prop.getPixelSize());
			dimensionProperties.setDeveloperMetadata(prop.getDeveloperMetadata());

			request.setRange(dimensionRange);
			request.setProperties(dimensionProperties);

			request.setFields("pixelSize");
			result.add(request);
			i++;
		}

		return result;
	}
}
