/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.converter.google;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.danboh.converter.google.adapter.EnumSheetAdapter;
import org.danboh.google.SheetAdapter;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.definition.EnumDefinition;
import org.danboh.xml.jaxb.definition.EnumValueField;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.Spread;
import org.danboh.xml.service.DefinitionEnumService;

import com.google.api.services.sheets.v4.Sheets;

/**
 * Enum定義書のスプレッド変換クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class EnumGoogleConverter extends AbstractGoogleConverter<EnumDefinition> {
	/**
	 * コンストラクタ。
	 *
	 * @param setting
	 *            設定オブジェクト
	 * @param sheets Googleスプレッドサービス
	 */
	public EnumGoogleConverter(Setting setting, Sheets sheets) {
		super(setting, sheets);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void convert(File[] files) throws Exception {
		DefinitionEnumService service = ServiceLocator.getDefinitionEnumService();

		ResourceBundle resource = ResourceBundle.getBundle("google");
		String range = resource.getString("range.enum");
		String templateRange = resource.getString("template.range.enum");

		List<String> templateRanges = Arrays.asList("テンプレート!" + templateRange);

		List<Spread> spreads = getSetting().getGoogle().getEnum().getSpread();
		for (Spread spread : spreads) {
			getAccessor().deleteSheets(spread.getId(), StringUtils.split(getSetting().getGoogle().getEnum().getSkip(), ","));
			Map template = getAccessor().getTemplateSheet(spread.getId(), templateRanges);
			SheetAdapter adapter = new EnumSheetAdapter(template);

			for (File file : files) {
				EnumDefinition definition = service.config(file);
				addSheet(spread.getId(), definition.getSheetName(), range, definition, adapter);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<List<Object>> toSpreadValue(EnumDefinition definition) {
		List<List<Object>> result = new LinkedList<List<Object>>();

		result.add(Utils.asList("Enum", definition.getEnum().getName(), definition.getEnum().getClassName()));
		result.add(Utils.asList("カテゴリ", definition.getEnum().getCategory()));
		result.add(Utils.asList("日本語名", "名前", "値"));

		if (definition.getEnum().getValues() != null) {
			for (EnumValueField enumValue : definition.getEnum().getValues().getValue()) {
				result.add(Utils.asList(enumValue.getLabel(), enumValue.getKey(), enumValue.getValue()));
			}
		}

		return result;
	}
}
