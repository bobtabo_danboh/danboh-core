/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * XML解析情報を取得するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XmlServiceAdapter<E> implements XmlService<E> {

	/** XMLファイルのバインディングオブジェクト */
	private Xao _xao;

	/**
	 * コンストラクタ
	 *
	 * @param xao
	 *            XMLバインディングオブジェクト
	 */
	public XmlServiceAdapter(Xao xao) {
		_xao = xao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E config(String path) throws XaoException {
		return config(FileUtils.getFile(path));
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public E config(File file) throws XaoException {
		InputStream input = null;
		try {
			input = FileUtils.openInputStream(file);
			return config(input);
		} catch (IOException e) {
			throw new XaoException(e);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E config(InputStream stream) throws XaoException {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getBytes(E xmlObject) throws XaoException {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void output(E xmlObject, String path) throws XaoException {
	}

	/**
	 * XMLバインディングオブジェクトを取得します。
	 *
	 * @return　XMLバインディングオブジェクト
	 */
	protected Xao getXao() {
		return _xao;
	}
}
