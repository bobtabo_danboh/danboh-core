/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;

/**
 * XMLファイルをバインディングする為のクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Xao {

	/** JAXBコンテキスト */
	private final JAXBContext _context;

	/**
	 * コンストラクタ
	 *
	 * @param contextPath
	 *            JAXBコンテキスト
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public Xao(String contextPath) throws XaoException {
		try {
			_context = JAXBContext.newInstance(contextPath);
		} catch (JAXBException e) {
			throw new XaoException(e);
		}
	}

	/**
	 * XMLファイルのデータをオブジェクトにバインディングします。
	 *
	 * @param input
	 *            解析するXMLファイルの入力ストリーム
	 * @return バインディングしたオブジェクト
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public Object read(InputStream input) throws XaoException {
		try {
			Unmarshaller unmarshaller = _context.createUnmarshaller();
			return unmarshaller.unmarshal(input);
		} catch (JAXBException e) {
			throw new XaoException(e);
		}
	}

	/**
	 * XMLオブジェクトをストリームに出力します。
	 *
	 * @param jaxbElement
	 *            XMLオブジェクト
	 * @return XMLデータ
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public byte[] write(Object jaxbElement) throws XaoException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			StreamResult stream = new StreamResult(baos);

			Marshaller marshaller = _context.createMarshaller();
			marshaller.marshal(jaxbElement, stream);

			return baos.toByteArray();
		} catch (JAXBException | TransformerFactoryConfigurationError | IOException e) {
			throw new XaoException(e);
		}
	}

	/**
	 * XMLオブジェクトをファイルに出力します。
	 *
	 * @param jaxbElement
	 *            XMLオブジェクト
	 * @param path
	 *            ファイルパス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public void write(Object jaxbElement, String path) throws XaoException {
		try (FileOutputStream fos = new FileOutputStream(path)) {
			Marshaller marshaller = _context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(jaxbElement, fos);
		} catch (JAXBException | IOException e) {
			throw new XaoException(path, e);
		}
	}
}
