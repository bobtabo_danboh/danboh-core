/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml;

/**
 * XMLアクセス例外クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class XaoException extends RuntimeException {

	/** シリアルバージョンID */
	private static final long serialVersionUID = -3301894767792059996L;

	/**
	 * 空のインスタンスを構築します。
	 */
	public XaoException() {
	}

	/**
	 * この例外のメッセージを指定してインスタンスを構築します。
	 *
	 * @param msg
	 *            この例外のメッセージ
	 */
	public XaoException(String msg) {
		super(msg);
	}

	/**
	 * この例外の原因となった例外とメッセージを指定してインスタンスを構築します。
	 *
	 * @param msg
	 *            この例外のメッセージ
	 * @param cause
	 *            この例外の原因となった例外
	 */
	public XaoException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * この例外の原因となった例外を指定してインスタンスを構築します。
	 *
	 * @param cause
	 *            この例外の原因となった例外
	 */
	public XaoException(Throwable cause) {
		super(cause);
	}
}
