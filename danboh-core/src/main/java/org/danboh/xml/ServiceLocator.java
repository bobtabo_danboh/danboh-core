/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml;

import org.danboh.xml.service.DefinitionBatchService;
import org.danboh.xml.service.DefinitionEnumService;
import org.danboh.xml.service.DefinitionMailService;
import org.danboh.xml.service.DefinitionTableService;
import org.danboh.xml.service.DefinitionWebService;
import org.danboh.xml.service.SettingService;

/**
 * XMLサービスクラスを提供するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public final class ServiceLocator {

	/** JAXBで自動生成されたクラスが格納されているパッケージ名 */
	private static final String JAXB_SETTING_PACKAGE = org.danboh.xml.jaxb.setting.ObjectFactory.class.getPackage().getName();
	private static final String JAXB_DEFINITION_PACKAGE = org.danboh.xml.jaxb.definition.ObjectFactory.class.getPackage().getName();

	/**
	 * 設定サービスを取得します
	 *
	 * @return 設定サービス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public static SettingService getSettingService() throws XaoException {
		return new SettingService(new Xao(JAXB_SETTING_PACKAGE));
	}

	/**
	 * 定義サービスを取得します
	 *
	 * @return 定義サービス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public static DefinitionEnumService getDefinitionEnumService() throws XaoException {
		return new DefinitionEnumService(new Xao(JAXB_DEFINITION_PACKAGE));
	}

	/**
	 * 定義サービスを取得します
	 *
	 * @return 定義サービス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public static DefinitionTableService getDefinitionTableService() throws XaoException {
		return new DefinitionTableService(new Xao(JAXB_DEFINITION_PACKAGE));
	}

	/**
	 * 定義サービスを取得します
	 *
	 * @return 定義サービス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public static DefinitionWebService getDefinitionWebService() throws XaoException {
		return new DefinitionWebService(new Xao(JAXB_DEFINITION_PACKAGE));
	}

	/**
	 * 定義サービスを取得します
	 *
	 * @return 定義サービス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public static DefinitionBatchService getDefinitionBatchService() throws XaoException {
		return new DefinitionBatchService(new Xao(JAXB_DEFINITION_PACKAGE));
	}

	/**
	 * 定義サービスを取得します
	 *
	 * @return 定義サービス
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public static DefinitionMailService getDefinitionMailService() throws XaoException {
		return new DefinitionMailService(new Xao(JAXB_DEFINITION_PACKAGE));
	}
}
