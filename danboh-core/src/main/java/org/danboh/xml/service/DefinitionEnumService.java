/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml.service;

import java.io.InputStream;

import org.danboh.xml.Xao;
import org.danboh.xml.XaoException;
import org.danboh.xml.XmlServiceAdapter;
import org.danboh.xml.jaxb.definition.EnumDefinition;

/**
 * 定義XMLサービスクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class DefinitionEnumService extends XmlServiceAdapter<EnumDefinition> {

	/**
	 * コンストラクタ
	 *
	 * @param xao
	 *            XMLバインディングオブジェクト
	 */
	public DefinitionEnumService(Xao xao) {
		super(xao);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EnumDefinition config(InputStream stream) throws XaoException {
		EnumDefinition result = (EnumDefinition) getXao().read(stream);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void output(EnumDefinition xmlObject, String path) throws XaoException {
		getXao().write(xmlObject, path);
	}
}
