/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml.service;

import java.io.InputStream;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.danboh.xml.Xao;
import org.danboh.xml.XaoException;
import org.danboh.xml.XmlServiceAdapter;
import org.danboh.xml.jaxb.setting.Generated;
import org.danboh.xml.jaxb.setting.Google;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;

/**
 * 設定XMLサービスクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class SettingService extends XmlServiceAdapter<Setting> {

	/**
	 * コンストラクタ
	 *
	 * @param xao
	 *            XMLバインディングオブジェクト
	 */
	public SettingService(Xao xao) {
		super(xao);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Setting config(InputStream stream) throws XaoException {
		JAXBElement<?> elements = (JAXBElement<?>) getXao().read(stream);
		Setting result = (Setting) elements.getValue();

		if (result.getGenerated() == null) {
			result.setGenerated(new Generated());
			result.getGenerated().setShow(Boolean.FALSE);
		}
		if (result.getGenerated().isShow()) {
			if (StringUtils.isEmpty(result.getGenerated().getName())) {
				result.getGenerated().setName("danboh");
			}
		}

		if (result.getGoogle().getWeb() == null) {
			result.getGoogle().setWeb(new Google.Web());
		}
		if (result.getGoogle().getEnum() == null) {
			result.getGoogle().setEnum(new Google.Enum());
		}
		if (result.getGoogle().getTable() == null) {
			result.getGoogle().setTable(new Google.Table());
		}
		if (result.getGoogle().getBatch() == null) {
			result.getGoogle().setBatch(new Google.Batch());
		}
		if (result.getGoogle().getMail() == null) {
			result.getGoogle().setMail(new Google.Mail());
		}

		StringBuilder builder = new StringBuilder();
		for (Output output : result.getOutputs().getOutput()) {
			builder.append(result.getPackage());
			if (StringUtils.isNotEmpty(output.getPackageRoot())) {
				builder.append(".").append(output.getPackageRoot());
			}
			output.setPackageRoot(builder.toString());
			builder.setLength(0);
		}

		return result;
	}
}
