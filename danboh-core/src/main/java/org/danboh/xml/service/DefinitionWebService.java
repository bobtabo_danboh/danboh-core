/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml.service;

import java.io.InputStream;

import org.danboh.xml.Xao;
import org.danboh.xml.XaoException;
import org.danboh.xml.XmlServiceAdapter;
import org.danboh.xml.jaxb.definition.WebDefinition;

/**
 * 定義XMLサービスクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class DefinitionWebService extends XmlServiceAdapter<WebDefinition> {

	/**
	 * コンストラクタ
	 *
	 * @param xao
	 *            XMLバインディングオブジェクト
	 */
	public DefinitionWebService(Xao xao) {
		super(xao);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WebDefinition config(InputStream stream) throws XaoException {
		WebDefinition result = (WebDefinition) getXao().read(stream);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void output(WebDefinition xmlObject, String path) throws XaoException {
		getXao().write(xmlObject, path);
	}
}
