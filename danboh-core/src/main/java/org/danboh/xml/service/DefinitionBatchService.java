/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml.service;

import java.io.InputStream;

import org.danboh.xml.Xao;
import org.danboh.xml.XaoException;
import org.danboh.xml.XmlServiceAdapter;
import org.danboh.xml.jaxb.definition.BatchDefinition;

/**
 * 定義XMLサービスクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class DefinitionBatchService extends XmlServiceAdapter<BatchDefinition> {

	/**
	 * コンストラクタ
	 *
	 * @param xao
	 *            XMLバインディングオブジェクト
	 */
	public DefinitionBatchService(Xao xao) {
		super(xao);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BatchDefinition config(InputStream stream) throws XaoException {
		BatchDefinition result = (BatchDefinition) getXao().read(stream);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void output(BatchDefinition xmlObject, String path) throws XaoException {
		getXao().write(xmlObject, path);
	}
}
