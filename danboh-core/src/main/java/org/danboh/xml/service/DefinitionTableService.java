/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml.service;

import java.io.InputStream;

import org.danboh.xml.Xao;
import org.danboh.xml.XaoException;
import org.danboh.xml.XmlServiceAdapter;
import org.danboh.xml.jaxb.definition.TableDefinition;

/**
 * 定義XMLサービスクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class DefinitionTableService extends XmlServiceAdapter<TableDefinition> {

	/**
	 * コンストラクタ
	 *
	 * @param xao
	 *            XMLバインディングオブジェクト
	 */
	public DefinitionTableService(Xao xao) {
		super(xao);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TableDefinition config(InputStream stream) throws XaoException {
		TableDefinition result = (TableDefinition) getXao().read(stream);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void output(TableDefinition xmlObject, String path) throws XaoException {
		getXao().write(xmlObject, path);
	}
}
