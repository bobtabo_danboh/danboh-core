/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.xml;

import java.io.File;
import java.io.InputStream;

/**
 * XML解析情報を提供するインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface XmlService<E> {

	/**
	 * XMLファイルを解析しバインド情報を取得します。
	 *
	 * @param path
	 *            XMLファイルパス
	 * @return バインド情報
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public E config(String path) throws XaoException;

	/**
	 * XMLファイルを解析しバインド情報を取得します。
	 *
	 * @param file
	 *            XMLファイル
	 * @return バインド情報
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public E config(File file) throws XaoException;

	/**
	 * XMLファイルを解析しバインド情報を取得します。
	 *
	 * @param stream
	 *            XMLファイルの入力ストリーム
	 * @return バインド情報
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public E config(InputStream stream) throws XaoException;

	/**
	 * XMLデータを取得します。
	 *
	 * @param xmlObject
	 *            XMLオブジェクト
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public byte[] getBytes(E xmlObject) throws XaoException;

	/**
	 * XMLデータを出力します。
	 *
	 * @param xmlObject
	 *            XMLオブジェクト
	 * @throws XaoException
	 *             サービスアクセス例外
	 */
	public void output(E xmlObject, String path) throws XaoException;
}
