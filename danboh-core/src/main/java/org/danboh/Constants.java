/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh;

import org.apache.commons.lang.StringUtils;

/**
 * 定数インターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface Constants {
	/** ON/OFF であることを意味します */
	static final String ON = "○";
	static final String OFF = "";

	/** から文字である事を意味します */
	static final String EMPTY = StringUtils.EMPTY;
}
