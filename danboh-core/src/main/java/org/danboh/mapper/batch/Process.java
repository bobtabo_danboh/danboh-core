/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.batch;

import java.util.LinkedList;
import java.util.List;

/**
 * イベント情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Process {
	/** 処理名 */
	private String processName;
	/** メソッド名 */
	private String methodName;
	/** 戻り値名 */
	private String resultName;
	/** 戻り型 */
	private String resultType;
	/** 引数リスト */
	private List<Arg> argList = new LinkedList<Arg>();

	/**
	 * @return processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * @param processName セットする processName
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * @return methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName セットする methodName
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return resultName
	 */
	public String getResultName() {
		return resultName;
	}

	/**
	 * @param resultName セットする resultName
	 */
	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	/**
	 * @return resultType
	 */
	public String getResultType() {
		return resultType;
	}

	/**
	 * @param resultType セットする resultType
	 */
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	/**
	 * @return argList
	 */
	public List<Arg> getArgList() {
		return argList;
	}

	/**
	 * @param argList セットする argList
	 */
	public void setArgList(List<Arg> argList) {
		this.argList = argList;
	}
}