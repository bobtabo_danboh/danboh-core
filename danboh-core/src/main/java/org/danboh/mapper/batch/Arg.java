/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.batch;

/**
 * 引数情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Arg {
	/** 引数名 */
	private String argName;
	/** フィールド名 */
	private String argField;
	/** 型 */
	private String argType;

	/**
	 * @return argName
	 */
	public String getArgName() {
		return argName;
	}
	/**
	 * @param argName セットする argName
	 */
	public void setArgName(String argName) {
		this.argName = argName;
	}
	/**
	 * @return argField
	 */
	public String getArgField() {
		return argField;
	}
	/**
	 * @param argField セットする argField
	 */
	public void setArgField(String argField) {
		this.argField = argField;
	}
	/**
	 * @return argType
	 */
	public String getArgType() {
		return argType;
	}
	/**
	 * @param argType セットする argType
	 */
	public void setArgType(String argType) {
		this.argType = argType;
	}
}
