/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.enums;

import java.util.LinkedHashMap;
import java.util.Map;

import org.danboh.mapper.AbstractSelect;

/**
 * Enum情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Enums extends AbstractSelect {
	/** 日本語 */
	private String entityName;

	/** クラス名 */
	private String className;

	/** カテゴリ名 */
	private String categoryName;

	/** カラムマップ */
	private Map<String, Field> fieldMap;

	/**
	 * コンストラクタ。
	 */
	public Enums() {
		entityName = null;
		className = null;
		fieldMap = new LinkedHashMap<String, Field>();
	}

	/**
	 * エンティティ名取得
	 *
	 * @return エンティティ名
	 */
	public String getEntityName() {
		return this.entityName;
	}

	/**
	 * クラス名取得
	 *
	 * @return クラス名
	 */
	public String getClassName() {
		return this.className;
	}

	/**
	 * エンティティ名設定
	 *
	 * @param entityName
	 *            エンティティ名
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * クラス名設定
	 *
	 * @param className
	 *            クラス名
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 *
	 * @return
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 *
	 * @param categoryName
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * カラム追加
	 *
	 * @param column
	 *            カラム
	 */
	public void add(Field field) {
		// カラムの登録
		this.fieldMap.put(field.getAttributeName(), field);
	}

	/**
	 * カラムマップ取得
	 *
	 * @return カラムマップ
	 */
	public Map<String, Field> getFieldMap() {
		return this.fieldMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return getEntityName();
	}
}