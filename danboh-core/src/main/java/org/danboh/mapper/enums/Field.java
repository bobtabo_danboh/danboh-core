/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.enums;

import java.util.Map;

import org.danboh.Constants;

/**
 * フィールド情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Field implements Constants {
	/** 属性名 */
	private String attributeName;

	/** フィールド名 */
	private String fieldName;

	/** デフォルト値 */
	private String defaultValue;

	/**
	 * コンストラクタ。^
	 */
	public Field(Map<String, Enums> enumsMap) {
		attributeName = EMPTY;
		fieldName = EMPTY;
		defaultValue = EMPTY;
	}

	/**
	 * 属性名取得
	 *
	 * @return 属性名
	 */
	public String getAttributeName() {
		return this.attributeName;
	}

	/**
	 * フィールド名取得
	 *
	 * @return フィールド名
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * デフォルト値取得
	 *
	 * @return デフォルト値
	 */
	public String getDefaultValue() {
		return this.defaultValue;
	}

	/**
	 * 属性名設定
	 *
	 * @param attributeName
	 *            属性名
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * フィールド名設定
	 *
	 * @param fieldName
	 *            フィールド名
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * デフォルト値設定
	 *
	 * @param defaultValue
	 *            デフォルト値
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}