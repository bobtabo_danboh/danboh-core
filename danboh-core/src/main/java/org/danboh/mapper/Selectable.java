/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper;

/**
 * マッピングクラスの共通フィールドを提供するインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface Selectable {
	/**
	 * 識別子を取得します。
	 *
	 * @return 選択時の値として使用できる識別子
	 */
	public String getKey();

	/**
	 * クラス名を取得します。
	 *
	 * @return クラス名
	 */
	public String getClassName();
}
