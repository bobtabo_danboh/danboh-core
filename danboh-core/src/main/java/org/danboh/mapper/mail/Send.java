/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.mail;

/**
 * イベント情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Send {
	/** 処理名 */
	private String processName;
	/** メソッド名 */
	private String methodName;

	/**
	 * @return processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * @param processName セットする processName
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * @return methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName セットする methodName
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
}