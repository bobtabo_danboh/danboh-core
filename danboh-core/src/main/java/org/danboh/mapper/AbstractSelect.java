/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper;

/**
 * マッピングクラスの共通フィールド基底クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractSelect implements Selectable {
	/** シート名 */
	private String sheetName;

	/**
	 * シート名を取得します。
	 *
	 * @return sheetName シート名
	 */
	public String getSheetName() {
		return sheetName;
	}

	/**
	 * シート名を設定します。
	 *
	 * @param sheetName シート名
	 */
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
}
