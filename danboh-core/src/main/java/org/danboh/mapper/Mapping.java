/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper;

/**
 * マッピング情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Mapping {
	/** DTO側フィールド */
	private String dtoField;

	/** テーブル側フィールド */
	private String entityField;

	/** 日付フォーマット */
	private String dateString;

	/** DTOフィールド型 */
	private String dtoFieldType;

	/**
	 * @return dateString
	 */
	public String getDateString() {
		return dateString;
	}

	/**
	 * @param dateString セットする dateString
	 */
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	/**
	 * @return dtoFieldType
	 */
	public String getDtoFieldType() {
		return dtoFieldType;
	}

	/**
	 * @param dtoFieldType セットする dtoFieldType
	 */
	public void setDtoFieldType(String dtoFieldType) {
		this.dtoFieldType = dtoFieldType;
	}

	/**
	 * @return dtoField
	 */
	public String getDtoField() {
		return dtoField;
	}

	/**
	 * @param dtoField セットする dtoField
	 */
	public void setDtoField(String dtoField) {
		this.dtoField = dtoField;
	}

	/**
	 * @return entityField
	 */
	public String getEntityField() {
		return entityField;
	}

	/**
	 * @param entityField セットする entityField
	 */
	public void setEntityField(String entityField) {
		this.entityField = entityField;
	}
}
