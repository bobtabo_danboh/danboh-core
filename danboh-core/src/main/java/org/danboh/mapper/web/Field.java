/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.web;

import java.util.Map;

import org.danboh.Constants;
import org.danboh.mapper.enums.Enums;
import org.danboh.util.MappingUtil;

/**
 * フィールド情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Field implements Constants {
	/** 属性名 */
	private String attributeName;

	/** フィールド名 */
	private String fieldName;

	/** デフォルト値 */
	private String defaultValue;

	/** 型 */
	private String type;

	/** コード */
	private String code;

	/** 入力 */
	private boolean input;

	/** 必須チェック */
	private boolean required;

	/** 最小桁数 */
	private int minLength;

	/** 最大桁数 */
	private int maxLength;

	/** 整数部桁数 */
	private int precision;

	/** 小数部桁数 */
	private int scale;

	/** 日付形式 */
	private String dateFormat;

	/** メールアドレス */
	private boolean email;

	/** URL */
	private boolean url;

	/** OGNL */
	private String ognlExpression;

	/** 半角英数字 */
	private boolean alphaNumeric;

	/** 半角数字 */
	private boolean numeric;

	/** 半角ｶﾀｶﾅ */
	private boolean oneByteKana;

	/** 正規表現 */
	private String regexExpression;

	private String refEntity;

	private String refField;

	private Map<String, Enums> enumsMap;

	/**
	 * コンストラクタ。^
	 */
	public Field(Map<String, Enums> enumsMap) {
		this.enumsMap = enumsMap;
		attributeName = EMPTY;
		fieldName = EMPTY;
		defaultValue = EMPTY;
		required = false;
		precision = -1;
		scale = -1;
		dateFormat = EMPTY;
		ognlExpression = EMPTY;
		regexExpression = EMPTY;
	}

	/**
	 * 属性名取得
	 *
	 * @return 属性名
	 */
	public String getAttributeName() {
		return this.attributeName;
	}

	/**
	 * フィールド名取得
	 *
	 * @return フィールド名
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * 型取得
	 *
	 * @return 型
	 */
	public String getType() {
		if (code.equals(EMPTY)) {
			return this.type;
		} else {
			return MappingUtil.getEnumsRelativePath(this.enumsMap, getCode());
		}
	}

	/**
	 * デフォルト値取得
	 *
	 * @return デフォルト値
	 */
	public String getDefaultValue() {
		return this.defaultValue;
	}

	/**
	 * 属性名設定
	 *
	 * @param attributeName
	 *            属性名
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * フィールド名設定
	 *
	 * @param fieldName
	 *            フィールド名
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * 型設定
	 *
	 * @param type
	 *            型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * デフォルト値設定
	 *
	 * @param defaultValue
	 *            デフォルト値
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public String getOgnlExpression() {
		return ognlExpression;
	}

	public void setOgnlExpression(String ognlExpression) {
		this.ognlExpression = ognlExpression;
	}

	public String getRegexExpression() {
		return regexExpression;
	}

	public void setRegexExpression(String regexExpression) {
		this.regexExpression = regexExpression;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isEmail() {
		return email;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public boolean isUrl() {
		return url;
	}

	public void setUrl(boolean url) {
		this.url = url;
	}

	public boolean isAlphaNumeric() {
		return alphaNumeric;
	}

	public void setAlphaNumeric(boolean alphaNumeric) {
		this.alphaNumeric = alphaNumeric;
	}

	public boolean isNumeric() {
		return numeric;
	}

	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}

	public boolean isOneByteKana() {
		return oneByteKana;
	}

	public void setOneByteKana(boolean oneByteKana) {
		this.oneByteKana = oneByteKana;
	}

	public boolean isValidate() {
		return isRequired() || isEmail() || isUrl() || getMinLength() > 0
				|| getMaxLength() > 0 || !getDateFormat().equals(EMPTY)
				|| getPrecision() > 0 || !getOgnlExpression().equals(EMPTY)
				|| !getRegexExpression().equals(EMPTY) || isAlphaNumeric() || isNumeric() || isOneByteKana();
	}

	public String getRefEntity() {
		return refEntity;
	}

	public void setRefEntity(String refEntity) {
		this.refEntity = refEntity;
	}

	public String getRefField() {
		return refField;
	}

	public void setRefField(String refField) {
		this.refField = refField;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isInput() {
		return input;
	}

	public void setInput(boolean input) {
		this.input = input;
	}

}