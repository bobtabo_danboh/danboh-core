/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.web;

/**
 * メッセージ情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Message {
	/** キー */
	private String key;
	/** メッセージ */
	private String message;
	/** メッセージ種別 */
	private String type;

	/**
	 * キーを取得します。
	 *
	 * @return キー
	 */
	public String getKey() {
		return key;
	}

	/**
	 * キーを設定します。
	 *
	 * @param key キー
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * メッセージを取得します。
	 *
	 * @return メッセージ
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メッセージを設定します。
	 *
	 * @param message メッセージ
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メッセージ種別を取得します。
	 *
	 * @return メッセージ種別
	 */
	public String getType() {
		return type;
	}

	/**
	 * メッセージ種別を設定します。
	 *
	 * @param type メッセージ種別
	 */
	public void setType(String type) {
		this.type = type;
	}
}
