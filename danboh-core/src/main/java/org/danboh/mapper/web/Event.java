/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.web;

/**
 * イベント情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Event {
	/** イベント名 */
	private String eventName;
	/** 遷移元 */
	private String backward;
	/** 遷移先 */
	private String forward;
	/** メソッド名 */
	private String methodName;
	/** 入力チェック有り */
	private boolean validate;
	/** トークン */
	private String token;

	/**
	 *
	 * @return
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 *
	 * @param methodName
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 *
	 * @return
	 */
	public boolean isValidate() {
		return validate;
	}

	/**
	 *
	 * @param validate
	 */
	public void setValidate(boolean validate) {
		this.validate = validate;
	}

	/**
	 * @return token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token セットする token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 *
	 * @return
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 *
	 * @param eventName
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return backward
	 */
	public String getBackward() {
		return backward;
	}

	/**
	 * @param backward セットする backward
	 */
	public void setBackward(String backward) {
		this.backward = backward;
	}

	/**
	 * @return forward
	 */
	public String getForward() {
		return forward;
	}

	/**
	 * @param forward セットする forward
	 */
	public void setForward(String forward) {
		this.forward = forward;
	}
}