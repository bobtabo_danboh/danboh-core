/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.danboh.mapper.AbstractSelect;
import org.danboh.mapper.Mapping;
import org.danboh.util.VelocityUtil;

/**
 * Web情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Web extends AbstractSelect {

	/** 日本語 */
	private String entityName;

	/** クラス名 */
	private String className;

	/** カテゴリ名 */
	private String categoryName;

	/** カラムマップ */
	private Map<String, Field> fieldMap = new LinkedHashMap<String, Field>();

	/** マッピング */
	private Map<String, Map<String, Mapping>> mapping = new HashMap<String, Map<String, Mapping>>();

	/** サブクラス */
	private Map<String, Web> subClasses = new HashMap<String, Web>();

	/** バリデーションが一個以上あるかどうか */
	private boolean hasValidation;

	/** メッセージが一個以上あるかどうか */
	private boolean hasMessage;

	/** イベント */
	private List<Action> actionList = new ArrayList<Action>();

	/** メッセージ */
	private List<Message> messageList = new ArrayList<Message>();

	/**
	 * コンストラクタ。
	 */
	public Web() {
	}

	/**
	 * エンティティ名取得
	 *
	 * @return エンティティ名
	 */
	public String getEntityName() {
		return this.entityName;
	}

	/**
	 * クラス名取得
	 *
	 * @return クラス名
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * エンティティ名設定
	 *
	 * @param entityName
	 *            エンティティ名
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * クラス名設定
	 *
	 * @param className
	 *            クラス名
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * カラム追加
	 *
	 * @param column
	 *            カラム
	 */
	public void add(Field field) {
		// カラムの登録
		this.fieldMap.put(field.getAttributeName(), field);
	}

	/**
	 * カラムマップ取得
	 *
	 * @return カラムマップ
	 */
	public Map<String, Field> getFieldMap() {
		return this.fieldMap;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, Map<String, Mapping>> getMapping() {
		return mapping;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, Web> getSubClasses() {
		return subClasses;
	}

	/**
	 *
	 * @return
	 */
	public boolean getHasValidation() {
		return hasValidation;
	}

	/**
	 *
	 * @param hasValidation
	 */
	public void setHasValidation(boolean hasValidation) {
		this.hasValidation = hasValidation;
	}

	/**
	 *
	 * @return
	 */
	public boolean getHasMessage() {
		return hasMessage;
	}

	/**
	 *
	 * @param hasMessage
	 */
	public void setHasMessage(boolean hasMessage) {
		this.hasMessage = hasMessage;
	}

	/**
	 *
	 * @return
	 */
	public List<Action> getActionList() {
		return actionList;
	}

	/**
	 *
	 * @param actionList
	 */
	public void setActionList(List<Action> actionList) {
		this.actionList = actionList;
	}

	/**
	 *
	 * @return
	 */
	public List<Message> getMessageList() {
		return messageList;
	}

	/**
	 *
	 * @param messageList
	 */
	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}

	/**
	 *
	 * @return
	 */
	public String getFileName() {
		return VelocityUtil.toSeparated(VelocityUtil.smallOne(className), '-');
	}

	/**
	 *
	 * @return
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 *
	 * @param categoryName
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return getCategoryName() + ":" + getEntityName();
	}
}