/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.web;

import java.util.ArrayList;
import java.util.List;

import org.danboh.mapper.AbstractSelect;
import org.danboh.util.VelocityUtil;

/**
 * アクション情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Action extends AbstractSelect {

	/** 日本語 */
	private String actionName;

	/** クラス名 */
	private String className;

	/** カテゴリ名 */
	private String categoryName;

	/** イベント */
	private List<Event> eventList = new ArrayList<Event>();

	/**
	 * コンストラクタ。
	 */
	public Action() {
	}

	/**
	 * アクション名取得
	 *
	 * @return アクション名
	 */
	public String getActionName() {
		return this.actionName;
	}

	/**
	 * クラス名取得
	 *
	 * @return クラス名
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * アクション名設定
	 *
	 * @param actionName
	 *            アクション名
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	/**
	 * クラス名設定
	 *
	 * @param className
	 *            クラス名
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 *
	 * @return
	 */
	public List<Event> getEventList() {
		return eventList;
	}

	/**
	 *
	 * @param eventList
	 */
	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

	/**
	 *
	 * @return
	 */
	public String getFileName() {
		return VelocityUtil.toSeparated(VelocityUtil.smallOne(className), '-');
	}

	/**
	 *
	 * @return
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 *
	 * @param categoryName
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return getCategoryName() + ":" + getActionName();
	}
}