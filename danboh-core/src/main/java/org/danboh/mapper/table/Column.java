/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.table;

import java.util.Map;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.StringUtils;
import org.danboh.Constants;
import org.danboh.mapper.enums.Enums;
import org.danboh.util.MappingUtil;
import org.danboh.xml.jaxb.setting.Conversion;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;

/**
 * カラム情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Column implements Constants {

	/** 属性名 */
	private String attributeName;

	/** カラム名 */
	private String columnName;

	/** フィールド名 */
	private String fieldName;

	/** 型 */
	private String type;

	/** 整数桁 */
	private int intNum;

	/** 小数桁 */
	private int decNum;

	/** 主キー */
	private boolean primaryKey;

	/** 非NULL */
	private boolean notNull;

	/** デフォルト値 */
	private String defaultValue;

	/** 参照エンティティ名 */
	private String referEntityName;

	/** 参照属性名 */
	private String referAttributeName;

	/** 参照フィールド名 */
	private String referFieldName;

	/** シーケンス名 */
	private String sequenceName;

	/** 自動採番 */
	private boolean identity;

	/** unsigned */
	private boolean unsigned;

	/** ユニーク */
	private String unique;

	/** インデックス */
	private String index1;
	private String index2;
	private String index3;
	private String index4;
	private String index5;

	/** バージョン管理 */
	private String versionTimestamp;

	/**  */
	private String code;

	/**  */
	private Map<String, Enums> enumsMap;

	private Setting setting;

	private Output enumOutput;

	/**
	 * コンストラクタ。
	 */
	public Column(Setting setting, Map<String, Enums> enumsMap, Output enumOutput) {
		this.setting = setting;
		this.enumsMap = enumsMap;
		this.enumOutput = enumOutput;
		attributeName = EMPTY;
		columnName = EMPTY;
		fieldName = EMPTY;
		type = EMPTY;
		intNum = 0;
		decNum = 0;
		primaryKey = false;
		notNull = false;
		defaultValue = EMPTY;
		referEntityName = EMPTY;
		referAttributeName = EMPTY;
		referFieldName = EMPTY;
		versionTimestamp = EMPTY;
		identity = false;
		unsigned = false;
		unique = EMPTY;
		index1 = EMPTY;
		index2 = EMPTY;
		index3 = EMPTY;
		index4 = EMPTY;
		index5 = EMPTY;
	}

	/**
	 * 属性名取得
	 *
	 * @return 属性名
	 */
	public String getAttributeName() {
		return this.attributeName;
	}

	/**
	 * カラム名取得
	 *
	 * @return カラム名
	 */
	public String getColumnName() {
		return this.columnName;
	}

	/**
	 * フィールド名取得
	 *
	 * @return フィールド名
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * 型取得
	 *
	 * @return 型
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * 整数桁取得
	 *
	 * @return 整数桁
	 */
	public int getIntNum() {
		return this.intNum;
	}

	/**
	 * 小数桁取得
	 *
	 * @return 小数桁
	 */
	public int getDecNum() {
		return this.decNum;
	}

	/**
	 * 主キー取得
	 *
	 * @return 主キー
	 */
	public boolean getPrimaryKey() {
		return this.primaryKey;
	}

	/**
	 * 非NULL取得
	 *
	 * @return 非NULL
	 */
	public boolean getNotNull() {
		return this.notNull;
	}

	/**
	 * デフォルト値取得
	 *
	 * @return デフォルト値
	 */
	public String getDefaultValue() {
		return this.defaultValue;
	}

	/**
	 * 参照エンティティ名取得
	 *
	 * @return 参照エンティティ名
	 */
	public String getReferEntityName() {
		return this.referEntityName;
	}

	/**
	 * 参照属性名取得
	 *
	 * @return 参照属性名
	 */
	public String getReferAttributeName() {
		return this.referAttributeName;
	}

	/**
	 * 参照フィールド名取得
	 *
	 * @return 参照フィールド名
	 */
	public String getReferFieldName() {
		return this.referFieldName;
	}

	/**
	 * シーケンス名取得
	 *
	 * @return シーケンス名
	 */
	public String getSequenceName() {
		return this.sequenceName;
	}

	/**
	 * versionTimestamp取得
	 *
	 * @return versionTimestamp
	 */
	public String getVersionTimestamp() {
		return this.versionTimestamp;
	}

	/**
	 * identity取得
	 *
	 * @return identity
	 */
	public boolean getIdentity() {
		return this.identity;
	}

	/**
	 * unsigned取得
	 *
	 * @return unsigned
	 */
	public boolean getUnsigned() {
		return this.unsigned;
	}

	/**
	 * ユニーク取得
	 *
	 * @return ユニーク
	 */
	public String getUnique() {
		return this.unique;
	}

	/**
	 * 属性名設定
	 *
	 * @param attributeName
	 *            属性名
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * カラム名設定
	 *
	 * @param columnName
	 *            カラム名
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * フィールド名設定
	 *
	 * @param fieldName
	 *            フィールド名
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * 型設定
	 *
	 * @param type
	 *            型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 整数桁設定
	 *
	 * @param intNum
	 *            整数桁
	 */
	public void setIntNum(int intNum) {
		this.intNum = intNum;
	}

	/**
	 * 小数桁設定
	 *
	 * @param decNum
	 *            小数桁
	 */
	public void setDecNum(int decNum) {
		this.decNum = decNum;
	}

	/**
	 * 主キー設定
	 *
	 * @param primaryKey
	 *            主キー
	 */
	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	/**
	 * 非NULL設定
	 *
	 * @param notNull
	 *            非NULL
	 */
	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}

	/**
	 * デフォルト値設定
	 *
	 * @param defaultValue
	 *            デフォルト値
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * 参照エンティティ名設定
	 *
	 * @param referEntityName
	 *            参照エンティティ名
	 */
	public void setReferEntityName(String referEntityName) {
		this.referEntityName = referEntityName;
	}

	/**
	 * 参照属性名設定
	 *
	 * @param referAttributeName
	 *            参照属性名
	 */
	public void setReferAttributeName(String referAttributeName) {
		this.referAttributeName = referAttributeName;
	}

	/**
	 * 参照フィールド名設定
	 *
	 * @param referFieldName
	 *            参照フィールド名
	 */
	public void setReferFieldName(String referFieldName) {
		this.referFieldName = referFieldName;
	}

	/**
	 * シーケンス名設定
	 *
	 * @return シーケンス名
	 */
	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}

	/**
	 * versionTimestamp設定
	 *
	 * @return versionTimestamp
	 */
	public void setVersionTimestamp(String versionTimestamp) {
		this.versionTimestamp = versionTimestamp;
	}

	/**
	 * identity設定
	 *
	 * @return identity
	 */
	public void setIdentity(boolean identity) {
		this.identity = identity;
	}

	/**
	 * unsigned設定
	 *
	 * @return unsigned
	 */
	public void setUnsigned(boolean unsigned) {
		this.unsigned = unsigned;
	}

	/**
	 * ユニーク設定
	 *
	 * @param unique
	 *            ユニーク
	 */
	public void setUnique(String unique) {
		this.unique = unique;
	}

	/**
	 * Java型を取得します。
	 *
	 * @return Java型
	 */
	public String getJavaType() {
		if (code == null || code.equals(EMPTY)) {
			BeanPredicate predicate = new BeanPredicate("sql", PredicateUtils.equalPredicate(type));
			Conversion conversion = (Conversion) CollectionUtils.find(setting.getDatabase().getConversions().getConversion(),
					predicate);
			if (conversion == null) {
				throw new RuntimeException("getJavaType Not Found. ColumnType[" + type + "]");
			}
			return conversion.getType();
		} else {
			StringBuilder builder = new StringBuilder();
			if (this.enumOutput != null) {
				builder.append(MappingUtil.getEnumsAbsolutePath(setting.getPackage(), this.enumOutput.getPackageRoot(), enumsMap, code));
			} else {
				builder.append(MappingUtil.getEnumsAbsolutePath(setting.getPackage(), StringUtils.EMPTY, enumsMap, code));
			}
			return builder.toString();
		}
	}

	/**
	 * 文字列化します。
	 *
	 * @return 文字列
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.columnName).append(", ");
		buffer.append(this.type).append(", ");
		buffer.append(this.intNum).append(", ");
		buffer.append(this.decNum).append(", ");
		buffer.append(this.defaultValue).append(", ");
		buffer.append(this.primaryKey).append(", ");
		buffer.append(this.notNull).append(", ");
		buffer.append(this.sequenceName).append(", ");
		buffer.append(this.identity).append(", ");
		buffer.append(this.unsigned).append(", ");
		buffer.append(this.unique).append(", ");
		buffer.append(this.index1).append(", ");
		buffer.append(this.index2).append(", ");
		buffer.append(this.index3).append(", ");
		buffer.append(this.index4).append(", ");
		buffer.append(this.index5);
		return buffer.toString();
	}

	/**
	 *
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 *
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public String getIndex1() {
		return index1;
	}

	public void setIndex1(String index1) {
		this.index1 = index1;
	}

	public String getIndex2() {
		return index2;
	}

	public void setIndex2(String index2) {
		this.index2 = index2;
	}

	public String getIndex3() {
		return index3;
	}

	public void setIndex3(String index3) {
		this.index3 = index3;
	}

	public String getIndex4() {
		return index4;
	}

	public void setIndex4(String index4) {
		this.index4 = index4;
	}

	public String getIndex5() {
		return index5;
	}

	public void setIndex5(String index5) {
		this.index5 = index5;
	}
}