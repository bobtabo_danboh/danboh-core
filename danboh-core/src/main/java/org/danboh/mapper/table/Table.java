/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.danboh.mapper.AbstractSelect;
import org.danboh.util.VelocityUtil;
import org.danboh.xml.jaxb.setting.ForeignKey;
import org.danboh.xml.jaxb.setting.Setting;

/**
 * テーブル情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Table extends AbstractSelect {

	/** ファイル名 */
	private String fileName;

	/** テーブル名 */
	private String tableName;

	/** エンティティ名 */
	private String entityName;

	/** クラス名 */
	private String className;

	/** スキーマ名 */
	private String schemaName;

	/** 読み取り専用 */
	private boolean readOnly;

	/** カラムマップ */
	private Map<String, Column> columnMap;

	/** 主キーリスト */
	private List<Column> primaryList;

	/** ユニークキーマップ */
	private Map<String, Column> uniqueMap;

	/** メソッドリスト */
	private List<Relation> relationList;

	/** INDEXマップ */
	private Map<String, Column> index1Map;
	private Map<String, Column> index2Map;
	private Map<String, Column> index3Map;
	private Map<String, Column> index4Map;
	private Map<String, Column> index5Map;

	private Setting setting;

	/**
	 * コンストラクタ。
	 */
	public Table(Setting setting) {
		this.setting = setting;
		columnMap = new LinkedHashMap<String, Column>();
		primaryList = new ArrayList<Column>();
		uniqueMap = new TreeMap<String, Column>();
		relationList = new ArrayList<Relation>();
		index1Map = new TreeMap<String, Column>();
		index2Map = new TreeMap<String, Column>();
		index3Map = new TreeMap<String, Column>();
		index4Map = new TreeMap<String, Column>();
		index5Map = new TreeMap<String, Column>();

	}

	/**
	 * テーブル名を取得します。
	 *
	 * @return テーブル名
	 */
	public String getTableName() {
		return this.tableName;
	}

	/**
	 * エンティティ名を取得します。
	 *
	 * @return エンティティ名
	 */
	public String getEntityName() {
		return this.entityName;
	}

	/**
	 * クラス名を取得します。
	 *
	 * @return クラス名
	 */
	public String getClassName() {
		return this.className;
	}

	/**
	 * スキーマ名を取得します。
	 *
	 * @return スキーマ名
	 */
	public String getSchemaName() {
		return this.schemaName;
	}

	/**
	 * 読み取り専用フラグを取得します。
	 *
	 * @return 読み取り専用フラグ
	 */
	public boolean getReadOnly() {
		return this.readOnly;
	}

	/**
	 * テーブル名を設定します。
	 *
	 * @param tableName
	 *            テーブル名
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * エンティティ名を設定します。
	 *
	 * @param entityName
	 *            エンティティ名
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * クラス名を設定します。
	 *
	 * @param className
	 *            クラス名
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * スキーマ名を設定します。
	 *
	 * @param schemaName
	 *            スキーマ名
	 */
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	/**
	 * 読み取り専用フラグを設定します。
	 *
	 * @param readOnly
	 *            読み取り専用フラグ
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * カラムを追加します。
	 *
	 * @param column
	 *            カラム
	 */
	public void add(Column column) {
		// カラムの登録
		this.columnMap.put(column.getAttributeName(), column);

		// 主キーの登録
		if (column.getPrimaryKey()) {
			this.primaryList.add(column);
		}

		// ユニークの登録
		if (StringUtils.isNotEmpty(column.getUnique())) {
			this.uniqueMap.put(column.getUnique(), column);
		}

		// INDEXの登録
		if (StringUtils.isNotEmpty(column.getIndex1())) {
			this.index1Map.put(column.getIndex1(), column);
		}
		if (StringUtils.isNotEmpty(column.getIndex2())) {
			this.index2Map.put(column.getIndex2(), column);
		}
		if (StringUtils.isNotEmpty(column.getIndex3())) {
			this.index3Map.put(column.getIndex4(), column);
		}
		if (StringUtils.isNotEmpty(column.getIndex4())) {
			this.index4Map.put(column.getIndex4(), column);
		}
		if (StringUtils.isNotEmpty(column.getIndex5())) {
			this.index5Map.put(column.getIndex5(), column);
		}
	}

	/**
	 * カラムマップを取得します。
	 *
	 * @return カラムマップ
	 */
	public Map<String, Column> getColumnMap() {
		return this.columnMap;
	}

	/**
	 * 主キーリストを取得します。
	 *
	 * @return 主キーリスト
	 */
	public List<Column> getPrimaryList() {
		return this.primaryList;
	}

	/**
	 * ユニークマップを取得します。
	 *
	 * @return ユニークマップ
	 */
	public Map<String, Column> getUniqueMap() {
		return this.uniqueMap;
	}

	/**
	 * @return index1Map
	 */
	public Map<String, Column> getIndex1Map() {
		return index1Map;
	}

	/**
	 * @param index1Map セットする index1Map
	 */
	public void setIndex1Map(Map<String, Column> index1Map) {
		this.index1Map = index1Map;
	}

	/**
	 * @return index2Map
	 */
	public Map<String, Column> getIndex2Map() {
		return index2Map;
	}

	/**
	 * @param index2Map セットする index2Map
	 */
	public void setIndex2Map(Map<String, Column> index2Map) {
		this.index2Map = index2Map;
	}

	/**
	 * @return index3Map
	 */
	public Map<String, Column> getIndex3Map() {
		return index3Map;
	}

	/**
	 * @param index3Map セットする index3Map
	 */
	public void setIndex3Map(Map<String, Column> index3Map) {
		this.index3Map = index3Map;
	}

	/**
	 * @return index4Map
	 */
	public Map<String, Column> getIndex4Map() {
		return index4Map;
	}

	/**
	 * @param index4Map セットする index4Map
	 */
	public void setIndex4Map(Map<String, Column> index4Map) {
		this.index4Map = index4Map;
	}

	/**
	 * @return index5Map
	 */
	public Map<String, Column> getIndex5Map() {
		return index5Map;
	}

	/**
	 * @param index5Map セットする index5Map
	 */
	public void setIndex5Map(Map<String, Column> index5Map) {
		this.index5Map = index5Map;
	}

	/**
	 *
	 * @return
	 */
	public List<Relation> getRelationList() {
		return relationList;
	}

	/**
	 *
	 * @param relationList
	 */
	public void setRelationList(List<Relation> relationList) {
		this.relationList = relationList;
	}

	/**
	 *
	 * @return
	 */
	public String getCompleteTableName() {
		return schemaName.length() == 0 ? tableName
				: schemaName + "."
						+ tableName;
	}

	/**
	 * 文字列化
	 *
	 * @return 文字列
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[Table:テーブル名=" + this.tableName + ", エンティティ名="
				+ entityName + ", クラス名=" + className + "{");
		for (Iterator<Column> it = columnMap.values().iterator(); it.hasNext();) {
			buffer.append(it.next());
			buffer.append(",");
		}
		buffer.delete(buffer.length() - 1, buffer.length());
		buffer.append("}]");
		return buffer.toString();
	}

	/**
	 * SQL文生成の行数取得
	 *
	 * @return 行数
	 */
	public int getCreateRowCount() {
		int result = 0;
		if (0 < this.primaryList.size()) {
			result = 1;
		}
		int uniqueSize = (this.uniqueMap.size() > 0) ? 1 : 0;
		int index1Size = (this.index1Map.size() > 0) ? 1 : 0;
		int index2Size = (this.index2Map.size() > 0) ? 1 : 0;
		int index3Size = (this.index3Map.size() > 0) ? 1 : 0;
		int index4Size = (this.index4Map.size() > 0) ? 1 : 0;
		int index5Size = (this.index5Map.size() > 0) ? 1 : 0;

		int foreignKeySize = 0;
		for (Relation rel : this.relationList) {
			for (ForeignKey fk : this.setting.getDatabase().getForeignKeys().getForeignKey()) {
				if (VelocityUtil.isOutPutForeignKey(fk, rel)) {
					foreignKeySize++;
					break;
				}
			}
		}

		return result + this.columnMap.size() + uniqueSize + index1Size + index2Size + index3Size + index4Size
				+ index5Size + foreignKeySize;
	}

	/**
	 *
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 *
	 * @param fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return getEntityName();
	}

	/**
	 *
	 * @return
	 */
	public List<Column> getUniqueList() {
		return toColumnList(this.getUniqueMap());
	}

	/**
	 *
	 * @return
	 */
	public List<Column> getIndex1List() {
		return toColumnList(this.getIndex1Map());
	}

	/**
	 *
	 * @return
	 */
	public List<Column> getIndex2List() {
		return toColumnList(this.getIndex2Map());
	}

	/**
	 *
	 * @return
	 */
	public List<Column> getIndex3List() {
		return toColumnList(this.getIndex3Map());
	}

	/**
	 *
	 * @return
	 */
	public List<Column> getIndex4List() {
		return toColumnList(this.getIndex4Map());
	}

	/**
	 *
	 * @return
	 */
	public List<Column> getIndex5List() {
		return toColumnList(this.getIndex5Map());
	}

	/**
	 *
	 * @param map
	 * @return
	 */
	private List<Column> toColumnList(Map<String, Column> map) {
		List<Column> result = new ArrayList<Column>();
		for (Entry<String, Column> entry : map.entrySet()) {
			result.add(entry.getValue());
		}
		return result;
	}
}