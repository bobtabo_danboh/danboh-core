/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.mapper.table;

import java.util.Map;

import org.danboh.Constants;

/**
 * リレーション情報を格納するクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Relation implements Constants {

	/** エンティティ名 */
	private String entityName;

	/** テーブル名 */
	private String tableName;

	/** フィールド名 */
	private String fieldName;

	/** フィールド名 */
	private String originalFieldName;

	/** 多重度 */
	private String multiplicity;

	/**  */
	private String mappedBy;
	private String joinColumn;
	private String classpath;
	private boolean relation = true;

	private Table table;
	@SuppressWarnings("unused")
	private Map<String, Table> tableMap;

	/**
	 * コンストラクタ。
	 */
	public Relation(Table table, Map<String, Table> tableMap) {
		this.entityName = EMPTY;
		this.multiplicity = EMPTY;

		this.table = table;
		this.tableMap = tableMap;
	}

	/**
	 *
	 * @return
	 */
	public String getMultiplicity() {
		return multiplicity;
	}

	/**
	 *
	 * @param multiplicity
	 */
	public void setMultiplicity(String multiplicity) {
		this.multiplicity = multiplicity;
	}

	/**
	 *
	 * @return
	 */
	public String getMappedBy() {
		return mappedBy;
	}

	/**
	 *
	 * @param mappedBy
	 */
	public void setMappedBy(String mappedBy) {
		this.mappedBy = mappedBy;
	}

	/**
	 *
	 * @return
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 *
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName セットする tableName
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 *
	 * @return
	 */
	public Table getTable() {
		return table;
	}

	/**
	 *
	 * @param table
	 */
	public void setTable(Table table) {
		this.table = table;
	}

	/**
	 *
	 * @return
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 *
	 * @return
	 */
	public boolean isToMany() {
		return multiplicity.endsWith("Many");
	}

	/**
	 *
	 * @param fieldName
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 *
	 * @return
	 */
	public String getJoinColumn() {
		return joinColumn;
	}

	/**
	 *
	 * @param joinColumn
	 */
	public void setJoinColumn(String joinColumn) {
		this.joinColumn = joinColumn;
	}

	/**
	 *
	 * @return
	 */
	public boolean isRelation() {
		return relation;
	}

	/**
	 *
	 * @param relation
	 */
	public void setRelation(boolean relation) {
		this.relation = relation;
	}

	/**
	 *
	 * @return
	 */
	public String getClasspath() {
		return classpath;
	}

	/**
	 *
	 * @param classpath
	 */
	public void setClasspath(String classpath) {
		this.classpath = classpath;
	}

	/**
	 * @return originalFieldName
	 */
	public String getOriginalFieldName() {
		return originalFieldName;
	}

	/**
	 * @param originalFieldName セットする originalFieldName
	 */
	public void setOriginalFieldName(String originalFieldName) {
		this.originalFieldName = originalFieldName;
	}
}