/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh;

import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.lang.ArrayUtils;
import org.apache.velocity.app.Velocity;
import org.danboh.converter.Converter;
import org.danboh.maker.Maker;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.table.Table;
import org.danboh.mapper.web.Web;
import org.danboh.reader.ReaderFactory;
import org.danboh.xml.ServiceLocator;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceType;
import org.danboh.xml.service.SettingService;

/**
 * ソースコード生成メインクラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Main {
	/**
	 * ソースコード生成を開始します。
	 *
	 * @param args コマンドライン引数（第1引数：Setting.xml、第2引数：変換方式、第3引数：変換ソース）
	 * @throws Exception ソースコード生成失敗時にスローされる例外です
	 */
	public static void main(String[] args) throws Exception {
		ResourceBundle resource = ResourceBundle.getBundle("danboh");

		boolean toXml = (ArrayUtils.indexOf(args, resource.getString("to.xml")) > ArrayUtils.INDEX_NOT_FOUND);
		boolean toGoogle = (ArrayUtils.indexOf(args, resource.getString("to.google")) > ArrayUtils.INDEX_NOT_FOUND);

		// 設定
		SettingService service = ServiceLocator.getSettingService();
		Setting setting = service.config(args[0]);

		// Google変換
		if (toGoogle) {
			Console.out("Google変換開始！");
			Converter converter = Converter.getInstance(setting);
			if (ArrayUtils.indexOf(args, SourceType.WEB.value()) > ArrayUtils.INDEX_NOT_FOUND) {
				converter.toGoogle(SourceType.WEB);
			}
			if (ArrayUtils.indexOf(args, SourceType.ENUM.value()) > ArrayUtils.INDEX_NOT_FOUND) {
				converter.toGoogle(SourceType.ENUM);
			}
			if (ArrayUtils.indexOf(args, SourceType.TABLE.value()) > ArrayUtils.INDEX_NOT_FOUND) {
				converter.toGoogle(SourceType.TABLE);
			}
			if (ArrayUtils.indexOf(args, SourceType.BATCH.value()) > ArrayUtils.INDEX_NOT_FOUND) {
				converter.toGoogle(SourceType.BATCH);
			}
			if (ArrayUtils.indexOf(args, SourceType.MAIL.value()) > ArrayUtils.INDEX_NOT_FOUND) {
				converter.toGoogle(SourceType.MAIL);
			}
			Console.out("Google変換終了！");
			System.exit(0);
		}

		// 読込
		ReaderFactory factory = ReaderFactory.getInstance(setting, toXml);
		Map<String, Enums> enumMap = factory.getEnumReader().parse();
		Map<String, Table> tableMap = factory.getTableReader(enumMap).parse();
		Map<String, Web> webMap = factory.getWebReader(tableMap, enumMap).parse();
		Map<String, Batch> batchMap = factory.getBatchReader(tableMap, enumMap).parse();
		Map<String, Mail> mailMap = factory.getMailReader(tableMap, enumMap).parse();

		// XML出力
		if (toXml) {
			Console.out("XML出力開始！");
			Converter converter = Converter.getInstance(setting);
			converter.toXml(SourceType.ENUM, enumMap);
			converter.toXml(SourceType.TABLE, tableMap);
			converter.toXml(SourceType.WEB, webMap);
			converter.toXml(SourceType.BATCH, batchMap);
			converter.toXml(SourceType.MAIL, mailMap);
			Console.out("XML出力終了！");
			System.exit(0);
		}

		// 初期化
		Console.out("ソース生成開始！");
		Properties properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("velocity.properties"));
		Velocity.init(properties);

		// 生成
		Maker maker = new Maker(setting);
		for (Output output : setting.getOutputs().getOutput()) {
			maker.make(output, webMap, tableMap, enumMap, batchMap, mailMap);
		}
		Console.out("ソース生成終了！");
	}
}