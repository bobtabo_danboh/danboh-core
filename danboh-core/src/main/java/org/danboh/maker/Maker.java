/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.maker;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.danboh.Console;
import org.danboh.maker.runner.BatchMakeRunner;
import org.danboh.maker.runner.EnumMakeRunner;
import org.danboh.maker.runner.MailMakeRunner;
import org.danboh.maker.runner.MultiFileMakeRunner;
import org.danboh.maker.runner.SingleFileMakeRunner;
import org.danboh.maker.runner.TableMakeRunner;
import org.danboh.maker.runner.WebActionMakeRunner;
import org.danboh.maker.runner.WebMakeRunner;
import org.danboh.mapper.batch.Batch;
import org.danboh.mapper.enums.Enums;
import org.danboh.mapper.mail.Mail;
import org.danboh.mapper.table.Table;
import org.danboh.mapper.web.Action;
import org.danboh.mapper.web.Web;
import org.danboh.util.VelocityUtil;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceSubType;

/**
 * ソースコード生成クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Maker {
	/** コンテキスト */
	private VelocityContext context;

	private Setting setting;

	/**
	 * コンストラクタ。
	 * @param setting
	 */
	public Maker(Setting setting) {
		this.setting = setting;
	}

	/**
	 * ソースコードを生成します。
	 *
	 * @param webMap Web定義マップ
	 * @param tableMap テーブル定義マップ
	 * @param enumMap Enum定義マップ
	 * @param enumMap バッチ定義マップ
	 * @param output 出力オブジェクト
	 * @throws Exception 生成失敗時にスローされる例外クラスです
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void make(Output output, Map<String, Web> webMap, Map<String, Table> tableMap,
			Map<String, Enums> enumMap, Map<String, Batch> batchMap, Map<String, Mail> mailMap)
			throws Exception {
		this.context = new VelocityContext();

		// 設定
		this.context.put("setting", setting);
		this.context.put("output", output);

		// ユーティリティ
		this.context.put("util", VelocityUtil.getInstance());

		this.context.put("tableMap", tableMap);
		this.context.put("enumMap", enumMap);

		String[] skipSheets = StringUtils.split(output.getSkip(), ",");

		MakeRunnable runner = null;
		MakeRunnable runnerSub = null;

		Console.out(output.getType().name(), " 出力開始！");

		switch (output.getType()) {
			case WEB:
				this.context.put("webMap", webMap);
				runner = new WebMakeRunner(this.setting, this.context);
				runnerSub = new WebActionMakeRunner(this.setting, this.context);
				for (Iterator<Web> it = webMap.values().iterator(); it.hasNext();) {
					Web web = it.next();
					if (ArrayUtils.contains(skipSheets, web.getSheetName())) {
						continue;
					}
					if (SourceSubType.ACTION.equals(output.getSubType())) {
						for (Action action : web.getActionList()) {
							this.context.put("web", web);
							runnerSub.make(output, action);
						}
					} else {
						runner.make(output, web);
					}
				}
				break;
			case ENUM:
				this.context.put("webMap", webMap);
				runner = new EnumMakeRunner(this.setting, this.context);
				for (Iterator<Enums> it = enumMap.values().iterator(); it.hasNext();) {
					Enums enums = it.next();
					if (ArrayUtils.contains(skipSheets, enums.getSheetName())) {
						continue;
					}
					runner.make(output, enums);
				}
				break;
			case TABLE:
				this.context.put("webMap", webMap);
				runner = new TableMakeRunner(this.setting, this.context);
				for (Iterator<Table> it = tableMap.values().iterator(); it.hasNext();) {
					Table table = it.next();
					if (ArrayUtils.contains(skipSheets, table.getSchemaName())) {
						continue;
					}
					runner.make(output, table);
				}
				break;
			case BATCH:
				this.context.put("batchMap", batchMap);
				runner = new BatchMakeRunner(this.setting, this.context);
				for (Iterator<Batch> it = batchMap.values().iterator(); it.hasNext();) {
					Batch batch = it.next();
					if (ArrayUtils.contains(skipSheets, batch.getSheetName())) {
						continue;
					}
					runner.make(output, batch);
				}
				break;
			case MAIL:
				this.context.put("mailMap", mailMap);
				runner = new MailMakeRunner(this.setting, this.context);
				for (Iterator<Mail> it = mailMap.values().iterator(); it.hasNext();) {
					Mail mail = it.next();
					if (ArrayUtils.contains(skipSheets, mail.getSheetName())) {
						continue;
					}
					runner.make(output, mail);
				}
				break;
			case SINGLE_FILE:
				runner = new SingleFileMakeRunner(this.setting, this.context);
				runner.make(output, null);
				break;
			case MULTI_FILE:
				runner = new MultiFileMakeRunner(this.setting, this.context);
				int i = 1;
				for (Iterator<Table> it = tableMap.values().iterator(); it.hasNext();) {
					Table table = it.next();
					if (ArrayUtils.contains(skipSheets, table.getSheetName())) {
						continue;
					}
					runner.make(output, table, i);
					i++;
				}
				break;
			default:
				break;
		}

		Console.out(output.getType().name(), " 出力終了！");
	}
}