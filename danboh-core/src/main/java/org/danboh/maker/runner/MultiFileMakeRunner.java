/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
/**
 *
 */
package org.danboh.maker.runner;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.maker.AbstractMakeRunner;
import org.danboh.mapper.table.Table;

/**
 * 複数ファイル生成クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class MultiFileMakeRunner extends AbstractMakeRunner<Table> {

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param context コンテキスト
	 */
	public MultiFileMakeRunner(Setting setting, VelocityContext context) {
		super(setting, context);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void make(Output output, Table element, Object... params) throws Exception {
		String packageName = getPackageName(output, null);
		getContext().put("packageName", packageName);
		getContext().put("author", getSetting().getAuthor());
		File dir = new File(getPackagePath(packageName, output));
		dir.mkdirs();
		File file = new File(dir, StringUtils.leftPad(String.valueOf(params[0]), 3, "0") + output.getPrefix() + element.getTableName()
				+ output.getSuffix() + "." + output.getExtention());
		getContext().put("table", element);

		outputFile(output, file);
	}
}
