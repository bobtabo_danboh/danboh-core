/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.maker.runner;

import java.io.File;

import org.apache.velocity.VelocityContext;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.maker.AbstractMakeRunner;

/**
 * 単一ファイル生成クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class SingleFileMakeRunner extends AbstractMakeRunner<Object> {

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param context コンテキスト
	 */
	public SingleFileMakeRunner(Setting setting, VelocityContext context) {
		super(setting, context);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void make(Output output, Object element, Object... params) throws Exception {
		String packageName = getPackageName(output, null);
		getContext().put("packageName", packageName);
		getContext().put("author", getSetting().getAuthor());
		File dir = new File(getPackagePath(packageName, output));
		dir.mkdirs();
		File file = new File(dir, output.getPrefix() + output.getSuffix()
				+ "." + output.getExtention());

		outputFile(output, file);
	}
}
