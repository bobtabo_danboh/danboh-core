/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.maker.runner;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.maker.AbstractMakeRunner;
import org.danboh.mapper.web.Action;
import org.danboh.mapper.web.Web;

/**
 * Webアクション関連ソース生成クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class WebActionMakeRunner extends AbstractMakeRunner<Action> {

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param context コンテキスト
	 */
	public WebActionMakeRunner(Setting setting, VelocityContext context) {
		super(setting, context);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void make(Output output, Action element, Object... params) throws Exception {
		// 除外カテゴリ対応
		if (StringUtils.isNotBlank(output.getExcludesCategory())) {
			String[] excludesCategories = output.getExcludesCategory().split(",");
			for (String excludesCategory : excludesCategories) {
				if (excludesCategory.equals(element.getCategoryName())) {
					return;
				}
			}
		}

		String packageName = getPackageName(output, element.getCategoryName());
		getContext().put("packageName", packageName);
		getContext().put("author", getSetting().getAuthor());
		if (StringUtils.isNotBlank(output.getExistsDataCheckMethod())) {
			PropertyDescriptor property = new PropertyDescriptor(output.getExistsDataCheckMethod(), Web.class);
			Method method = property.getReadMethod();
			Boolean hasData = (Boolean) method.invoke(element);
			if (!hasData) {
				File file = new File(new File(getPackagePath(packageName, output)),
						output.getPrefix() + element.getClassName()
								+ output.getSuffix() + "." + output.getExtention());
				if (file.exists()) {
					file.delete();
				}
				return;
			}
		}
		File dir = new File(getPackagePath(packageName, output));
		dir.mkdirs();
		File file = new File(dir, output.getPrefix() + element.getClassName()
				+ output.getSuffix() + "." + output.getExtention());

		getContext().put("action", element);

		outputFile(output, file);
	}
}
