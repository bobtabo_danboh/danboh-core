/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.maker;

import org.danboh.xml.jaxb.setting.Output;

/**
 * ソースコード生成を提供するインターフェースです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public interface MakeRunnable<E> {
	/**
	 * 生成します。
	 *
	 * @param output 出力情報
	 * @param element 生成情報
	 * @param params パラメータ
	 * @throws Exception 生成失敗時にスローされる例外です
	 */
	public void make(Output output, E element, Object... params) throws Exception;
}
