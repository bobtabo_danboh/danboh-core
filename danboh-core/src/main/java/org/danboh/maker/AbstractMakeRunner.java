/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
/**
 *
 */
package org.danboh.maker;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.danboh.Constants;
import org.danboh.util.NativeToAsciiInputStream;
import org.danboh.xml.jaxb.setting.Output;
import org.danboh.xml.jaxb.setting.Setting;
import org.danboh.xml.jaxb.setting.SourceType;

/**
 * 基底ソースコード生成クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public abstract class AbstractMakeRunner<E> implements MakeRunnable<E>, Constants {
	/** コンテキスト */
	private VelocityContext context;
	/** 設定オブジェクト */
	private Setting setting;

	/**
	 * コンストラクタ。
	 *
	 * @param setting 設定オブジェクト
	 * @param context コンテキスト
	 */
	public AbstractMakeRunner(Setting setting, VelocityContext context) {
		this.setting = setting;
		this.context = context;
	}

	/**
	 * 設定オブジェクトを取得します。
	 *
	 * @return setting 設定オブジェクト
	 */
	protected Setting getSetting() {
		return setting;
	}

	/**
	 * コンテキストを取得します。
	 *
	 * @return コンテキスト
	 */
	protected VelocityContext getContext() {
		return this.context;
	}

	/**
	 * ファイルを出力します。
	 *
	 * @param output 出力オブジェクト
	 * @param file 出力ファイルオブジェクト
	 * @throws Exception ファイル出力失敗時にスローされる例外クラスです
	 */
	@SuppressWarnings("resource")
	protected void outputFile(Output output, File file) throws Exception {
		if (file.exists() && !output.isOverwrite()) {
			return;
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(file), setting.getEncode().getOutput()));

		// テンプレート適用
		Template template = Velocity.getTemplate(output.getTemplate(), setting.getEncode().getVm());

		if (output.isNativeToAscii()) {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			BufferedWriter byteArrayOutputWriter = new BufferedWriter(
					new OutputStreamWriter(byteArrayOutputStream));
			template.merge(this.context, byteArrayOutputWriter);
			byteArrayOutputWriter.flush();
			byteArrayOutputWriter.close();

			InputStream in = new NativeToAsciiInputStream(
					new ByteArrayInputStream(byteArrayOutputStream
							.toByteArray()));
			while (true) {
				int i = in.read();
				if (i < 0) {
					break;
				}
				writer.write(i);
			}
		} else {
			template.merge(this.context, writer);
		}

		writer.flush();
		writer.close();
	}

	/**
	 * パッケージ名を取得します。
	 *
	 * @param output 出力オブジェクト
	 * @param String カテゴリ名
	 * @return パッケージ名
	 */
	protected String getPackageName(Output output, String categoryName) {
		String packageName = EMPTY;

		if (output.getType() != SourceType.SINGLE_FILE && output.getType() != SourceType.MULTI_FILE) {
			packageName = output.getPackageRoot();
		}

		if (StringUtils.isNotBlank(output.getPackagePrefix())) {
			packageName += "." + output.getPackagePrefix();
		}

		if (output.getType() == SourceType.WEB && StringUtils.isNotEmpty(categoryName)) {
			packageName += "." + categoryName;
		}

		if (StringUtils.isNotBlank(output.getPackageSuffix())) {
			packageName += "." + output.getPackageSuffix();
		}

		return packageName;
	}

	/**
	 * パッケージパスを取得します。
	 *
	 * @param packageName パッケージ名
	 * @param output 出力オブジェクト
	 * @return パッケージパス
	 */
	protected String getPackagePath(String packageName, Output output) {
		StringBuilder builder = new StringBuilder();

		if (StringUtils.isNotEmpty(output.getProjectRoot())) {
			builder.append(output.getProjectRoot());
		}

		if (StringUtils.isNotEmpty(builder.toString())) {
			builder.append("/");
		}

		builder.append(output.getPath()).append("/").append(packageName.replace('.', '/'));

		return builder.toString();
	}
}
