/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * The original developer is Matobato of Providence.com.
 *
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh;

import org.apache.commons.lang.StringUtils;

/**
 * コンソール出力クラスです。
 *
 * @author <a href="mailto:bobtabo.buhibuhi@gmail.com">Satoshi Nagashiba</a>
 */
public class Console {

	/**
	 * コンストラクタ。
	 */
	private Console() {
	}

	/**
	 * 出力します。
	 *
	 * @param message メッセージ
	 */
	public static void out(String message) {
		out(null, message);
	}

	/**
	 * 出力します。
	 *
	 * @param prefix 接頭辞
	 * @param message メッセージ
	 */
	public static void out(String prefix, String message) {
		out(prefix, message, null);
	}

	/**
	 * 出力します。
	 *
	 * @param prefix 接頭辞
	 * @param message メッセージ
	 * @param suffix 接尾辞
	 */
	public static void out(String prefix, String message, String suffix) {
		StringBuilder builder = new StringBuilder();

		if (StringUtils.isNotEmpty(prefix)) {
			builder.append(prefix);
		}

		builder.append(message);

		if (StringUtils.isNotEmpty(suffix)) {
			builder.append(suffix);
		}

		System.out.println(builder.toString());
	}
}
